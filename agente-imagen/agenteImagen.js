const Sequelize = require('sequelize');
const config = require('../config/config.json');
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
// const containerName = "mvarticulos";
const { BlobServiceClient, StorageSharedKeyCredential } = require("@azure/storage-blob");
// const account = "tricodestorage";
// const accountKey = "AIo7Z1XkOJJHbYs5hkV13lmBiqGBkjbxzeocidFzvPVW90it7B3G/pdRGfVDECQGDzC7oYuNge3zViwc/geu7w==";
const sharedKeyCredential = new StorageSharedKeyCredential(environmentConfig.account, environmentConfig.accountKey);
const blobServiceClient = new BlobServiceClient(
    `https://${environmentConfig.account}.blob.core.windows.net`,
    sharedKeyCredential
);
var Jimp = require('jimp');
const { getBuffer } = require('jimp');
let bandera = false;


const fuenteDatos = new Sequelize(environmentConfig.database, environmentConfig.username, environmentConfig.password, {
    host: environmentConfig.host,
    dialect: environmentConfig.dialect,
    port: environmentConfig.port,
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

fuenteDatos
    .authenticate()
    .then(() => {
        console.log('Conecto a la fuente con exito.');

    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


// async function subirImagenes(articuloId, tablaId, buf, tipoImagen, folder) {
//     const containerClient = blobServiceClient.getContainerClient(environmentConfig.containerName);
//     const content = buf;
//     const blobName = `${folder}/${articuloId}-${tablaId}.${tipoImagen}`;
//     const blockBlobClient = containerClient.getBlockBlobClient(blobName);
//     const uploadBlobResponse = await blockBlobClient.upload(content, content.length);
//     console.log(`Upload block blob ${blobName} successfully`, uploadBlobResponse.requestId);
// };

async function subirImagenes(entidadId, tablaId, buf, tipoImagen, folder) {
    const containerClient = blobServiceClient.getContainerClient(environmentConfig.containerName);
    const content = buf;
    const blobName = `${folder}/${entidadId}-${tablaId}.${tipoImagen}`;
    const blockBlobClient = containerClient.getBlockBlobClient(blobName);
    const uploadBlobResponse = await blockBlobClient.upload(content, content.length);
    console.log(`Upload block blob ${blobName} successfully`, uploadBlobResponse.requestId);
};

function resize(buf, width, height) {
    return Jimp.read(buf)
        .then((image) => {
            let imagen;
            image
                .resize(width, height)
                .quality(100)
                .getBuffer(Jimp.MIME_PNG, (err, buffer) => {
                    imagen = buffer
                })
            return imagen;
        })
        .catch((err) => {
            console.log('err :>> ', err);
            bandera = true
            return false;
        });
}




module.exports = {

    async traerImagenes() {
        return fuenteDatos.query(`SELECT articuloId,articuloImagenId,articuloImagenBase64,articuloImagenTipo FROM articulo_imagen WHERE articuloImagenBase64 IS NOT NULL LIMIT 1`, { type: fuenteDatos.QueryTypes.SELECT })
            .then(async (res) => {
                if (res.length) {
                    // console.log('RES', res[0])
                    let buf = Buffer.from(res[0].articuloImagenBase64, 'base64');
                    let tipoImagen = (res[0].articuloImagenTipo).substring(6);
                    if (buf) {
                        await resize(buf, Jimp.AUTO, 1250)
                            .then(async (result) => {
                                await subirImagenes(res[0].articuloId,res[0].articuloImagenId, result, tipoImagen, 'lg')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        await resize(buf, Jimp.AUTO, 338)
                            .then(async (result) => {
                                await subirImagenes(res[0].articuloId,res[0].articuloImagenId, result, tipoImagen, 'md')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        await resize(buf, Jimp.AUTO, 225)
                            .then(async (result) => {
                                await subirImagenes(res[0].articuloId,res[0].articuloImagenId, result, tipoImagen, 'sm')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        // await resize(buf, 650, 813)
                        //     .then(async (result) => {
                        //         await subirImagenes(res[0].articuloId, result, tipoImagen, '650x813')
                        //     }).catch((err) => {
                        //         console.log('err :>> ', err);
                        //         bandera = true;
                        //     });

                        if (!bandera) {
                            fuenteDatos.query(`UPDATE articulo_imagen SET articuloImagenBase64 = NULL where articuloId = ${res[0].articuloId}`, { type: fuenteDatos.QueryTypes.UPDATE })
                                .then((result) => {
                                    console.log("Se subieron las imagenes en todos los tamaños del articulo");
                                }).catch((err) => {
                                    console.log('err:   ', err);
                                });
                        }
                    }

                }



            })
            .catch(err => {
                console.log('err :>> ', err);
            })

    },

    //? Usuario [Imagen]
    async traerImagenesUsuario() {
        return fuenteDatos.query(`SELECT appuserId , appuserImagenBase64, appuserImagenTipo FROM appuser WHERE appuserImagenBase64 IS NOT NULL LIMIT 1`, { type: fuenteDatos.QueryTypes.SELECT })
            .then(async (res) => {
                if (res.length) {
                    // console.log('RES', res[0])
                    let buf = Buffer.from(res[0].appuserImagenBase64, 'base64');
                    let tipoImagen = (res[0].appuserImagenTipo).substring(6);
                    if (buf) {
                        await resize(buf, Jimp.AUTO, 1250)
                            .then(async (result) => {
                                await subirImagenes(res[0].appuserId, "appuser", result, tipoImagen, 'appuser-lg')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        await resize(buf, Jimp.AUTO, 338)
                            .then(async (result) => {
                                await subirImagenes(res[0].appuserId, "appuser", result, tipoImagen, 'appuser-md')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        await resize(buf, Jimp.AUTO, 225)
                            .then(async (result) => {
                                await subirImagenes(res[0].appuserId, "appuser", result, tipoImagen, 'appuser-sm')
                            }).catch((err) => {
                                console.log('err :>> ', err);
                                bandera = true;
                            });
                        if (!bandera) {
                            fuenteDatos.query(`UPDATE appuser SET appuserImagenBase64 = NULL where appuserId = ${res[0].appuserId}`, { type: fuenteDatos.QueryTypes.UPDATE })
                                .then((result) => {
                                    console.log("Se subieron las imagenes en todos los tamaños del usuario");
                                }).catch((err) => {
                                    console.log('err:   ', err);
                                });
                        }
                    }

                }



            })
            .catch(err => {
                console.log('err :>> ', err);
            })

    },


    async traerImagenesCarousel  () {
        return fuenteDatos.query(`SELECT articuloId,articuloCarouselId,articuloCarouselImagenBase64,articuloCarouselImagenTipo FROM articulo_carousel WHERE  articuloCarouselImagenBase64 IS NOT NULL LIMIT 1;`, { type: fuenteDatos.QueryTypes.SELECT })
        .then(async(res) => {
            if (res.length) {
                let buf = Buffer.from(res[0].articuloCarouselImagenBase64,'base64');
                let tipoImagen = (res[0].articuloCarouselImagenTipo).substring(6);
                await resize(buf,Jimp.AUTO,713)
                .then(async(result) => {
                    await subirImagenes(res[0].articuloId,res[0].articuloCarouselId,result,tipoImagen,'articulo_carousel')
                }).catch((err) => {
                    console.log('ERROR :>> ', err);
                    bandera = true;
                });

                if (!bandera) {
                    fuenteDatos.query(`UPDATE articulo_carousel SET articuloCarouselImagenBase64 = NULL where articuloId = ${res[0].articuloId}`, { type: fuenteDatos.QueryTypes.UPDATE })
                    .then((result) => {
                        console.log("Se subieron las imagenes en todos los tamaños del articulo  de la tabla articulo_carousel");
                    }).catch((err) => {
                        console.log('err:   ', err);
                    });
                }
            }
            
    
          
        })
        .catch(err =>{
            console.log('err :>> ', err);
        })
    
    }
}

