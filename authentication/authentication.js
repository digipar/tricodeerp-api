appuser = require('../models').appuser;
appuserCaja = require('../models').appuserCaja;
appuserSucursal = require('../models').appuserSucursal;
var jwt = require('jwt-simple');
var moment = require('moment-timezone');


function createJWT(user, cajas, cajasFondos,sucursales) {
    // console.log('createJWT for',user);
    //rol
    //nombre usuario
    var payload = {
        appuserId: user.appuserId,
        appuserNombre: user.appuserNombre,
        appuserRolId: user.appuserRolId,
        appuserRolDenominacion: user.appuserRolDenominacion,
        appuserLogin: user.appuserLogin,
        iat: moment().unix(),
        exp: moment().add(14, 'days').unix(),
        appuserContrasenaTemporal: user.appuserContrasenaTemporal,
        vendedorId: user.vendedorId,
        cobradorId: user.cobradorId,
        cajas: cajas,
        sucursales:sucursales,
        cajasFondos: cajasFondos
    };

    return jwt.encode(payload, 'tricodeerp@2021');
}

module.exports = {
    login(req, res) {
        console.log("login -> req.body", req.body)
        console.log(req.body);
        appuser.findOne({ where: { appuserLogin: req.body.login } })
            .then(function (user) {
                // console.log("🚀user", user)

                if (!user) {
                    res.status(401).json({ message: 'Usuario y/o contraseña son inválidos' });
                } else {
                    if (user.appuserEstado == 1) {
                        if (req.body.password == user.appuserContrasena) {
                            appuserSucursal.findAll({ where: { appuserId: user.appuserId }, raw: true })
                            .then((result) => {
                                let sucursales = result.map(({ sucursalId }) => (sucursalId));
                                appuserCaja.findAll({ where: { appuserId: user.appuserId }, raw: true })
                                .then((result) => {
                                    if (result.length) {
                                        let cajas = result.map(({ cajaId }) => (cajaId));
                                        let cajasFondos = result.map(({ cajaId, fondoId }) => ({ cajaId, fondoId }));
                                        res.send({ token: createJWT(user, cajas, cajasFondos,sucursales) });
                                    } else {
                                        res.send({ token: createJWT(user, [], [],[]) });
                                    }
                                }).catch((err) => {
                                    console.log(err);
                                    res.status(500).json(err);
                                });
                            }).catch((err) => {
                                console.log(err);
                                res.status(500).json(err);
                            });

                       
                        } else {
                            res.status(401).send({ message: 'Usuario y/o contraseña son inválidos'});
                        }
                    } else {
                        res.status(401).send({ message: 'Usuario inactivo!' });
                    }
                }
            })
            .catch(function (err) {
                // console.log('catch...');
                console.log(err);
                res.status(500).json(err);
            });
    },

    //Get all
    findAll(req, res) {
        aplicacion.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        aplicacion.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        aplicacion.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        aplicacion.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    createTrans(req, res) {
        // console.log('Request', req);
        aplicacion.create(req)
            .then(function (result) {
                return true
            })
            .catch(function (err) {
                console.log(err);
                // throw new Error();
            });
    },
    updateTrans(req, res) {
        return aplicacion.update(elemento, criterio)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                console.log(err);
            });
    },
    //Edit
    update(req, res) {
        aplicacion.update(req.body, {
            where: {
                aplicacionId: req.params.id
            }
        })
            .then(function (result) {
                aplicacion.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        aplicacion.destroy({
            where: {
                aplicacionId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};