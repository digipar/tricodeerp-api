appuser = require('../models').appuser;

module.exports = {
    

    //Get all
    findAll(req, res) {
        appuser.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
        appuser.findByPk(req.params.id)
         .then(function(result) {
             res.status(200).json(result);
         })
         .catch(function(err) {
             res.status(500).json(err);
         });
     },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        appuser.findAll({
            where: req.body,
            attributes: {
                exclude: ['appuserContrasena']
            }
        })
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       appuser.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Edit
    update(req, res) {
       appuser.update(req.body, {
            where: {
               appuserId: req.params.id
            }
        })
        .then(function(result) {
           appuser.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    }
};