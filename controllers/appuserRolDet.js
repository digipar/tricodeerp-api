appuserRolDet = require('../models').appuserRolDet;

module.exports = {
    //Get all
    findAll(req, res) {
        appuserRolDet.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findByPk(req, res) {
       appuserRolDet.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
     //Get by Filter
     findAllByFilter(req, res) {
        console.log(req.body);
        appuserRolDet.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       appuserRolDet.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Edit
    update(req, res) {
       appuserRolDet.update(req.body, {
            where: {
               appuserRolDetId: req.params.id
            }
        })
        .then(function(result) {
           appuserRolDet.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Delete
    delete(req, res) {
       appuserRolDet.destroy({
            where: {
               appuserRolId: req.params.appuserRolId,
               appaccessId: req.params.appaccessId
            }
        })
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
};