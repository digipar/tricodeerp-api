articulo = require('../models').articulo;
var articuloCategoria = require('../controllers/articuloCategoria');
var models = require('../models/index');
module.exports = {
    //Get all
    findAll(req, res) {
        articulo.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articulo.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        articulo.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articulo.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        articulo.update(req.body, {
            where: {
                articuloId: req.params.id
            }
        })
            .then(function (result) {
                articulo.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // Crear artículo con categorías
    createArticuloCategorias(req, res) {
        // console.log('req.body', req.body)
        // let dato = {
        //     articuloDenominacion: req.body.articuloDenominacion,
        //     articuloDescripcion: req.body.articuloDescripcion,
        //     articuloCodigo: req.body.articuloCodigo,
        //     articuloEstado: req.body.articuloEstado,
        //     marcaId: req.body.marcaId,
        //     articuloIvaPorc: articulo.articuloIvaPorc
        // }
        let categorias = req.body.categorias;
        
        return models.sequelize.transaction(function (t) {
            return articulo.create(req.body)
                .then(function (result) {
                    let articuloId = result.dataValues.articuloId
                    categorias.forEach(categoriaItem => {
                        categoriaItem.articuloId = articuloId
                    });
                    return articuloCategoria.bulkCreate(categorias, { transaction: t })
                        .catch(function (err) {
                            console.log("1")
                            res.status(500).json(err);
                        })
                })
                .catch(function (err) {
                    console.log("2")
                    res.status(500).json(err);
                });

        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            console.log("3", err)
            res.status(500).json(err);
        })
    },

    updateArticuloCategorias(req, res) {
        // let dato = {
        //     articuloDenominacion: req.body.articuloDenominacion,
        //     articuloDescripcion: req.body.articuloDescripcion,
        //     articuloCodigo: req.body.articuloCodigo,
        //     marcaId: req.body.marcaId,
        //     articuloIvaPorc: req.body.articuloIvaPorc
        // }
        let articuloId = req.params.id;
        let categorias = req.body.categorias;

        return models.sequelize.transaction(function (t) {
            // Update del artículo
            return articulo.update(req.body, {
                where: {
                    articuloId: req.params.id
                }
            })
                .then(function (result) {
                    // Elimina las categorías con el id del artículo
                    return articuloCategoria.deletePorArticulo(articuloId, { transaction: t })
                        .then(function (result) {
                            // Crea las nuevas categorias del artículo
                            console.log('categorias', categorias)
                            return articuloCategoria.bulkCreate(categorias, { transaction: t })
                                .catch(function (err) {
                                    console.log("4")
                                    res.status(500).json(err);
                                })
                        })
                        .catch(function (err) {
                            console.log('3')
                            res.status(500).json(err);
                        });
                })
                .catch(function (err) {
                    console.log('2')
                    res.status(500).json(err);
                });
        })
            .then(() => {
                res.status(200).json({ success: true });
            })
            .catch(function (err) {
                console.log('1')
                res.status(500).json(err);
            });
    }
};