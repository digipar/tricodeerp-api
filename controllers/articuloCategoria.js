articuloCategoria = require('../models').articuloCategoria;

module.exports = {
    //Get all
    findAll(req, res) {
        articuloCategoria.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloCategoria.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        articuloCategoria.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articuloCategoria.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        articuloCategoria.update(req.body, {
            where: {
                articuloCategoriaId: req.params.id
            }
        })
            .then(function (result) {
                articuloCategoria.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return articuloCategoria.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                //res.status(500).json(err);
                throw new Error();
                // return false;
            });
    },

    //Delete
    deletePorArticulo(req) {
        return articuloCategoria.destroy({
            where: {
                articuloId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
};