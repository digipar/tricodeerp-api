articuloCuExt = require('../models').articuloCuExt;

module.exports = {
    //Get all
    findAll(req, res) {
        articuloCuExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloCuExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        articuloCuExt.findAll({ where: req.body.where,offset:req.body.offset,limit:req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },

    //Get by Filter
    getLength(req, res) {
        articuloCuExt.count()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};