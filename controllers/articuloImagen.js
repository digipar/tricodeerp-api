articuloImagen = require('../models').articuloImagen;
var models = require('../models/index');
module.exports = {
    //Get all
    findAll(req, res) {
        articuloImagen.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloImagen.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        articuloImagen.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articuloImagen.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
     //Edit
    //  update(req, res) {
    //     articuloImagen.update(req.body, {
    //         where: {
    //             articuloId: req.params.id
    //         }
    //     })
    //         .then(function (result) {
    //             articuloImagen.findByPk(req.params.id)
    //                 .then(function (result) {
    //                     res.status(200).json(result);
    //                 })
    //                 .catch(function (err) {
    //                     res.status(500).json(err);
    //                 });
    //         })
    //         .catch(function (err) {
    //             res.status(500).json(err);
    //         });
    // },
   
    update(req, res) {
        let datos = {
            ...req.body
        }
        let articuloImagenId = req.params.id;
        // Validación, una sola imagen principal
        if (datos.articuloImagenPrincipal == 1) {
            return models.sequelize.transaction(function (t) {
                // encuentra articuloId del registro a actualizar
                return articuloImagen.findByPk(articuloImagenId)
                    .then(function (result) {
                        // udate => articuloImagenPrincipal = 0 a todos los registros con el articuloId
                        let articuloId = result.dataValues.articuloId
                        return articuloImagen.update({ articuloImagenPrincipal: 0 }, {
                            where: {
                                articuloId: articuloId, articuloImagenPrincipal: 1
                            }
                        }).then(function (result) {
                            // udate
                            return articuloImagen.update(datos, {
                                where: {
                                    articuloImagenId: articuloImagenId
                                }
                            }).then(function (result) {
                               return articuloImagen.findByPk(articuloImagenId)
                                    .catch(function (err) {
                                        res.status(500).json(err);
                                    });
                            })
                                .catch(function (err) {
                                    res.status(500).json(err);
                                });
                        })
                            .catch(function (err) {
                                res.status(500).json(err);
                            });
                    })
                    .catch(function (err) {
                        console.log('2')
                        res.status(500).json(err);
                    });

            })
                .then(() => {
                    res.status(200).json({ success: true });
                })
                .catch(function (err) {
                    console.log('1')
                    res.status(500).json(err);
                });
        } else {
            articuloImagen.update(req.body, {
                where: {
                    articuloImagenId: req.params.id
                }
            })
                .then(function (result) {
                    articuloImagen.findByPk(req.params.id)
                        .then(function (result) {
                            res.status(200).json(result);
                        })
                        .catch(function (err) {
                            res.status(500).json(err);
                        });
                })
                .catch(function (err) {
                    res.status(500).json(err);
                });
        }
    },
    //Delete
    delete(req, res) {
        articuloImagen.destroy({
            where: {
                articuloImagenId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};