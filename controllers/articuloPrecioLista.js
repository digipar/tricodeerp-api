articuloPrecioLista = require('../models').articuloPrecioLista;
const models = require('../models/index');

module.exports = {
    //Get all
    findAll(req, res) {
        articuloPrecioLista.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloPrecioLista.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        articuloPrecioLista.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articuloPrecioLista.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        articuloPrecioLista.update(req.body, {
            where: {
                articuloPrecioListaId: req.params.id
            }
        })
            .then(function (result) {
                articuloPrecioLista.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        articuloPrecioLista.destroy({
            where: {
                articuloPrecioListaId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    insertarDetalleArticulos(req, res) {
        console.log('articuloPrecioListaId: ', req.params.id);
        return models.sequelize.query('CALL SP_insertar_articulos_lista (:articuloPrecioListaId)', 
        {replacements: { articuloPrecioListaId: req.params.id}})
        .then(function (result) {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(function (err) {
            console.log(err)
            res.status(500).json(err);
        }); 
    },
};