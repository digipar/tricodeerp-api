articuloPrecioListaDet = require('../models').articuloPrecioListaDet;
const articuloPrecioListaDetExt = require('../controllers/articuloPrecioListaDetExt');
module.exports = {
    //Get all
    findAll(req, res) {
        articuloPrecioListaDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloPrecioListaDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        articuloPrecioListaDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articuloPrecioListaDet.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findByPk(req, res) {
        articuloPrecioListaDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        articuloPrecioListaDet.update(req.body, {
            where: {
                articuloPrecioListaDetId: req.params.id
            }
        })
            .then(function (result) {
                articuloPrecioListaDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    updateArticuloPrecioLista(req, res) {
      
        articuloPrecioListaDet.update(req.body, {
            where: {
                articuloPrecioListaId: req.params.id
            }
        })
            .then(function (result) {
                articuloPrecioListaDetExt.findAllByFilterReturn({articuloPrecioListaId: req.params.id})
                .then(function (result) {
                    res.status(200).json(result);
                })
                .catch(function (err) {
                    console.log('err', err)
                    res.status(500).json(err);
                });
            })
            .catch(function (err) {
                console.log('err', err)
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        articuloPrecioListaDet.destroy({
            where: {
                articuloPrecioListaDetId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};