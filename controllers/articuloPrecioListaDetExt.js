articuloPrecioListaDetExt = require('../models').articuloPrecioListaDetExt;

module.exports = {
    //Get all
    findAll(req, res) {
        articuloPrecioListaDetExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloPrecioListaDetExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        articuloPrecioListaDetExt.findAll({ where: req.body,order:[['articuloDenominacionCodigo', 'ASC']]})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    findAllByFilterReturn(req, res) {
        console.log(req);
        return articuloPrecioListaDetExt.findAll({ where: req })
            .then(function (result) {
                return result
            })
    }
};