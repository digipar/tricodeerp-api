articuloPrecioListaFinancParam = require('../models').articuloPrecioListaFinancParam;

module.exports = {
    //Get all
    findAll(req, res) {
        articuloPrecioListaFinancParam.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        articuloPrecioListaFinancParam.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        articuloPrecioListaFinancParam.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        articuloPrecioListaFinancParam.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findByPk(req, res) {
        articuloPrecioListaFinancParam.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        articuloPrecioListaFinancParam.update(req.body, {
            where: {
                articuloPrecioListaFinancParamId: req.params.id
            }
        })
            .then(function (result) {
                articuloPrecioListaFinancParam.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        articuloPrecioListaFinancParam.destroy({
            where: {
                articuloPrecioListaFinancParamId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};