compensaCliente = require('../models').compensaCliente;

module.exports = {
    //Get all
    findAll(req, res) {
        compensaCliente.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compensaCliente.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compensaCliente.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compensaCliente.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('Error al crear: ', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compensaCliente.update(req.body, {
            where: {
                compensaClienteId: req.params.id
            }
        })
            .then(function (result) {
                compensaCliente.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                console.log('Error alactualizar: ', err)
                res.status(500).json(err);
            });
    },
    create2(req) {
        return compensaCliente.create(req)
            .then(function (result) {
                return result;
            })
    },
};