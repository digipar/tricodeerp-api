compensaClienteDet = require('../models').compensaClienteDet;

module.exports = {
    //Get all
    findAll(req, res) {
        compensaClienteDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compensaClienteDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compensaClienteDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compensaClienteDet.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('Error al crear: ', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compensaClienteDet.update(req.body, {
            where: {
                compensaClienteDetId: req.params.id
            }
        })
            .then(function (result) {
                compensaClienteDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                console.log('Error alactualizar: ', err)
                res.status(500).json(err);
            });
    },
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return compensaClienteDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
    },
};