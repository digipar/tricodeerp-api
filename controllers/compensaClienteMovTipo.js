compensaClienteMovTipo = require('../models').compensaClienteMovTipo;

module.exports = {
    //Get all
    findAll(req, res) {
        compensaClienteMovTipo.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compensaClienteMovTipo.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compensaClienteMovTipo.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compensaClienteMovTipo.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('Error al crear: ', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compensaClienteMovTipo.update(req.body, {
            where: {
                compensaClienteMovTipoId: req.params.id
            }
        })
            .then(function (result) {
                compensaClienteMovTipo.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                console.log('Error alactualizar: ', err)
                res.status(500).json(err);
            });
    }
};