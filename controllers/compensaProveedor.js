compensaProveedor = require('../models').compensaProveedor;

module.exports = {
    //Get all
    findAll(req, res) {
        compensaProveedor.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compensaProveedor.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compensaProveedor.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compensaProveedor.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('Error al crear: ', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compensaProveedor.update(req.body, {
            where: {
                compensaProveedorId: req.params.id
            }
        })
            .then(function (result) {
                compensaProveedor.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                console.log('Error alactualizar: ', err)
                res.status(500).json(err);
            });
    },
    create2(req) {
        return compensaProveedor.create(req)
            .then(function (result) {
                return result;
            })
    },
};