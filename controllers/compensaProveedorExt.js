compensaProveedorExt = require('../models').compensaProveedorExt;

module.exports = {
    //Get all
    findAll(req, res) {
        compensaProveedorExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compensaProveedorExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compensaProveedorExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
       //Get by Filter
       findAllByFilterPaginado(req, res) {
        compensaProveedorExt.findAll({ where: req.body.where,order:[['compensaProveedorId', 'DESC' ]],offset:req.body.offset,limit:req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
        },
        //Get by Filter
        getLength(req, res) {
            console.log('req.body :>> ', req.body);
            compensaProveedorExt.count({where:req.body})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};