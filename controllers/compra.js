compra = require('../models').compra;

module.exports = {
    //Get all
    findAll(req, res) {
        compra.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
       compra.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    compra.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       compra.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
		console.log('Error al crear: ',err)
            res.status(500).json(err);
        });
    },
    create2(req) {
        return compra.create(req)
            .then(function (result) {
                return true;
            })
            // .catch(function (err) {
            //     console.log(`err`, err)
            //     //res.status(500).json(err);
            //     throw new Error();
            //     // return false;
            // });
    },
    //Edit
    update(req, res) {
       compra.update(req.body, {
            where: {
               compraId: req.params.id
            }
        })
        .then(function(result) {
           compra.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
		console.log('Error alactualizar: ',err)
            res.status(500).json(err);
        });
    }
};