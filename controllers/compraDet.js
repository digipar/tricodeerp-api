compraDet = require('../models').compraDet;

module.exports = {
    //Get all
    findAll(req, res) {
        compraDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compraDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compraDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compraDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compraDet.update(req.body, {
            where: {
                compraDetId: req.params.id
            }
        })
            .then(function (result) {
                compraDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return compraDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
    },
     //Delete
     deletePorCompra2(req) {
        return compraDet.destroy({
            where: {
                compraId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePorVentaPedido(req) {
        return compraDet.destroy({
            where: {
                compraDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
};