compraDetRecepcion = require('../models').compraDetRecepcion;

module.exports = {
    //Get all
    findAll(req, res) {
        compraDetRecepcion.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compraDetRecepcion.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        compraDetRecepcion.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        compraDetRecepcion.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        compraDetRecepcion.update(req.body, {
            where: {
                compraDetRecepcionId: req.params.id
            }
        })
            .then(function (result) {
                compraDetRecepcion.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return compraDetRecepcion.bulkCreate(req)
            .then(function (result) {
                return true;
            })
    },
    // BulkUpdate
    bulkUpdate(req, res) {
        // console.log(`req.body`, req.body)
        compraDetRecepcion.update(req.body.put, {
            where: req.body.where
        })
        .then(function (result) {
            compraDetRecepcion.findAll({ where: req.body.where })
                .then(function (result) {
                    res.status(200).json(result);
                })
                .catch(function (err) {
                    res.status(500).json(err);
                });
        })
        .catch(function (err) {
            res.status(500).json(err);
        });
    },
    
     //Delete
     deletePorCompra2(req) {
        return compraDetRecepcion.destroy({
            where: {
                compraDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePorVentaPedido(req) {
        return compraDetRecepcion.destroy({
            where: {
                compraDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
};