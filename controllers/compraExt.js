compraExt = require('../models').compraExt;

module.exports = {
    //Get all
    findAll(req, res) {
        compraExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        compraExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterPaginado(req, res) {
    compraExt.findAll({ where: req.body.where,order:[['compraId', 'DESC' ]],offset:req.body.offset,limit:req.body.limit })
        .then(function (result) {
            res.status(200).json(result);
        })
        .catch(function (err) {
            res.status(500).json(err);
        });
    },



       //Get by Filter
       getLength(req, res) {
        compraExt.count({where:req.body})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    
    findAllByFilter(req, res) {
        compraExt.findAll({where: req.body})
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
};