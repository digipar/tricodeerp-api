costoCentro = require('../models').costoCentro;
const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
module.exports = {
    //Get all
    findAll(req, res) {
        if (config.esWaldbott) {
            let costoCentro = [{
                costoCentroId : 1,
                costoCentroDenominacion: 'waldbott'
            }];
            res.status(200).json(costoCentro);
        }else{
            costoCentro.findAll()
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        }
    },
    //Get by id
    findById(req, res) {
       costoCentro.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    costoCentro.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       costoCentro.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
		console.log('Error al crear: ',err)
            res.status(500).json(err);
        });
    },
    //Edit
    update(req, res) {
       costoCentro.update(req.body, {
            where: {
               costoCentroId: req.params.id
            }
        })
        .then(function(result) {
           costoCentro.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
		console.log('Error alactualizar: ',err)
            res.status(500).json(err);
        });
    }
};