cuentaClienteAcobrar = require('../models').cuentaClienteAcobrar;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaClienteAcobrar.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaClienteAcobrar.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaClienteAcobrar.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        console.log('req', req.body)
        cuentaClienteAcobrar.bulkCreate(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('err', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        cuentaClienteAcobrar.update(req.body, {
            where: {
                cuentaClienteAcobrarId: req.params.id
            }
        })
            .then(function (result) {
                cuentaClienteAcobrar.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return cuentaClienteAcobrar.bulkCreate(req)
            .then(function (result) {
               return true
            })
    },
};