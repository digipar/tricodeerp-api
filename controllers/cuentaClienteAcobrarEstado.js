cuentaClienteAcobrarEstado = require('../models').cuentaClienteAcobrarEstado;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaClienteAcobrarEstado.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        cuentaClienteAcobrarEstado.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};