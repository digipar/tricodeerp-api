cuentaClienteAcobrarEstadoV2 = require('../models').cuentaClienteAcobrarEstadoV2;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaClienteAcobrarEstadoV2.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaClienteAcobrarEstadoV2.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaClienteAcobrarEstadoV2.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterLimit(req, res) {
        cuentaClienteAcobrarEstadoV2.findAll({ where: req.body.where, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    }
};