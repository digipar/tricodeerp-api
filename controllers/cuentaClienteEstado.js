cuentaClienteEstado = require('../models').cuentaClienteEstado;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaClienteEstado.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        cuentaClienteEstado.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};