cuentaClienteEstadoV2 = require('../models').cuentaClienteEstadoV2;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaClienteEstadoV2.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaClienteEstadoV2.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaClienteEstadoV2.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterLimit(req, res) {
        cuentaClienteEstadoV2.findAll({ where: req.body.where, order:[['cuentaClienteEstadoFecha', 'DESC' ]], limit: req.body.limit})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    }
};