cuentaCobro = require('../models').cuentaCobro;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaCobro.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaCobro.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaCobro.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        cuentaCobro.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    create2(req, res) {
       return cuentaCobro.create(req)
        .then(function (result) {
           return result.dataValues
        })
    },
    //Edit
    update(req, res) {
        cuentaCobro.update(req.body, {
            where: {
                cuentaCobroId: req.params.id
            }
        })
            .then(function (result) {
                cuentaCobro.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req, res) {
        console.log('req.body :>> ', req.body);
        cuentaCobro.bulkCreate(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};