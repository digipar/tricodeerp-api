cuentaCobroExt = require('../models').cuentaCobroExt;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaCobroExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaCobroExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaCobroExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    findAllByFilter(req, res) {
        cuentaCobroExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterPaginado(req, res) {
        cuentaCobroExt.findAll({ where: req.body.where, order: [['cuentaCobroId', 'DESC']], offset: req.body.offset, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    getLength(req, res) {
        console.log('req.body :>> ', req.body);
        cuentaCobroExt.count({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterLimit(req, res) {
        cuentaCobroExt.findAll({ where: req.body.where, order: [['cuentaCobroFecha', 'DESC']], limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    }
}