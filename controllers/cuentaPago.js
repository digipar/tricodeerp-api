cuentaPago = require('../models').cuentaPago;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaPago.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaPago.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaPago.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        cuentaPago.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    create2(req, res) {
       return cuentaPago.create(req)
        .then(function (result) {
           return result.dataValues
        })
    },
    //Edit
    update(req, res) {
        cuentaPago.update(req.body, {
            where: {
                cuentaPagoId: req.params.id
            }
        })
            .then(function (result) {
                cuentaPago.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req, res) {
        console.log('req.body :>> ', req.body);
        cuentaPago.bulkCreate(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};