cuentaPagoExt = require('../models').cuentaPagoExt;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaPagoExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaPagoExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaPagoExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    findAllByFilter(req, res) {
        cuentaPagoExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterPaginado(req, res) {
        cuentaPagoExt.findAll({ where: req.body.where, order: [['cuentaPagoId', 'DESC']], offset: req.body.offset, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    getLength(req, res) {
        console.log('req.body :>> ', req.body);
        cuentaPagoExt.count({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
}