cuentaProveedorEstadoV2 = require('../models').cuentaProveedorEstadoV2;

module.exports = {
    //Get all
    findAll(req, res) {
        cuentaProveedorEstadoV2.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        cuentaProveedorEstadoV2.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        cuentaProveedorEstadoV2.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    }
};