documentoTipo = require('../models').documentoTipo;

module.exports = {
    //Get all
    findAll(req, res) {
        documentoTipo.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
       documentoTipo.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    documentoTipo.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       documentoTipo.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
		console.log('Error al crear: ',err)
            res.status(500).json(err);
        });
    },
    //Edit
    update(req, res) {
       documentoTipo.update(req.body, {
            where: {
               documentoTipoId: req.params.id
            }
        })
        .then(function(result) {
           documentoTipo.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
		console.log('Error alactualizar: ',err)
            res.status(500).json(err);
        });
    }
};