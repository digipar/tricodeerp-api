fondoMovTipoDet = require('../models').fondoMovTipoDet;

module.exports = {
    //Get all
    findAll(req, res) {
        fondoMovTipoDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        fondoMovTipoDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        fondoMovTipoDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        fondoMovTipoDet.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                console.log('Error al crear: ', err)
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        fondoMovTipoDet.update(req.body, {
            where: {
                fondoMovTipoDetId: req.params.id
            }
        })
            .then(function (result) {
                fondoMovTipoDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                console.log('Error alactualizar: ', err)
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return fondoMovTipoDet.bulkCreate(req)
            .then(function (result) {
                console.log(`result`, result)
                return true
            })
    },
    create2(req, res) {
        return fondoMovTipoDet.create(req)
            .then(function (result) {
                return result
            })
    },
};