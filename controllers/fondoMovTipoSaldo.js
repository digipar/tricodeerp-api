fondoMovTipoSaldo = require('../models').fondoMovTipoSaldo;

module.exports = {
    //Get all
    findAll(req, res) {
        fondoMovTipoSaldo.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    fondoMovTipoSaldo.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },

 
};