fondoTransf = require('../models').fondoTransf;

module.exports = {
    //Get all
    findAll(req, res) {
        fondoTransf.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
       fondoTransf.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    fondoTransf.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
       fondoTransf.create(req.body)
        .then(function(result) {
            res.status(200).send(result);
        })
        .catch(function(err) {
		console.log('Error al crear: ',err)
            res.status(500).json(err);
        });
    },
    //Edit
    update(req, res) {
       fondoTransf.update(req.body, {
            where: {
               fondoTransfId: req.params.id
            }
        })
        .then(function(result) {
           fondoTransf.findByPk(req.params.id)
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                res.status(500).json(err);
            });
        })
        .catch(function(err) {
		console.log('Error alactualizar: ',err)
            res.status(500).json(err);
        });
    }
};