notaCreditoDet = require('../models').notaCreditoDet;

module.exports = {
    //Get all
    findAll(req, res) {
        notaCreditoDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        notaCreditoDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        notaCreditoDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        notaCreditoDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        notaCreditoDet.update(req.body, {
            where: {
                notaCreditoDetId: req.params.id
            }
        })
            .then(function (result) {
                notaCreditoDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return notaCreditoDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            // .catch(function (err) {
            //     //res.status(500).json(err);
            //     throw new Error();
            //     // return false;
            // });
    },
     //Delete
     deletePornotaCredito2(req) {
        return notaCreditoDet.destroy({
            where: {
                notaCreditoId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePornotaCredito(req) {
        return notaCreditoDet.destroy({
            where: {
                notaCreditoDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    }
};