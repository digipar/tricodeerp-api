notaCreditoExt = require('../models').notaCreditoExt;

module.exports = {
    //Get all
    findAll(req, res) {
        notaCreditoExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        notaCreditoExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        notaCreditoExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    findAllByFilterPaginado(req, res) {
        notaCreditoExt.findAll({ where: req.body.where,order:[['notaCreditoId', 'DESC' ]],offset:req.body.offset,limit:req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
        },
        
       //Get by Filter
       getLength(req, res) {
        notaCreditoExt.count({where:req.body})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        notaCreditoExt.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        notaCreditoExt.update(req.body, {
            where: {
                notaCreditoExtId: req.params.id
            }
        })
            .then(function (result) {
                notaCreditoExt.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        notaCreditoExt.destroy({
            where: {
                notaCreditoExtId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};