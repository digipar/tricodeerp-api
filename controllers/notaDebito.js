notaDebito = require('../models').notaDebito;
const notaDebitoDet = require('../models').notaDebitoDet;
module.exports = {
    //Get all
    findAll(req, res) {
        notaDebito.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        notaDebito.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body);
        notaDebito.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        notaDebito.create(req.body)
            .then(function (result) {
                res.status(200).send(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    create2(req) {
        console.log('req', req)
        return notaDebito.create(req)
            .then(function (result) {
                return result;
            })
    },
    //Edit
    update(req, res) {
        notaDebito.update(req.body, {
            where: {
                notaDebitoId: req.params.id
            }
        })
            .then(function (result) {
                notaDebito.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Delete
    delete(req, res) {
        notaDebito.destroy({
            where: {
                notaDebitoId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit notaDebitoDet
    updateNotaDebitoDetAentregado(req, res) {
        notaDebitoDet.update(req.body, {
            where: {
                notaDebitoId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};