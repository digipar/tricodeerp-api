notaDebitoDet = require('../models').notaDebitoDet;

module.exports = {
    //Get all
    findAll(req, res) {
        notaDebitoDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        notaDebitoDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        notaDebitoDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        notaDebitoDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        notaDebitoDet.update(req.body, {
            where: {
                notaDebitoDetId: req.params.id
            }
        })
            .then(function (result) {
                notaDebitoDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return notaDebitoDet.bulkCreate(req)
            .then(function (result) {
                
                return true;
            })
            // .catch(function (err) {
            //     console.log('err', err)
            //     //res.status(500).json(err);
            //     throw new Error();
            //     // return false;
            // });
    },
     //Delete
     deletePornotaDebito2(req) {
        return notaDebitoDet.destroy({
            where: {
                notaDebitoId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePornotaDebito(req) {
        return notaDebitoDet.destroy({
            where: {
                notaDebitoDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    }
};