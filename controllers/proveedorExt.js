proveedorExt = require('../models').proveedorExt;

module.exports = {
    //Get all
    findAll(req, res) {
        proveedorExt.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
       proveedorExt.findByPk(req.params.id)
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
    proveedorExt.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },


};