reciboCobroExt = require('../models').reciboCobroExt;

module.exports = {
    //Get all
    findAll(req, res) {
        reciboCobroExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        reciboCobroExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilterPaginado(req, res) {
        reciboCobroExt.findAll({ where: req.body.where, order: [['reciboCobroId', 'DESC']], offset: req.body.offset, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },

    findAllByFilter(req, res) {
        reciboCobroExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).json(err);
            });
    },

    //Get by Filter
    getLength(req, res) {
        reciboCobroExt.count({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};