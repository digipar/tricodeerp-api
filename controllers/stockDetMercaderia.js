stockDetMercaderia = require('../models').stockDetMercaderia;

module.exports = {
    //Get all
    findAll(req, res) {
        stockDetMercaderia.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log('req.body', req.body)
        stockDetMercaderia.findAll({where: req.body})       
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
                console.log('err', err)
            });
    }
};