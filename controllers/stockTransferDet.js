stockTransferDet = require('../models').stockTransferDet;

module.exports = {
    //Get all
    findAll(req, res) {
        stockTransferDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        stockTransferDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        stockTransferDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        stockTransferDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        stockTransferDet.update(req.body, {
            where: {
                stockTransferDetId: req.params.id
            }
        })
            .then(function (result) {
                stockTransferDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return stockTransferDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            // .catch(function (err) {
            //     res.status(500).json(err);
            //     throw new Error();
            //     // return false;
            // });
    },
     //Delete
     deletePorstockTransfer2(req) {
        return stockTransferDet.destroy({
            where: {
                stockTransferId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePorVentaPedido(req) {
        return stockTransferDet.destroy({
            where: {
                stockTransferDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
};