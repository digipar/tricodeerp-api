stockTransferExt = require('../models').stockTransferExt;

module.exports = {
    //Get all
    findAll(req, res) {
        stockTransferExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        stockTransferExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        stockTransferExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    findAllByFilterPaginado(req, res) {
        stockTransferExt.findAll({ where: req.body.where, order: [['stockTransferEstado', 'DESC'],['stockTransferId', 'DESC']], offset: req.body.offset, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },



    //Get by Filter
    getLength(req, res) {
        stockTransferExt.count({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};