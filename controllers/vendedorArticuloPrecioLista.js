vendedorArticuloPrecioLista= require('../models').vendedorArticuloPrecioLista

module.exports = {
    //Get all
    findAll(req, res) {
        vendedorArticuloPrecioLista.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log(req.body)
        vendedorArticuloPrecioLista.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
        vendedorArticuloPrecioLista.findOne({where:{ articuloPrecioListaId: req.params.articuloPrecioListaId, vendedorId: req.params.vendedorId}})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Create
    create(req, res) {
        vendedorArticuloPrecioLista.create(req.body)
         .then(function(result) {
             res.status(200).send(result);
         })
         .catch(function(err) {
         console.log('Error al crear: ',err)
             res.status(500).json(err);
         });
     },
     //Edit
     update(req, res) {
        vendedorArticuloPrecioLista.update(req.body, {
             where: {
                vendedorId: req.params.id
             }
         })
         .then(function(result) {
            vendedor.findByPk(req.params.id)
             .then(function(result) {
                 res.status(200).json(result);
             })
             .catch(function(err) {
                 res.status(500).json(err);
             });
         })
         .catch(function(err) {
         console.log('Error alactualizar: ',err)
             res.status(500).json(err);
         });
     },
        //Delete
        delete(req, res) {
            vendedorArticuloPrecioLista.destroy({
                where: {
                    articuloPrecioListaId: req.params.articuloPrecioListaId,
                    vendedorId: req.params.vendedorId
                }
            })
                .then(function (result) {
                    res.status(200).json(result);
                })
                .catch(function (err) {
                    res.status(500).json(err);
                });
        },
  
};