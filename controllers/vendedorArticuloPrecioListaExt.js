vendedorArticuloPrecioListaExt = require('../models').vendedorArticuloPrecioListaExt;

module.exports = {
    //Get all
    findAll(req, res) {
        vendedorArticuloPrecioListaExt.findAll()
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        console.log("findAllByFilter | req.body ==>",req.body)
        vendedorArticuloPrecioListaExt.findAll({where: req.body})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            console.log(err);
            res.status(500).json(err);
        });
    },
    //Get by id
    findById(req, res) {
        vendedorArticuloPrecioListaExt.findByPk({where:{ articuloPrecioListaId: req.params.articuloPrecioListaId, vendedorId: req.params.vendedorId}})
        .then(function(result) {
            res.status(200).json(result);
        })
        .catch(function(err) {
            res.status(500).json(err);
        });
    },
  
};