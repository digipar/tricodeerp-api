ventaCondicion = require('../models').ventaCondicion;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaCondicion.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaCondicion.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaCondicion.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        ventaCondicion.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },

    //Edit
    update(req, res) {
        ventaCondicion.update(req.body, {
            where: {
                ventaCondicionId: req.params.id
            }
        })
            .then(function (result) {
                ventaCondicion.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return ventaCondicion.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                //res.status(500).json(err);
                throw new Error();
                // return false;
            });
    },

    //Delete
    deletePorVentaCondicion(req) {
        return ventaCondicion.destroy({
            where: {
                ventaCondicionId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
    //Delete
    update2(req) {
        ventaCondicion.update(req.body, {
            where: {
                ventaCondicionId: req.params.id
            }
        })
        .then(function (result) {
            return true;
        })
        .catch(function (err) {
            throw new Error();
        });
    }
};