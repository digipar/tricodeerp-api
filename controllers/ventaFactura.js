ventaFactura = require('../models').ventaFactura;
const models = require('../models/index');

module.exports = {
    //Get all
    findAll(req, res) {
        ventaFactura.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaFactura.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaFactura.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req) {
        return ventaFactura.create(req)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                //res.status(500).json(err);
                throw new Error();
                // return false;
            });
    },

    //Edit
    update(req, res) {
        ventaFactura.update(req.body, {
            where: {
                ventaFacturaId: req.params.id
            }
        })
            .then(function (result) {
                ventaFactura.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return ventaFactura.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                //res.status(500).json(err);
                throw new Error();
                // return false;
            });
    },

    //Delete
    deletePorVentaFactura(req) {
        return ventaFactura.destroy({
            where: {
                ventaFacturaId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
    //Delete
    update2(req) {
        ventaFactura.update(req.body, {
            where: {
                ventaFacturaId: req.params.id
            }
        })
        .then(function (result) {
            return true;
        })
        .catch(function (err) {
            throw new Error();
        });
    },
    // Stored procedure
    insertarVentaFactura(req, res) {
        return models.sequelize.query(`CALL SP_generar_factura('${req.params.ventaPedidoId}',${req.params.appuserId}) `)
        .then(function (result) {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(function (err) {
            console.log(err)
            res.status(500).json(err);
        }); 
    },
};