ventaFacturaDet = require('../models').ventaFacturaDet;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaFacturaDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaFacturaDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaFacturaDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        ventaFacturaDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        ventaFacturaDet.update(req.body, {
            where: {
                ventaFacturaDetId: req.params.id
            }
        })
            .then(function (result) {
                ventaFacturaDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return ventaFacturaDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                //res.status(500).json(err);
                throw new Error();
                // return false;
            });
    },
     //Delete
     deletePorVentaFactura2(req) {
        return ventaFacturaDet.destroy({
            where: {
                ventaFacturaId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePorVentaFactura(req) {
        return ventaFacturaDet.destroy({
            where: {
                ventaFacturaDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
};