ventaFacturaExt = require('../models').ventaFacturaExt;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaFacturaExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaFacturaExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
   //Get by Filter
   findAllByFilterPaginado(req, res) {
        ventaFacturaExt.findAll({ where: req.body.where,order:[['ventaFacturaId', 'DESC' ]],offset:req.body.offset,limit:req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },

    findAllByFilter(req, res) {
        ventaFacturaExt.findAll({where: req.body})
            .then(function(result) {
                res.status(200).json(result);
            })
            .catch(function(err) {
                console.log(err);
                res.status(500).json(err);
            });
    },

    //Get by Filter
    getLength(req, res) {
        ventaFacturaExt.count({where:req.body})
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};