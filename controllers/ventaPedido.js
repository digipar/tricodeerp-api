ventaPedido = require('../models').ventaPedido;
const ventaPedidoDet = require('../models').ventaPedidoDet;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaPedido.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaPedido.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaPedido.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req) {
        return ventaPedido.create(req)
            .then(function (result) {
                return result;
            })
    },

    //Edit
    update(req, res) {
        ventaPedido.update(req.body, {
            where: {
                ventaPedidoId: req.params.id
            }
        })
            .then(function (result) {
                ventaPedido.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return ventaPedido.bulkCreate(req)
            .then(function (result) {
                return true;
            })
        // .catch(function (err) {
        //     //res.status(500).json(err);
        //     throw new Error();
        //     // return false;
        // });
    },

    //Delete
    deletePorVentaPedido(req) {
        return ventaPedido.destroy({
            where: {
                ventaPedidoId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },
    //Delete
    update2(req) {
        ventaPedido.update(req.body, {
            where: {
                ventaPedidoId: req.params.id
            }
        })
            .then(function (result) {
                return result;
            })
        // .catch(function (err) {
        //     throw new Error();
        // });
    },
    //Edit ventaPedidoDet
    updateVentaPedidoDetAentregado(req, res) {
        ventaPedidoDet.update(req.body, {
            where: {
                ventaPedidoId: req.params.id
            }
        })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
};