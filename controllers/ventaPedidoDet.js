ventaPedidoDet = require('../models').ventaPedidoDet;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaPedidoDet.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaPedidoDet.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaPedidoDet.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Create
    create(req, res) {
        ventaPedidoDet.create(req.body)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Edit
    update(req, res) {
        ventaPedidoDet.update(req.body, {
            where: {
                ventaPedidoDetId: req.params.id
            }
        })
            .then(function (result) {
                ventaPedidoDet.findByPk(req.params.id)
                    .then(function (result) {
                        res.status(200).json(result);
                    })
                    .catch(function (err) {
                        res.status(500).json(err);
                    });
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // BulkCreate
    bulkCreate(req) {
        console.log('bulkCreate: ', req);
        return ventaPedidoDet.bulkCreate(req)
            .then(function (result) {
                return true;
            })
            // .catch(function (err) {
            //     //res.status(500).json(err);
            //     throw new Error();
            //     // return false;
            // });
    },
     //Delete
     deletePorVentaPedido2(req) {
        return ventaPedidoDet.destroy({
            where: {
                ventaPedidoId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    },

    //Delete
    deletePorVentaPedido(req) {
        return ventaPedidoDet.destroy({
            where: {
                ventaPedidoDetId: req
            }
        })
            .then(function (result) {
                return true;
            })
            .catch(function (err) {
                throw new Error();
            });
    }
};