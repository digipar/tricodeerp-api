ventaPedidoExt = require('../models').ventaPedidoExt;

module.exports = {
    //Get all
    findAll(req, res) {
        ventaPedidoExt.findAll()
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by id
    findById(req, res) {
        ventaPedidoExt.findByPk(req.params.id)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    //Get by Filter
    findAllByFilter(req, res) {
        ventaPedidoExt.findAll({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },
    // Get by Filter
    findAllByFilterPaginado(req, res) {
        ventaPedidoExt.findAll({ where: req.body.where,  order:[['ventaPedidoId', 'DESC' ]], offset: req.body.offset, limit: req.body.limit })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    },

    getLength(req, res) {
        ventaPedidoExt.count({ where: req.body })
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (err) {
                res.status(500).json(err);
            });
    }
};