let exec = require('child_process').exec;
const { series } = require('gulp');
let argv = require('yargs').argv;
let tag;
let branch;

function defaultTask(cb) {
    exec('ping localhost', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
}

function setTag(cb) {
    console.log(argv.tag);
    tag = (argv.tag === 'latest') ? 'latest':'test';
    branch = (tag === 'latest') ? 'master' : 'desarrollo';
    console.log(tag);
    cb();
}

function checkout(cb) {
    exec(`git checkout ${branch}`, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
}

function pull(cb) {
    exec(`git pull origin ${branch}`, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err)
    });
}

function dockerBuild(cb) {
    exec(`docker build -t tricode.azurecr.io/tricode-erp/tricode-erp-api:${tag} .`, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err)
    });
}

function dockerPush(cb) {
    exec(`docker push tricode.azurecr.io/tricode-erp/tricode-erp-api:${tag}`, function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err)
    });
}

exports.setTag = setTag;
exports.dockerPush = dockerPush;
exports.dockerBuild = dockerBuild;
exports.pull = pull;
exports.default = defaultTask;
exports.build = series(setTag,checkout,pull,dockerBuild,dockerPush);

// to "pull" the latest image from the registry
// docker pull tricode.azurecr.io/tricode-erp/tricode-erp-api:test  
// to restart the container
// docker-compose up -d