let express = require('express');
let path = require('path');
let bodyParser = require('body-parser')
let cors = require('cors');
let morgan = require('morgan');
let methodOverride = require('method-override');
let compression = require('compression');
let fs = require('fs');
let routes = require('./routes');
const functionFlag = process.env.FUNCTIONFLAG || "development_villmor";
const cron = require('node-cron');
const agenteImagen = require("./agente-imagen/agenteImagen")


cron.schedule('*/1 * * * *', () => {
  agenteImagen.traerImagenes()
    .then((result) => {
      console.log('result :>> ', result);
    }).catch((err) => {
      console.log('err :>> ', err);
    }) 
  agenteImagen.traerImagenesUsuario()
    .then((result) => {
      console.log('result :>> ', result);
    }).catch((err) => {
      console.log('err :>> ', err);
    })
  agenteImagen.traerImagenesCarousel()
    .then((result) => {
      console.log(`result carousel`, result)
    }).catch((err) => {
      console.log(`err`, err)
    });
})

  let api = express();
  // Puertos
  const apiPort = process.env.PORT || 6060;
  // parse application/x-www-form-urlencoded
  api.use(express.urlencoded({ limit: '50mb', extended: false }));
  // parse application/json
  api.use(express.json({ limit: '50mb' }));
  // compress all responses
  api.use(compression());
  // Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn’t support it.
  api.use(methodOverride());
  // HTTP request logger middleware for node.js
  api.use(morgan('dev'));
  // enable all CORS requests 
  api.use(cors());
  // Api models
  fs
    .readdirSync(__dirname + "/models")
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js' && file !== 'index.js');
    })
  // Api Routes
  api.use(routes(express.Router()));
  // Static files 
  api.use(express.static(__dirname + '/'));
  api.set('port', apiPort);
  api.listen(api.get('port'), function () {
    console.log(`tricode erp API listening on http://localhost:${apiPort}`);
  });

