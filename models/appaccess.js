/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    var appaccess = sequelize.define('appaccess', {
      appaccessId: {
        type: DataTypes.STRING(50),
        allowNull: false,
        primaryKey: true,
        field: 'appaccessId'
      },
      appaccessTipo: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
   defaultValue: '1',        defaultValue: '1',
        field: 'appaccessTipo'
      },
      appaccessDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appaccessDenominacion'
      },
    }, {
      timestamps: false,
      tableName: 'appaccess'
    });
    appaccess.removeAttribute('id');
    return appaccess;
  };
  