/* jshint indent: 2 */
module.exports = function(sequelize, DataTypes) {
    var appparam = sequelize.define('appparam', {
      appParamId: {
        type: DataTypes.STRING(100),
        allowNull: false,
        primaryKey: true,
        field: 'appparamId'
      },
      appParamValue: {
        type: DataTypes.STRING(1000),
        allowNull: false,
        field: 'appparamValue'
      }
    }, {
      timestamps: false,
      tableName: 'appparam'
    });
    appparam.removeAttribute('id');
    return appparam;
  };
  