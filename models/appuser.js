const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
module.exports = function (sequelize, DataTypes) {
  if (config.esWaldbott) {
    var appuser = sequelize.define('appuser', {
      appuserId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: 'appuserId'
      },
      appuserLogin: {
        type: DataTypes.STRING(8),
        field: 'appuserLogin'
      },
      appuserEmail: {
        type: DataTypes.STRING(100),
        field: 'appuserEmail'
      },
      appuserContrasena: {
        type: DataTypes.STRING(100),
        field: 'appuserContrasena'
      },
      appuserNombre: {
        type: DataTypes.STRING(100),
        field: 'appuserNombre'
      },
      appuserEstado: {
        type: DataTypes.INTEGER,
        field: 'appuserEstado'
      },
      appuserCreadoPor: {
        type: DataTypes.INTEGER,
        field: "appuserCreadoPor"
      },
      appuserCreadoFecha: {
        type: DataTypes.DATE,
        field: "appuserCreadoFecha"
      },
      appuserHash: {
        type: DataTypes.CHAR(32),
        field: 'appuserHash'
      },
      appuserHashValidez: {
        type: DataTypes.DATE,
        field: "appuserHashValidez"
      },
      appuserRolId: {
        type: DataTypes.INTEGER,
        field: 'appuserRolId'
      },
      appuserContrasenaTemporal: {
        type: DataTypes.INTEGER,
        field: 'appuserContrasenaTemporal'
      },
      vendedorId: {
        type: DataTypes.INTEGER,
        field: 'vendedorId'
      },
      appuserImagenBase64: {
        type: DataTypes.TEXT,
        field: 'appuserImagenBase64'
      },
      appuserImagenTipo: {
        type: DataTypes.STRING(30),
        field: 'appuserImagenTipo'
      },
      cobradorId: {
        type: DataTypes.INTEGER,
        field: 'cobradorId'
      }
    }, {
      timestamps: false,
      tableName: 'W_CM_appuser'
    });
  } else {
    var appuser = sequelize.define('appuser', {
      appuserId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'appuserId'
      },
      appuserEmail: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserEmail'
      },
      appuserContrasena: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserContrasena'
      },
      appuserNombre: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appuserNombre'
      },
      appuserEstado: {
        type: DataTypes.INTEGER(10),
        allowNull: true,
        field: 'appuserEstado'
      },
      appuserCreadoPor: {
        allowNull: false,
        type: DataTypes.INTEGER(10),
        field: "appuserCreadoPor"
      },
      appuserCreadoFecha: {
        allowNull: false,
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE,
        field: "appuserCreadoFecha"
      },
      appuserHash: {
        type: DataTypes.CHAR(32),
        allowNull: true,
        field: 'appuserHash'
      },
      appuserHashValidez: {
        allowNull: true,
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE,
        field: "appuserHashValidez"
      },
      appuserRolId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: true,
        field: 'appuserRolId'
      },
      appuserLogin: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserLogin'
      },
      appuserContrasenaTemporal: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'appuserContrasenaTemporal'
      },
      vendedorId: {
        type: DataTypes.INTEGER(10),
        allowNull: true,
        field: 'vendedorId'
      },
      appuserImagenBase64: {
        type: DataTypes.TEXT,
        allowNull: true,
        field: 'appuserImagenBase64'
      },
      appuserImagenTipo: {
        type: DataTypes.STRING(30),
        allowNull: true,
        field: 'appuserImagenTipo'
      },
      cobradorId: {
        type: DataTypes.INTEGER(10),
        allowNull: true,
        field: 'cobradorId'
      }
    }, {
      timestamps: false,
      tableName: 'appuser'
    });
  }
  appuser.removeAttribute('id');
  return appuser;
};
