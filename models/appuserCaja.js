/* jshint indent: 2 */
const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
module.exports = function (sequelize, DataTypes) {
  if (config.esWaldbott) {
    var appuserCaja = sequelize.define('appuserCaja', {
      appuserCajaId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      appuserId: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      cajaId: {
        type: DataTypes.STRING(10),
        allowNull: false
      },
      fondoId: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    }, {
      timestamps: false,
      tableName: 'W_CM_appuser_caja'
    });
  } else {
    var appuserCaja = sequelize.define('appuserCaja', {
      appuserCajaId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      appuserId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false
      },
      cajaId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false
      },
      fondoId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true
      }
    }, {
      timestamps: false,
      tableName: 'appuser_caja'
    });
  }
  appuserCaja.removeAttribute('id');
  return appuserCaja;
};
