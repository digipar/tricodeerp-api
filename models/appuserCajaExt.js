/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    var appuserCajaExt = sequelize.define('appuserCajaExt', {
        appuserCajaId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            field: 'appuserNombre'
        },
        cajaId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        cajaDenominacion: {
            type: DataTypes.STRING(100),
            field:'cajaDenominacion'
        },
        cajaEstado: {
            type: DataTypes.TINYINT(1),
        },
        fondoId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: true
        }
     
    }, {
      timestamps: false,
      tableName: 'appuser_caja_ext'
    });
    appuserCajaExt.removeAttribute('id');
    return appuserCajaExt;
  };
  