/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    const appuserExt = sequelize.define('appuserExt', {
      appuserId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        primaryKey: true,
        allowNull: false,
        defaultValue: '0',
        field: 'appuserId'
      },
      appuserEmail: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appuserEmail'
      },
      appuserNombre: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appuserNombre'
      },
      appuserEstado: {
        type: DataTypes.INTEGER(10),
        allowNull: false,
        defaultValue: '2',
        field: 'appuserEstado'
      },
      appuserCreadoPor: {
        type: DataTypes.INTEGER(10),
        allowNull: false,
        field: 'appuserCreadoPor'
      },
      appuserCreadoFecha: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
        field: 'appuserCreadoFecha'
      },
      appuserRolId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        field: 'appuserRolId'
      },
      appuserRolDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserRolDenominacion'
      },
      appuserEstadoDesc: {
        type: DataTypes.STRING(23),
        allowNull: true,
        field: 'appuserEstadoDesc'
      },
      appuserLogin: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserLogin'
      },
      vendedorId: {
        type: DataTypes.INTEGER(10),
        allowNull: false,
        field:'vendedorId'
      },
      vendedorNombre: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'vendedorNombre'
      },
      appuserImagenUrlNombre: {
        type: DataTypes.STRING(44),
        allowNull: true
      },
      appuserImagenTipo: {
        type: DataTypes.STRING(30),
        allowNull: true,
        field: 'appuserImagenTipo'
      },
      cobradorId:{
        type: DataTypes.INTEGER(10)
      },
      cobradorNombre: {
        type: DataTypes.STRING(100)
      },
    }, {
      timestamps: false,
      tableName: 'appuser_ext'
    });
    appuserExt.removeAttribute('id');
      return appuserExt;
  };
  