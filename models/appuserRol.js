/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    var appuserRol = sequelize.define('appuserRol', {
      appuserRolId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'appuserRolId'
      },
      appuserId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        field: 'appuserId'
      },
      appuserRolDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appuserRolDenominacion'
      },
      appuserRolEstado: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '2',
        field: 'appuserRolEstado'
      }
    }, {
      timestamps: false,
      tableName: 'appuser_rol'
    });
    appuserRol.removeAttribute('id');
    return appuserRol;
  };
  