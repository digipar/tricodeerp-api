module.exports = function(sequelize, DataTypes) {
    var appuserRolDet = sequelize.define('appuserRolDet', {
        appuserRolId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            primaryKey: true,
            field: 'appuserRolId'
        },
        appaccessId: {
            type: DataTypes.STRING(50),
            primaryKey: true,
            allowNull: false,
            field: 'appaccessId'
        }
    }, {
      timestamps: false,
      tableName: 'appuser_rol_det'
    });
    appuserRolDet.removeAttribute('id');
    return appuserRolDet;
  };
  