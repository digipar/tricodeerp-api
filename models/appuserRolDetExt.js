/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    var appuserRolDetExt = sequelize.define('appuserRolDetExt', {
      appuserRolId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        field: 'appuserRolId'
      },
      appaccessId: {
        type: DataTypes.STRING(50),
        allowNull: false,
        field: 'appaccessId'
      },
      appuserRolDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserRolDenominacion'
      },
      appuserRolEstado: {
        type: DataTypes.INTEGER(1),
        allowNull: true,
        defaultValue: '2',
        field: 'appuserRolEstado'
      },
      appaccessDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appaccessDenominacion'
      },
    }, {
      timestamps: false,
      tableName: 'appuser_rol_det_ext'
    });
    appuserRolDetExt.removeAttribute('id');
    return appuserRolDetExt;
  };
  