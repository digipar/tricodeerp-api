/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    const appuserRolExt = sequelize.define('appuserRolExt', {
      appuserRolId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        defaultValue: '0',
        field: 'appuserRolId'
      },
      appuserRolDenominacion: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'appuserRolDenominacion'
      },
      appuserRolEstado: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '2',
        field: 'appuserRolEstado'
      },
      appuserRolEstadoDesc: {
        type: DataTypes.STRING(11),
        allowNull: true,
        field: 'appuserRolEstadoDesc'
      },
      appuserId: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: true,
        field: 'appuserId'
      },
      appuserNombre: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: 'appuserNombre'
      }
    }, {
      timestamps: false,
      tableName: 'appuser_rol_ext'
    });
    appuserRolExt.removeAttribute('id');
    return appuserRolExt;
  };
  