const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
module.exports = function (sequelize, DataTypes) {
  if (config.esWaldbott) {
    var appuserSucursal = sequelize.define('appuserSucursal', {
      appuserSucursalId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      appuserId: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      sucursalId: {
        type: DataTypes.STRING(10),
        allowNull: false
      },

    }, {
      timestamps: false,
      tableName: 'W_CM_appuser_caja'
    });
  } else {
    var appuserSucursal = sequelize.define('appuserSucursal', {
      appuserSucursalId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      appuserId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false
      },
      sucursalId: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false
      },
   
    }, {
      timestamps: false,
      tableName: 'appuser_sucursal'
    });
  }
  appuserSucursal.removeAttribute('id');
  return appuserSucursal;
};