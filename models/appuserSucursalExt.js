module.exports = function(sequelize, DataTypes) {
    var appuserSucursalExt = sequelize.define('appuserSucursalExt', {
        appuserSucursalId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            primaryKey: true,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            field: 'appuserNombre'
        },
        sucursalId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        sucursalDenominacion: {
            type: DataTypes.STRING(100),
            field:'sucursalDenominacion'
        },
        sucursalEstado: {
            type: DataTypes.TINYINT(1),
        },
      
    }, {
      timestamps: false,
      tableName: 'appuser_sucursal_ext'
    });
    appuserSucursalExt.removeAttribute('id');
    return appuserSucursalExt;
  };
  