module.exports = function (sequelize, DataTypes) {
    var articulo = sequelize.define('articulo', {
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDescripcion: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        articuloCreadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        articuloModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        articuloIvaPorc: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        articuloErpId: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        articuloEspecificacionPedido: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        articuloStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'appuserId'
        },
    }, {
        timestamps: false,
        tableName: 'articulo'
    });
    articulo.removeAttribute('id');
    return articulo;
};
