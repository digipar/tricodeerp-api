module.exports = function (sequelize, DataTypes) {
    var articuloCatalogoExt = sequelize.define('articuloCatalogoExt', {
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        articuloPrecioListaDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        },
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        articuloDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: false,
            field: 'articuloDenominacion'
        },
        articuloDescripcion: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'categoriaDenominacion'
        },
        marcaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'marcaDenominacion'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaDenominacion'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaCod'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        articuloPrecioListaDetMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'articuloPrecioListaDetMonto'
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        articuloImagenUrlNombre: {
            type: DataTypes.INTEGER(47),
            allowNull: true
        },
        articuloPrecioListaDescuentoPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaDescuentoPorcMax'
        },
        stockMercaderiaStock: {
            type: DataTypes.INTEGER(10)
        }
    }, {
        timestamps: false,
        tableName: 'articulo_catalogo_ext'
    });
    articuloCatalogoExt.removeAttribute('id');
    return articuloCatalogoExt;
};