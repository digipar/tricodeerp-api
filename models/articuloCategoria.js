module.exports = function(sequelize, DataTypes) {
    var articuloCategoria = sequelize.define('articuloCategoria', {
        articuloCategoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true
        },
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'articulo_categoria'
    });
    articuloCategoria.removeAttribute('id');
    return articuloCategoria;
};