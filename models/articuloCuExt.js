module.exports = function (sequelize, DataTypes) {
    var articuloCuExt = sequelize.define('articuloCuExt', {
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDescripcion: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        marcaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        articuloCreadoEn: {
            type: DataTypes.DATE,
            allowNull: false
        },
        articuloModificadoEn: {
            type: DataTypes.DATE,
            allowNull: false
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloIvaPorc: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        articuloStock: {
            type: DataTypes.TINYINT(1)
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },

        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
    }, {
        timestamps: false,
        tableName: 'articulo_cu_ext'
    });
    articuloCuExt.removeAttribute('id');
    return articuloCuExt;
};
