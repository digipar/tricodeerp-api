module.exports = function (sequelize, DataTypes) {
    var articuloExt = sequelize.define('articuloExt', {
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDescripcion: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        articuloStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        articuloCreadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        articuloModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        articuloIvaPorc: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        articuloErpId: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
     
    }, {
        timestamps: false,
        tableName: 'articulo_ext'
    });
    articuloExt.removeAttribute('id');
    return articuloExt;
};