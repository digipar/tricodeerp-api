module.exports = function (sequelize, DataTypes) {
    var articuloImagen = sequelize.define('articuloImagen', {
        articuloImagenId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'articuloImagenId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        articuloImagenBase64: {
            type: DataTypes.TEXT,
            allowNull: false,
            field: 'articuloImagenBase64'
        },
        articuloImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'articuloImagenTipo'
        },
        articuloImagenPrincipal: {
            type: DataTypes.TINYINT(1),
            allowNull: true,
            field: 'articuloImagenPrincipal'
        },
        articuloImagenEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true,
            field: 'articuloImagenEstado'
        }
    }, {
        timestamps: false,
        tableName: 'articulo_imagen'
    });
    articuloImagen.removeAttribute('id');
    return articuloImagen;
};