module.exports = function (sequelize, DataTypes) {
    var articuloPrecioLista = sequelize.define('articuloPrecioLista', {
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        },
        articuloPrecioListaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            field: 'articuloPrecioListaEstado'
        },
        articuloPrecioListaTasaFinancMensual: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaTasaFinancMensual'
        },
        articuloPrecioListaDescuentoPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaDescuentoPorcMax'
        }
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista'
    });
    articuloPrecioLista.removeAttribute('id');
    return articuloPrecioLista;
};