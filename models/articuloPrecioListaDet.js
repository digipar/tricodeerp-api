module.exports = function (sequelize, DataTypes) {
    var articuloPrecioListaDet = sequelize.define('articuloPrecioListaDet', {
        articuloPrecioListaDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'articuloPrecioListaDetId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'monedaId'
        },
        articuloPrecioListaDetMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'articuloPrecioListaDetMonto'
        },
        articuloPrecioListaDetMargenPorcen: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'articuloPrecioListaDetMargenPorcen'
        },
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista_det'
    });
    articuloPrecioListaDet.removeAttribute('id');
    return articuloPrecioListaDet;
};