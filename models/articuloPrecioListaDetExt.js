module.exports = function (sequelize, DataTypes) {
    var articuloPrecioListaDetExt = sequelize.define('articuloPrecioListaDetExt', {
        articuloPrecioListaDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'articuloPrecioListaDetId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        articuloPrecioListaDetMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'articuloPrecioListaDetMonto'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: false,
            field: 'monedaCod'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'monedaDenominacion'
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            defaultValue: '0',
            field: 'monedaDecimales'
        },
        monedaPrincipal: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: '1',
            field: 'monedaPrincipal'
        },
        articuloPrecioListaDetMontoConv: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'articuloPrecioListaDetMontoConv'
        },
        stockMercaderiaStock: {
            type: DataTypes.INTEGER(10)
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        articuloIvaPorc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        articuloStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        articuloPrecioListaDetMargenPorcen: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'articuloPrecioListaDetMargenPorcen'
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista_det_ext'
    });
    articuloPrecioListaDetExt.removeAttribute('id');
    return articuloPrecioListaDetExt;
};