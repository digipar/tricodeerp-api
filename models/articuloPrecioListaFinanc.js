module.exports = function (sequelize, DataTypes) {
    var articuloPrecioListaFinanc = sequelize.define('articuloPrecioListaFinanc', {
        articuloPrecioListaFinancParamId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            field: 'articuloPrecioListaFinancParamId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloPrecioListaDetMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'articuloPrecioListaDetMonto'
        },
        articuloPrecioListaFinancParamCuotasCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaFinancParamCuotasCantidad'
        },
        articuloPrecioListaTasaFinancMensual: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaTasaFinancMensual'
        },
        articuloPrecioListaTasaFinancMensualPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaTasaFinancMensualPorc'
        },
        cuotaMensualMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'cuotaMensualMonto'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: false,
            field: 'monedaCod'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'monedaDenominacion'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            defaultValue: '0',
            field: 'monedaDecimales'
        }
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista_financ'
    });
    articuloPrecioListaFinanc.removeAttribute('id');
    return articuloPrecioListaFinanc;
};