module.exports = function (sequelize, DataTypes) {
    var articuloPrecioListaFinancParam = sequelize.define('articuloPrecioListaFinancParam', {
        articuloPrecioListaFinancParamId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            autoIncrement:true,
            primaryKey: true,
            field: 'articuloPrecioListaFinancParamId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaFinancParamCuotasCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaFinancParamCuotasCantidad'
        },
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista_financ_param'
    });
    articuloPrecioListaFinancParam.removeAttribute('id');
    return articuloPrecioListaFinancParam;
};