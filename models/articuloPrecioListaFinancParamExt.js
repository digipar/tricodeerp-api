module.exports = function (sequelize, DataTypes) {
    var articuloPrecioListaFinancParamExt = sequelize.define('articuloPrecioListaFinancParamExt', {
        articuloPrecioListaFinancParamId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            field: 'articuloPrecioListaFinancParamId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaFinancParamCuotasCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloPrecioListaFinancParamCuotasCantidad'
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        }
    }, {
        timestamps: false,
        tableName: 'articulo_precio_lista_financ_param_ext'
    });
    articuloPrecioListaFinancParamExt.removeAttribute('id');
    return articuloPrecioListaFinancParamExt;
};