module.exports = function(sequelize, DataTypes) {
    var caja = sequelize.define('caja', {
        cajaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'cajaId'
        },
        cajaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field:'cajaDenominacion'
        },
        cajaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        }
        
    },{
        timestamps: false,
        tableName: 'caja'
    });
    caja.removeAttribute('id');
    return caja;
};