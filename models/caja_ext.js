module.exports = function (sequelize, DataTypes) {
    var cajaExt = sequelize.define('cajaExt', {
        cajaId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        cajaDenominacion: {
            type: DataTypes.STRING(100)
        },
        cajaEstado: {
            type: DataTypes.TINYINT(1)
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },      
        sucursalDenominacion: {
            type: DataTypes.STRING(100)
        },
   
    }, {
        timestamps: false,
        tableName: 'caja_ext'
    });
    cajaExt.removeAttribute('id');
    return cajaExt;
};