module.exports = function(sequelize, DataTypes) {
    var categoria = sequelize.define('categoria', {
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'categoriaId'
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'categoriaDenominacion'
        },
        categoriaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
          },
        categoriaCreadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        categoriaModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
      
    },{
        timestamps: false,
        tableName: 'categoria'
    });
    categoria.removeAttribute('id');
    return categoria;
};