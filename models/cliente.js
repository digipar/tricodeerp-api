module.exports = function (sequelize, DataTypes) {
    var cliente = sequelize.define('cliente', {
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'clienteId'
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteRazonSocial'
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
            allowNull: false,
            field: 'clienteDocNro'
        },
        clienteDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'clienteDireccion'
        },
        clienteEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'documentoTipoId'
        },
        clienteTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteTelefono'
        },
        clienteErpId: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteErpId'
        },
        clienteCreadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        clienteModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'vendedorId'
        },
        cobradorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        clienteNacionalidad: {
            type: DataTypes.STRING(20),
            allowNull: true,
            field: 'clienteNacionalidad'
        },
        clienteNacimientoFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        clienteViviendaTipo: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        clienteTrabajoEmpresaNombre: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoEmpresaNombre'
        },
        clienteTrabajoEmpresaActividad: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoEmpresaActividad'
        },
        clienteTrabajoEmpresaDireccion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoEmpresaDireccion'
        },
        clienteTrabajoEmpresaTelefono: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoEmpresaTelefono'
        },
        clienteTrabajoSalario: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoSalario'
        },
        clienteTrabajoAntiguedad: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoAntiguedad'
        },
        clienteTrabajoOtrosIngresos: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'clienteTrabajoOtrosIngresos'
        },
        clienteObservacion: {
            type: DataTypes.TEXT,
            allowNull: true,
            field: 'clienteObservacion'
        },
        



    }, {
        timestamps: false,
        tableName: 'cliente'
    });
    cliente.removeAttribute('id');
    return cliente;
};