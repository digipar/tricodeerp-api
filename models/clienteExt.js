module.exports = function (sequelize, DataTypes) {
    var clienteExt = sequelize.define('clienteExt', {
        clienteId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100)
        },
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        clienteRazonSocialSucursal: {
            type: DataTypes.STRING(100)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT
        },
        clienteDireccion: {
            type: DataTypes.STRING(100)
        },
        clienteEstado: {
            type: DataTypes.TINYINT(1)
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11)
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'vendedorNombre'
        },
        clienteTelefono: {
            type: DataTypes.STRING(100)
        },
        clienteErpId: {
            type: DataTypes.STRING(50)
        },
        clienteCreadoEn: {
            type: DataTypes.DATE
        },
        clienteModificadoEn: {
            type: DataTypes.DATE
        },
        clienteRazonSocialDocNro: {
            type: DataTypes.STRING(100)
        },
        cobradorId: {
            type: DataTypes.INTEGER(10)
        },
        cobradorNombre: {
            type: DataTypes.STRING(100)
        },
        vendedorSucursalId: {
            type: DataTypes.INTEGER(10)
        },
        vendedorSucursalDenominacion: {
            type: DataTypes.STRING(100)
        },
        ventaPedidoFechaMax: {
            type: DataTypes.DATE
        },
    }, {
        timestamps: false,
        tableName: 'cliente_ext'
    });
    clienteExt.removeAttribute('id');
    return clienteExt;
};