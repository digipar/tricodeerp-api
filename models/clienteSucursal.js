module.exports = function(sequelize, DataTypes) {
    var clienteSucursal = sequelize.define('clienteSucursal', {
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true
        },
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'clienteId'
        },
        clienteSucursalNombre: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        clienteSucursalDireccion: {
            type: DataTypes.STRING(200),
            allowNull: true
        },
        clienteSucursalTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        clienteSucursalEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'cliente_sucursal'
    });
    clienteSucursal.removeAttribute('id');
    return clienteSucursal;
};