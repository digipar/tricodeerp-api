module.exports = function (sequelize, DataTypes) {
    var clienteSucursalExt = sequelize.define('clienteSucursalExt', {
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true
        },
        clienteId: {
            type: DataTypes.INTEGER(11)
        },
        clienteSucursalNombre: {
            type: DataTypes.STRING(100)
        },
        clienteSucursalDireccion: {
            type: DataTypes.STRING(200)
        },
        clienteSucursalTelefono: {
            type: DataTypes.STRING(100)
        },
        clienteSucursalEstado: {
            type: DataTypes.TINYINT(1)
        },
        clienteSucursalNombreRazonSocial: {
            type: DataTypes.STRING(200)
        },
        clienteEstado: {
            type: DataTypes.TINYINT(1)
        },
        clienteNacimientoFecha: {
            type: DataTypes.DATE
        },
        clienteDocNro: {
            type: DataTypes.BIGINT
        },
        cobradorId: {
            type: DataTypes.INTEGER(10)
        },
        vendedorId: {
            type: DataTypes.INTEGER(10)
        },
        clienteObservacion: {
            type: DataTypes.TEXT
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11)
        },
        clienteRazonSocialDocNro: {
            type: DataTypes.STRING(200)
        },
    }, {
        timestamps: false,
        tableName: 'cliente_sucursal_ext'
    });
    clienteSucursalExt.removeAttribute('id');
    return clienteSucursalExt;
};