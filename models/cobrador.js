module.exports = function(sequelize, DataTypes) {
    var cobrador = sequelize.define('cobrador', {
        cobradorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true
        },
        cobradorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true
        },
        cobradorEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'cobrador'
    });
    cobrador.removeAttribute('id');
    return cobrador;
};