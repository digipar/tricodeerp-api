module.exports = function(sequelize, DataTypes) {
    var compensaCliente = sequelize.define('compensaCliente', {
        compensaClienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
            field:'compensaClienteId'
        },
        compensaClienteFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        compensaClienteFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        compensaClienteEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
            field:'compensaClienteEstado'
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'appuserId'
        },
        clienteId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'clienteId'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'compensa_cliente'
    });
    compensaCliente.removeAttribute('id');
    return compensaCliente;
};