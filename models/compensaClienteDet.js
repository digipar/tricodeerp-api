module.exports = function(sequelize, DataTypes) {
    var compensaClienteDet = sequelize.define('compensaClienteDet', {
        compensaClienteDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
            field:'compensaClienteDetId'
        },
        compensaClienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        compensaClienteDetDebe: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        compensaClienteDetHaber: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        compensaClienteMovId:{
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        compensaClienteMovTipoId:{
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'compensa_cliente_det'
    });
    compensaClienteDet.removeAttribute('id');
    return compensaClienteDet;
};