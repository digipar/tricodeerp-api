module.exports = function(sequelize, DataTypes) {
    var compensaClienteDetExt = sequelize.define('compensaClienteDetExt', {
        compensaClienteDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            field:'compensaClienteDetId'
        },
        compensaClienteId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaClienteAcobrarReferencia:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
    
        compensaClienteDetDebe: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaClienteDetHaber: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaClienteMovId:{
            type: DataTypes.INTEGER(11)
        },
        compensaClienteMovTipoId:{
            type: DataTypes.INTEGER(11)
        },
        compensaClienteMovTipoDenominacion: {
            type: DataTypes.STRING(100),
        },
        compensaClienteFecha: {
            type: DataTypes.DATEONLY
        },
        compensaClienteEstado: {
            type: DataTypes.TINYINT(1)
        },
        clienteId:{
            type: DataTypes.INTEGER(11)
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
        },
        ventaPedidoId:{
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoFecha: {
            type: DataTypes.DATEONLY
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATEONLY
        },
        cuentaClienteDocTipoId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaClienteDocTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11)
        },
        ventaTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        cuentaClienteTipoDenominacion:{
            type: DataTypes.STRING(100)
        },
        monedaId:{
            type: DataTypes.INTEGER(11)
        },
        monedaCod:{
            type: DataTypes.STRING(20)
        },
        monedaDecimales:{
            type: DataTypes.INTEGER(11)
        },
        ventaFacturaNumero:{
            type: DataTypes.STRING(20)
        }
    },{
        timestamps: false,
        tableName: 'compensa_cliente_det_ext'
    });
    compensaClienteDetExt.removeAttribute('id');
    return compensaClienteDetExt;
};