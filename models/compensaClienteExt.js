module.exports = function(sequelize, DataTypes) {
    var compensaClienteExt = sequelize.define('compensaClienteExt', {
        compensaClienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
            field:'compensaClienteId'
        },
        compensaClienteFechaHoraRegistro: {
            type: DataTypes.DATE
        },
        compensaClienteFecha: {
            type: DataTypes.DATEONLY
        },
        compensaClienteEstado:{
            type: DataTypes.TINYINT(1)
        },
        appuserId:{
            type: DataTypes.INTEGER(11)
        },
        clienteId:{
            type: DataTypes.INTEGER(11)
        },
        appuserNombre:{
            type: DataTypes.STRING(100)
        },
        clienteRazonSocialEstado:{
            type: DataTypes.STRING(100)
        },
        clienteDocNro:{
            type: DataTypes.BIGINT,
        },
        compensaClienteDetDebeSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaClienteDetHaberSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        monedaFuerteId:{ 
            type: DataTypes.INTEGER(11)
        },
        monedaFuerteDecimales:{ 
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod:{ 
            type: DataTypes.STRING(20)
        },
        appuserImagenTipo:{
            type: DataTypes.STRING(20)
        },
        appuserImagenUrlNombre:{
            type: DataTypes.STRING(100)
        },
    },{
        timestamps: false,
        tableName: 'compensa_cliente_ext'
    });
    compensaClienteExt.removeAttribute('id');
    return compensaClienteExt;
};