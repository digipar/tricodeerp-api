module.exports = function (sequelize, DataTypes) {
    var compensaClienteMovTipo = sequelize.define('compensaClienteMovTipo', {
        compensaClienteMovTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        compensaClienteMovTipoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'compensa_cliente_mov_tipo'
    });
    compensaClienteMovTipo.removeAttribute('id');
    return compensaClienteMovTipo;
};