module.exports = function(sequelize, DataTypes) {
    var compensaProveedor = sequelize.define('compensaProveedor', {
        compensaProveedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
        },
        compensaProveedorFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        compensaProveedorFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        compensaProveedorEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        proveedorId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'compensa_proveedor'
    });
    compensaProveedor.removeAttribute('id');
    return compensaProveedor;
};