module.exports = function(sequelize, DataTypes) {
    var compensaProveedorDet = sequelize.define('compensaProveedorDet', {
        compensaProveedorDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true
        },
        compensaProveedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        compensaProveedorDetDebe: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        compensaProveedorDetHaber: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        compensaProveedorMovId:{
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        compensaProveedorMovTipoId:{
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'compensa_proveedor_det'
    });
    compensaProveedorDet.removeAttribute('id');
    return compensaProveedorDet;
};