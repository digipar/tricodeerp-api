module.exports = function(sequelize, DataTypes) {
    var compensaProveedorDetExt = sequelize.define('compensaProveedorDetExt', {
        compensaProveedorDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
        },
        compensaProveedorId: {
            type: DataTypes.INTEGER(11)
        },
        compensaProveedorDetDebe: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaProveedorDetHaber: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaProveedorMovId:{
            type: DataTypes.INTEGER(11)
        },
        compensaProveedorMovTipoId:{
            type: DataTypes.INTEGER(11)
        },
        compensaProveedorMovTipoDenominacion: {
            type: DataTypes.STRING(100),
        },
        compensaProveedorFecha: {
            type: DataTypes.DATEONLY
        },
        compensaProveedorEstado: {
            type: DataTypes.TINYINT(1)
        },
        proveedorId:{
            type: DataTypes.INTEGER(11)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
        },
        compraId:{
            type: DataTypes.INTEGER(11)
        },
        compraFecha: {
            type: DataTypes.DATEONLY
        },
        cuentaProveedorApagarFechaVencimiento: {
            type: DataTypes.DATEONLY
        },
        cuentaProveedorDocTipoId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorDocTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        compraTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        cuentaProveedorTipoDenominacion:{
            type: DataTypes.STRING(100)
        },
        monedaId:{
            type: DataTypes.INTEGER(11)
        },
        monedaCod:{
            type: DataTypes.STRING(20)
        },
        monedaDecimales:{
            type: DataTypes.INTEGER(11)
        }
    },{
        timestamps: false,
        tableName: 'compensa_proveedor_det_ext'
    });
    compensaProveedorDetExt.removeAttribute('id');
    return compensaProveedorDetExt;
};