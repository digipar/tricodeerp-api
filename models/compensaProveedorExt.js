module.exports = function(sequelize, DataTypes) {
    var compensaProveedorExt = sequelize.define('compensaProveedorExt', {
        compensaProveedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
            field:'compensaProveedorId'
        },
        compensaProveedorFechaHoraRegistro: {
            type: DataTypes.DATE
        },
        compensaProveedorFecha: {
            type: DataTypes.DATEONLY
        },
        compensaProveedorEstado:{
            type: DataTypes.TINYINT(1)
        },
        appuserId:{
            type: DataTypes.INTEGER(11)
        },
        ProveedorId:{
            type: DataTypes.INTEGER(11)
        },
        appuserNombre:{
            type: DataTypes.STRING(100)
        },
        proveedorRazonSocialEstado:{
            type: DataTypes.STRING(100)
        },
        proveedorRuc:{
            type: DataTypes.INTEGER,
        },
        compensaProveedorDetDebeSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        compensaProveedorDetHaberSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        monedaFuerteId:{ 
            type: DataTypes.INTEGER(11)
        },
        monedaFuerteDecimales:{ 
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod:{ 
            type: DataTypes.STRING(20)
        },
        appuserImagenTipo:{
            type: DataTypes.STRING(20)
        },
        appuserImagenUrlNombre:{
            type: DataTypes.STRING(100)
        },
    },{
        timestamps: false,
        tableName: 'compensa_proveedor_ext'
    });
    compensaProveedorExt.removeAttribute('id');
    return compensaProveedorExt;
};