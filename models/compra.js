module.exports = function(sequelize, DataTypes) {
    var compra = sequelize.define('compra', {
        compraId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            primaryKey: true,
            autoIncrement:true,
            field:'compraId'
        },
        compraFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        compraFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        compraVencimiento: {
            type: DataTypes.DATE,
            allowNull: true
        },
        compraTipoId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'compraTipoId'
        },
        compraCondicionId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'compraCondicionId'
        },
        compraEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true,
            defaultValue: 0,
        },
        proveedorId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'proveedorId'
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'appuserId'
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'monedaId'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        compraCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraCotizacion'
        },
        compraNroDoc: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'compraNroDoc'
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        compraGastoCuentaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
    },{
        timestamps: false,
        tableName: 'compra'
    });
    compra.removeAttribute('id');
    return compra;
};