module.exports = function(sequelize, DataTypes) {
    var compraCondicion = sequelize.define('compraCondicion', {
        compraCondicionId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'compraCondicionId'
        },
        compraCondicionDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field:'compraCondicionDenominacion'
        },
        compraCondicionEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        compraCondicionCuentaApagar: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        compraTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'compraTipoId'
        },
      
    },{
        timestamps: false,
        tableName: 'compra_condicion'
    });
    compraCondicion.removeAttribute('id');
    return compraCondicion;
};