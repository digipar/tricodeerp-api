module.exports = function(sequelize, DataTypes) {
    var compraDet = sequelize.define('compraDet', {
        compraDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'compraDetId'
        },
        compraId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'compraId'
        },
        articuloId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'articuloId'
        },
        compraDetCantidad:{
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field:'compraDetCantidad'
        },
        compraDetPrecioUnitario: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'compraDetPrecioUnitario'
        },
        compraDetIvaPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'compraDetIvaPorc'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'compra_det'
    });
    compraDet.removeAttribute('id');
    return compraDet;
};