module.exports = function (sequelize, DataTypes) {
    var compraDetExt = sequelize.define('compraDetExt', {
        compraDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'compraDetId'
        },
        compraId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'compraId'
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'articuloId'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        compraDetCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'compraDetCantidad'
        },
        compraDetCantidadSaldo: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'compraDetCantidadSaldo'
        },
        compraDetPrecioUnitario: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraDetPrecioUnitario'
        },
        compraDetIvaPorc: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraDetIvaPorc'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        compraDetTotal: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraDetTotal'
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        compraDetRecibidoCantidad: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        }, 
   
    }, {
        timestamps: false,
        tableName: 'compra_det_ext'
    });
    compraDetExt.removeAttribute('id');
    return compraDetExt;
};