module.exports = function(sequelize, DataTypes) {
    var compraDetRecepcion = sequelize.define('compraDetRecepcion', {
        compraDetRecepcionId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'compraDetId'
        },
        compraDetId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'compraDetId'
        },
        compraDetRecepcionCantidad: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraDetRecepcionCantidad'
        },
        compraDetRecepcionEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        depositoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        }, 
  
    },{
        timestamps: false,
        tableName: 'compra_det_recepcion'
    });
    compraDetRecepcion.removeAttribute('id');
    return compraDetRecepcion;
};