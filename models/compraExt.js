module.exports = function (sequelize, DataTypes) {
    var compraExt = sequelize.define('compraExt', {
        compraId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'compraId'
        },
        compraFecha: {
            type: DataTypes.DATEONLY,
            allowNull: true,
            field: 'compraFecha'
        },
        compraVencimiento: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'compraVencimiento'
        },
        compraFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'compraFechaHoraRegistro'
        },
       compraNroDoc: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'compraNroDoc'
        },
        compraEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
       compraPrecioTotal: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'compraPrecioTotal'
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        compraCondicionId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'compraCondicionId'
        },
        compraCondicionDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field: 'compraCondicionDenominacion'
        },
        compraTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'compraTipoId'
        },
        compraTipoDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field: 'compraTipoDenominacion'
        },
        compraTipoStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        compraRecibido: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'monedaDenominacion'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            defaultValue: '0',
            field: 'monedaDecimales'
        },
        monedaPrincipal: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: '1',
            field: 'monedaPrincipal'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaCod'
        },
        
        proveedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'proveedorId'
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'proveedorRazonSocialEstado'
        },
        proveedorRuc: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'proveedorRuc'
        },
        proveedorDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'proveedorDireccion'
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'appuserNombre'
        },
        compraCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'compraCotizacion'
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        compraGastoCuentaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
        },
        compraGastoCuentaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
    }, {
        timestamps: false,
        tableName: 'compra_ext'
    });
    compraExt.removeAttribute('id');
    return compraExt;
};