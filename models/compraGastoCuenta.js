module.exports = function(sequelize, DataTypes) {
    var compraGastoCuenta = sequelize.define('compraGastoCuenta', {
        compraGastoCuentaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true
        },
        compraGastoCuentaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        compraGastoCuentaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
    },{
        timestamps: false,
        tableName: 'compra_gasto_cuenta'
    });
    compraGastoCuenta.removeAttribute('id');
    return compraGastoCuenta;
};