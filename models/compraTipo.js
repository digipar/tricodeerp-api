module.exports = function(sequelize, DataTypes) {
    var compraTipo = sequelize.define('compraTipo', {
        compraTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'compraTipoId'
        },
        compraTipoDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field:'compraTipoDenominacion'
        },
        compraTipoStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
   
      
    },{
        timestamps: false,
        tableName: 'compra_tipo'
    });
    compraTipo.removeAttribute('id');
    return compraTipo;
};