module.exports = function(sequelize, DataTypes) {
    var costoCentro = sequelize.define('costoCentro', {
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        costoCentroEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 1,
        },
        costoCentroLogo: {
            type: DataTypes.TEXT,
            allowNull: false,
            field: 'costoCentroLogo'
        },
    },{
        timestamps: false,
        tableName: 'costo_centro'
    });
    costoCentro.removeAttribute('id');
    return costoCentro;
};