module.exports = function (sequelize, DataTypes) {
    var cotizacionExt = sequelize.define('cotizacionExt', {
        cotizacionId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            primaryKey: true,
            field: 'cotizacionId'
        },
        cotizacionFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cotizacionFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cotizacionCompra: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'cotizacionCompra'
        },
        cotizacionVenta: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'cotizacionVenta'
        },
        monedaId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            primaryKey: true,
            field: 'monedaId'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'monedaDenominacion'
        },
        cotizacionRefMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            field: 'cotizacionRefMonto'
        },
     
    }, {
        timestamps: false,
        tableName: 'cotizacion_ext'
    });
    cotizacionExt.removeAttribute('id');
    return cotizacionExt;
};