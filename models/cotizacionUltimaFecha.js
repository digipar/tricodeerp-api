const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];

module.exports = function (sequelize, DataTypes) {
    if (config.esWaldbott) {
        var cotizacionUltimaFecha = sequelize.define('cotizacionUltimaFecha', {
            cotizacionId: {
                type: DataTypes.STRING(8),
                primaryKey: true,
                field: 'cotizacionId'
            },
            monedaId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                field: 'monedaId'
            },
            cotizacionRefMonto: {
                type: DataTypes.DECIMAL,
                allowNull: true,
                field: 'cotizacionRefMonto'
            },
            cotizacionFecha: {
                type: DataTypes.DATE,
                allowNull: true
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50),
                allowNull: false,
                field: 'monedaDenominacion'
            },
            monedaCod: {
                type: DataTypes.STRING(5),
                allowNull: false,
                field: 'monedaCod'
            },
            monedaDecimales: {
                type: DataTypes.INTEGER,
                allowNull: true,
                field: 'monedaDecimales'
            },
            monedaPrincipal: {
                type: DataTypes.INTEGER,
                allowNull: true,
                field: 'monedaPrincipal'
            }
        }, {
            timestamps: false,
            tableName: 'W_CM_cotizacion_ultima_fecha'
        });
    }else{
        var cotizacionUltimaFecha = sequelize.define('cotizacionUltimaFecha', {
            cotizacionId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                primaryKey: true,
                field: 'cotizacionId'
            },
            monedaId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                primaryKey: true,
                field: 'monedaId'
            },
            cotizacionFecha: {
                type: DataTypes.DATE,
                allowNull: true
            },
            cotizacionRefMonto: {
                type: DataTypes.DECIMAL,
                allowNull: true,
                field: 'cotizacionRefMonto'
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50),
                allowNull: false,
                field: 'monedaDenominacion'
            },
            monedaCod: {
                type: DataTypes.STRING(5),
                allowNull: false,
                field: 'monedaCod'
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50),
                allowNull: false,
                field: 'monedaDenominacion'
            },
            monedaDecimales: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
                defaultValue: '0',
                field: 'monedaDecimales'
            },
            monedaDecimales: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
                defaultValue: '0',
                field: 'monedaDecimales'
            },
            monedaPrincipal: {
                type: DataTypes.INTEGER(11),
                allowNull: true,
                defaultValue: '1',
                field: 'monedaPrincipal'
            }
        }, {
            timestamps: false,
            tableName: 'cotizacion_ultima_fecha'
        });
    }
    
    cotizacionUltimaFecha.removeAttribute('id');
    return cotizacionUltimaFecha;
};