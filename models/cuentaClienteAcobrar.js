module.exports = function (sequelize, DataTypes) {
    var cuentaClienteAcobrar = sequelize.define('cuentaClienteAcobrar', {
        cuentaClienteAcobrarId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            primaryKey: true
        },
        cuentaClienteTipoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        cuentaClienteDocTipoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        cuentaClienteAcobrarFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        cuentaClienteAcobrarReferencia:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        cuentaClienteAcobrarMonto: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        cuentaClienteAcobrarCotizacion: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        cuentaClienteAcobrarEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        transaccionId: {
            type: DataTypes.STRING(100),
            allowNull: true
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_a_cobrar'
    });
    cuentaClienteAcobrar.removeAttribute('id');
    return cuentaClienteAcobrar;
};