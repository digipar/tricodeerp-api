module.exports = function (sequelize, DataTypes) {
    var cuentaClienteAcobrarEstado = sequelize.define('cuentaClienteAcobrarEstado', {
        clienteId: {
            type: DataTypes.INTEGER(10)
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100),
        },
        clienteDocNro: {
            type: DataTypes.BIGINT
        },
        clienteTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteTelefono'
        },
        cuentaClienteAcobrarMontoSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        cuentaClienteCobroMontoSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        cuentaClienteAcobrarEstadoSaldo: {
            type: DataTypes.DECIMAL(14,2)
        },
        monedaPrincipalId: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        cobradorId: {
            type: DataTypes.INTEGER(10)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_a_cobrar_estado'
    });
    cuentaClienteAcobrarEstado.removeAttribute('id');
    return cuentaClienteAcobrarEstado;
};