module.exports = function(sequelize, DataTypes) {
    var cuentaClienteAcobrarEstadoV2 = sequelize.define('cuentaClienteAcobrarEstadoV2', {
        clienteId: {
            type: DataTypes.INTEGER(11)
        },
        clienteRazonSocial:{
            type: DataTypes.STRING(100)
        },
        clienteDocNro:{
            type: DataTypes.BIGINT,
        },
        clienteTelefono:{
            type: DataTypes.STRING(100),
        },
        cobradorId:{
            type: DataTypes.INTEGER(11)
        },
        cobradorNombre:{
            type: DataTypes.STRING(100)
        },
        cuentaClienteAcobrarEstadoSaldo:{
            type: DataTypes.DECIMAL(14,2)
        },
        monedaId:{ 
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales:{ 
            type: DataTypes.INTEGER(10)
        },
        monedaCod:{ 
            type: DataTypes.STRING(20)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(11)
        }
    },{
        timestamps: false,
        tableName: 'cuenta_cliente_a_cobrar_estado_v2'
    });
    cuentaClienteAcobrarEstadoV2.removeAttribute('id');
    return cuentaClienteAcobrarEstadoV2;
};