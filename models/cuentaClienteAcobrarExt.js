module.exports = function (sequelize, DataTypes) {
    var cuentaClienteAcobrarExt = sequelize.define('cuentaClienteAcobrarExt', {
        cuentaClienteAcobrarId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true
        },
        cuentaClienteTipoId: {
            type: DataTypes.INTEGER(10),
        },
        cuentaClienteDocTipoId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaClienteAcobrarReferencia:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        cuentaClienteAcobrarFecha: {
            type: DataTypes.DATEONLY
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATEONLY
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaClienteAcobrarMonto: {
            type: DataTypes.DECIMAL(14,2)
        },
        cuentaClienteAcobrarCotizacion: {
            type: DataTypes.DECIMAL(14,2)
        },
        monedaId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaClienteAcobrarEstado: {
            type: DataTypes.TINYINT(1)
        },
        ventaFacturaId: {
            type: DataTypes.INTEGER(10)
        },
        ventaFacturaNumero:{
            type: DataTypes.STRING(20)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_a_cobrar_ext'
    });
    cuentaClienteAcobrarExt.removeAttribute('id');
    return cuentaClienteAcobrarExt;
};