module.exports = function (sequelize, DataTypes) {
    var cuentaClienteCobro = sequelize.define('cuentaClienteCobro', {
        cuentaClienteCobroId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            primaryKey: true
        },
        cuentaClienteAcobrarId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        cuentaClienteTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        cuentaClienteDocTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        cuentaClienteCobroMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        transaccionId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_cobro'
    });
    cuentaClienteCobro.removeAttribute('id');
    return cuentaClienteCobro;
};