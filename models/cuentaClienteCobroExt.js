module.exports = function (sequelize, DataTypes) {
    var cuentaClienteCobroExt = sequelize.define('cuentaClienteCobroExt', {
        cuentaClienteCobroId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true
        },
        cuentaClienteAcobrarId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaClienteDocTipoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        conceptoTexto: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'conceptoTexto'
        },
        reciboCobroId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        cuentaClienteCobroMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATE
        },
        cuentaClienteAcobrarMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaClienteCobroMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaClienteEstadoMontoSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        cuentaClienteAcobrarFecha: {
            type: DataTypes.DATE
        },
        cuentaClienteTipoId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaClienteTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        reciboCobroFecha: {
            type: DataTypes.DATEONLY
        },
        appuserId: {
            type: DataTypes.INTEGER(11)
        },
        cobradorId: {
            type: DataTypes.INTEGER(11)
        },
        cobradorId: {
            type: DataTypes.INTEGER(11)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_cobro_ext'
    });
    cuentaClienteCobroExt.removeAttribute('id');
    return cuentaClienteCobroExt;
};