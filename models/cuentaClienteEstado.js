module.exports = function (sequelize, DataTypes) {
    var cuentaClienteEstado = sequelize.define('cuentaClienteEstado', {
        cuentaClienteAcobrarId: {
            type: DataTypes.INTEGER(11)
        },
        clienteId: {
            type: DataTypes.INTEGER(11)
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100)
        },
        cuentaClienteAcobrarMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaClienteAcobrarMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaClienteEstadoMontoSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            defaultValue: '0',
            field: 'monedaDecimales'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: false,
            field: 'monedaCod'
        },
        cuentaClienteAcobrarEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cuentaClienteAcobrarFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
           
        },
        clienteTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteTelefono'
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
        },
        cuentaClienteTipoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        cuentaClienteTipoDenominacion:{
            type: DataTypes.INTEGER(100),
            allowNull: false
        },
        cobradorId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaClienteMontoSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        ventaTipoDenominacion: {
            type: DataTypes.STRING(50)
        },
        ventaPedidoFecha: {
            type: DataTypes.DATEONLY
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cliente_estado'
    });
    cuentaClienteEstado.removeAttribute('id');
    return cuentaClienteEstado;
};