module.exports = function(sequelize, DataTypes) {
    var cuentaClienteEstadoV2 = sequelize.define('cuentaClienteEstadoV2', {
        clienteId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaClienteEstadoMovId:{
            type: DataTypes.INTEGER(11)
        },
        cuentaClienteAcobrarReferencia:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        compensaClienteMovTipoId:{
            type: DataTypes.INTEGER(11)
        },
        compensaClienteMovMonto:{
            type: DataTypes.DECIMAL(14,2)
        },
        compensaClienteMovCompensado:{
            type: DataTypes.DECIMAL(14,2)
        },
        cuentaClienteEstadoMontoSaldo:{
            type: DataTypes.DECIMAL(14,2)
        },
        ventaPedidoId:{
            type: DataTypes.INTEGER(11)
        },
        ventaTipoDenominacion:{
            type: DataTypes.STRING(50)
        },
        cuentaClienteTipoDenominacion:{
            type: DataTypes.STRING(50)
        },
        cuentaClienteAcobrarFecha: {
            type: DataTypes.DATEONLY
        },
        cuentaClienteAcobrarFechaVencimiento: {
            type: DataTypes.DATEONLY
        },
        diasVencidos:{
            type: DataTypes.INTEGER(10)
        },
        cobradorId:{
            type: DataTypes.INTEGER(11)
        },
        cobradorNombre: {
            type: DataTypes.STRING(100)
        },
        monedaDecimales:{ 
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        },
        monedaId:{
            type: DataTypes.INTEGER(11)
        },
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(11)
        },
        clienteTelefono:{
            type: DataTypes.STRING(100),
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
        },
        cuentaClienteEstadoMovTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        costoCentroId:{
            type: DataTypes.INTEGER(11)
        },  
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        cuentaClienteEstadoFecha:{
            type: DataTypes.DATEONLY
        },
        ventaFacturaId: {
            type: DataTypes.INTEGER(10)
        },
        ventaFacturaNumero:{
            type: DataTypes.STRING(20)
        }
    },{
        timestamps: false,
        tableName: 'cuenta_cliente_estado_v2'
    });
    cuentaClienteEstadoV2.removeAttribute('id');
    return cuentaClienteEstadoV2;
};