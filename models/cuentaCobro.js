module.exports = function (sequelize, DataTypes) {
    var cuentaCobro = sequelize.define('cuentaCobro', {
        cuentaCobroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        cuentaCobroFecha: {
            type: DataTypes.DATE,
            allowNull: false
        },
        cuentaCobroFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaCobroDocNro: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true
        },
        clienteId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaCobroEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        cobradorId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        monedaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        cuentaCobroMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        cuentaCobroCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        conceptoTexto: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'conceptoTexto'
        },
    }, {
        timestamps: false,
        tableName: 'cuenta_cobro'
    });
    cuentaCobro.removeAttribute('id');
    return cuentaCobro;
};