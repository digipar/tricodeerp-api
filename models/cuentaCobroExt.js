module.exports = function (sequelize, DataTypes) {
    var cuentaCobroExt = sequelize.define('cuentaCobroExt', {
        cuentaCobroId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true
        },
        cuentaCobroFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        cuentaCobroFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaCobroDocNro: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: true
        },
        clienteId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaCobroEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        cobradorId: {
         type: DataTypes.INTEGER(10),
         allowNull: true   
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaCobroMonto: {
            type: DataTypes.DECIMAL(14,2)
        },
        cuentaCobroCotizacion: {
            type: DataTypes.DECIMAL(14,2)
        },
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        conceptoTexto: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'conceptoTexto'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod:{
            type: DataTypes.STRING(20)
        },
        monedaDenominacion:{
            type: DataTypes.STRING(50)
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        fondoMov:{
            type: DataTypes.INTEGER(11)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_cobro_ext'
    });
    cuentaCobroExt.removeAttribute('id');
    return cuentaCobroExt;
};