module.exports = function (sequelize, DataTypes) {
    var cuentaPago = sequelize.define('cuentaPago', {
        cuentaPagoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        cuentaPagoFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cuentaPagoFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaPagoDocNro: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true
        },
        proveedorId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false
        },
        cuentaPagoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        monedaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        cuentaPagoMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        cuentaPagoCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_pago'
    });
    cuentaPago.removeAttribute('id');
    return cuentaPago;
};