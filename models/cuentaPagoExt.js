module.exports = function (sequelize, DataTypes) {
    var cuentaPagoExt = sequelize.define('cuentaPagoExt', {
        cuentaPagoId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true
        },
        cuentaPagoFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        cuentaPagoFechaHoraRegistro: {
            type: DataTypes.DATE
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        cuentaPagoDocNro: {
            type: DataTypes.DECIMAL(14, 2)
        },
        proveedorId: {
            type: DataTypes.INTEGER(11).UNSIGNED
        },
        cuentaPagoEstado: {
            type: DataTypes.TINYINT(1)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaPagoMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaPagoCotizacion: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        },
        appuserNombre: {
            type: DataTypes.DECIMAL(14, 2)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'proveedorRuc'
        },
        fondoMovId:{
            type: DataTypes.INTEGER(11)
        },
    
    }, {
        timestamps: false,
        tableName: 'cuenta_pago_ext'
    });
    cuentaPagoExt.removeAttribute('id');
    return cuentaPagoExt;
};