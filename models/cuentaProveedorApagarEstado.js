module.exports = function (sequelize, DataTypes) {
    var cuentaProveedorApagarEstado = sequelize.define('cuentaProveedorApagarEstado', {
        proveedorId: {
            type: DataTypes.INTEGER(11)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorApagarMontoSum: {
            type: DataTypes.DECIMAL(36,2)
        },
        cuentaProveedorPagoMontoSum: {
            type: DataTypes.DECIMAL(36,2)
        },
        cuentaProveedorEstadoSaldoMonto: {
            type: DataTypes.DECIMAL(37, 2)
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_proveedor_a_pagar_estado'
    });
    cuentaProveedorApagarEstado.removeAttribute('id');
    return cuentaProveedorApagarEstado;
};