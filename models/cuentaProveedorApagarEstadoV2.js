module.exports = function(sequelize, DataTypes) {
    var cuentaProveedorApagarEstadoV2 = sequelize.define('cuentaProveedorApagarEstadoV2', {
        proveedorId: {
            type: DataTypes.INTEGER(11)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
        },
        proveedorTelefono: {
            type: DataTypes.STRING(100),
        },
        cuentaProveedorEstadoSaldoMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(11)
        }
    },{
        timestamps: false,
        tableName: 'cuenta_proveedor_a_pagar_estado_v2'
    });
    cuentaProveedorApagarEstadoV2.removeAttribute('id');
    return cuentaProveedorApagarEstadoV2;
};