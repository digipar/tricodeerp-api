module.exports = function (sequelize, DataTypes) {
    var cuentaProveedorEstado = sequelize.define('cuentaProveedorEstado', {
        cuentaProveedorApagarId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorApagarFecha: {
            type: DataTypes.DATE
        },
        cuentaProveedorApagarFechaVencimiento: {
            type: DataTypes.DATE
        },
        proveedorId: {
            type: DataTypes.INTEGER(11)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        cuentaProveedorApagarMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaProveedorPagoMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaProveedorEstadoMontoSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        cuentaProveedorDocTipoId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorDocTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        compraId: {
            type: DataTypes.INTEGER(10)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        cuentaProveedorTipoId:{
            type: DataTypes.INTEGER(10)
        },
        compraEstado: {
            type: DataTypes.TINYINT(1)
        },
        cuentaProveedorTipoDenominacion: {
            type: DataTypes.STRING(50)
        },
        compraTipoId: {
            type: DataTypes.INTEGER(10)
        },
        compraTipoDenominacion: {
            type: DataTypes.STRING(50)
        },
        compraFecha: {
            type: DataTypes.DATEONLY
        },
        compraVencimiento: {
            type: DataTypes.DATEONLY
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_proveedor_estado'
    });
    cuentaProveedorEstado.removeAttribute('id');
    return cuentaProveedorEstado;
};