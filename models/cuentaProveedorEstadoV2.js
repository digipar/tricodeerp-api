module.exports = function (sequelize, DataTypes) {
    var cuentaProveedorEstadoV2 = sequelize.define('cuentaProveedorEstadoV2', {
        proveedorId: {
            type: DataTypes.INTEGER(11)
        },
        compensaProveedorMovTipoId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorEstadoMovId: {
            type: DataTypes.INTEGER(11)
        },
        compensaProveedorMovMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        compensaProveedorMovCompensado: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaProveedorEstadoMontoSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        compraId: {
            type: DataTypes.INTEGER(11)
        },
        compraTipoDenominacion: {
            type: DataTypes.STRING(50)
        },
        cuentaProveedorTipoDenominacion: {
            type: DataTypes.STRING(50)
        },
        cuentaProveedorApagarFecha: {
            type: DataTypes.DATEONLY
        },
        cuentaProveedorApagarFechaVencimiento: {
            type: DataTypes.DATEONLY
        },
        diasVencidos: {
            type: DataTypes.INTEGER(10)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaId:{
            type: DataTypes.INTEGER(11)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(11)
        },
        proveedorTelefono:{
            type: DataTypes.STRING(100),
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
        },
        cuentaProveedorEstadoMovTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(11)
        },
        cuentaProveedorApagarId:{
            type: DataTypes.INTEGER(11)
        },
        compraNroDoc:{
            type: DataTypes.BIGINT
        },
        cuentaPagoDocNro:{
            type: DataTypes.STRING(100)
        },
    }, {
        timestamps: false,
        tableName: 'cuenta_proveedor_estado_v2'
    });
    cuentaProveedorEstadoV2.removeAttribute('id');
    return cuentaProveedorEstadoV2;
};