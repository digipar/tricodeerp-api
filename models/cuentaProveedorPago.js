module.exports = function (sequelize, DataTypes) {
    var cuentaProveedorPago = sequelize.define('cuentaProveedorPago', {
        cuentaProveedorPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            primaryKey: true
        },
        cuentaProveedorApagarId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaProveedorTipoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaProveedorDocTipoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        cuentaProveedorPagoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(100),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_proveedor_pago'
    });
    cuentaProveedorPago.removeAttribute('id');
    return cuentaProveedorPago;
};