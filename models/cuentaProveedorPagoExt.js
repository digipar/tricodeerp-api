module.exports = function (sequelize, DataTypes) {
    var cuentaProveedorPagoExt = sequelize.define('cuentaProveedorPagoExt', {
        cuentaProveedorPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true
        },
        cuentaProveedorApagarId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaProveedorTipoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        cuentaProveedorDocTipoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        cuentaProveedorPagoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        cuentaProveedorApagarFechaVencimiento: {
            type: DataTypes.DATE
        },
        cuentaProveedorApagarMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaProveedorPagoMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        cuentaProveedorEstadoMontoSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        cuentaProveedorApagarFecha: {
            type: DataTypes.DATE
        },
        cuentaProveedorDocTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        compraId: {
            type: DataTypes.INTEGER(10)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100)
        }
    }, {
        timestamps: false,
        tableName: 'cuenta_proveedor_pago_ext'
    });
    cuentaProveedorPagoExt.removeAttribute('id');
    return cuentaProveedorPagoExt;
};