module.exports = function (sequelize, DataTypes) {
    var deposito = sequelize.define('deposito', {
        depositoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'depositoId'
        },
        depositoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'depositoDenominacion'
        },
        depositoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
     
    }, {
        timestamps: false,
        tableName: 'deposito'
    });
    deposito.removeAttribute('id');
    return deposito;
};