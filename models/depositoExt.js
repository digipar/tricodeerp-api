module.exports = function (sequelize, DataTypes) {
    var depositoExt = sequelize.define('depositoExt', {
        depositoId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        depositoDenominacion: {
            type: DataTypes.STRING(100)
        },
        depositoDenominacionEstado: {
            type: DataTypes.STRING(100)
        },
        depositoCentroCostoDenominacionEstado: {
            type: DataTypes.STRING(100)
        },
        depositoEstado: {
            type: DataTypes.TINYINT(1)
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100)
        },
        sucursalDenominacion: {
            type: DataTypes.STRING(100)
        },
        depositoCostoCentroDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: false
        },
    }, {
        timestamps: false,
        tableName: 'deposito_ext'
    });
    depositoExt.removeAttribute('id');
    return depositoExt;
};