module.exports = function(sequelize, DataTypes) {
    var documentoTipo = sequelize.define('documentoTipo', {
        documentoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'documentoTipoId'
        },
        documentoTipoNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'documentoTipoNombre'
        },
        documentoTipoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
          },
     
    },{
        timestamps: false,
        tableName: 'documento_tipo'
    });
    documentoTipo.removeAttribute('id');
    return documentoTipo;
};