module.exports = function(sequelize, DataTypes) {
    var fondo = sequelize.define('fondo', {
        fondoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'fondoId'
        },
        fondoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field:'fondoDenominacion'
        },
        fondoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
     
    },{
        timestamps: false,
        tableName: 'fondo'
    });
    fondo.removeAttribute('id');
    return fondo;
};