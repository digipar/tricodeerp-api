module.exports = function (sequelize, DataTypes) {
    var fondoCapitalizacion = sequelize.define('fondoCapitalizacion', {
        fondoCapitalizacionId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoCapitalizacionFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoCapitalizacionFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: true,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoCapitalizacionEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        fondoCapitalizacionCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        fondoCapitalizacionMonto:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        }
    }, {
        timestamps: false,
        tableName: 'fondo_capitalizacion'
    });
    fondoCapitalizacion.removeAttribute('id');
    return fondoCapitalizacion;
};