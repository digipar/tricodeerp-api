module.exports = function (sequelize, DataTypes) {
    var fondoCapitalizacionExt = sequelize.define('fondoCapitalizacionExt', {
        fondoCapitalizacionId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoCapitalizacionFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoCapitalizacionFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: false,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        appuserNombre:{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoCapitalizacionEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoCapitalizacionCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        fondoCapitalizacionMonto:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        monedaDecimales:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
        },
    }, {
        timestamps: false,
        tableName: 'fondo_capitalizacion_ext'
    });
    fondoCapitalizacionExt.removeAttribute('id');
    return fondoCapitalizacionExt;
};