module.exports = function (sequelize, DataTypes) {
    var fondoDeposito = sequelize.define('fondoDeposito', {
        fondoDepositoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoDepositoFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoDepositoFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: true,
        },
        cajaId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoIdDestino:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoDepositoEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        fondoDepositoCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        fondoDepositoMonto:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        }
    }, {
        timestamps: false,
        tableName: 'fondo_deposito'
    });
    fondoDeposito.removeAttribute('id');
    return fondoDeposito;
};