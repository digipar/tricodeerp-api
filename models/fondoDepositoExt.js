module.exports = function (sequelize, DataTypes) {
    var fondoDepositoExt = sequelize.define('fondoDepositoExt', {
        fondoDepositoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoDepositoFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoDepositoFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: false,
        },
        cajaId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        cajaDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoIdDestino:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoDestinoDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoOrigenDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        appuserNombre:{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoDepositoEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoDepositoCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        fondoDepositoMonto:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        monedaDecimales:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
        },
    }, {
        timestamps: false,
        tableName: 'fondo_deposito_ext'
    });
    fondoDepositoExt.removeAttribute('id');
    return fondoDepositoExt;
};