module.exports = function(sequelize, DataTypes) {
    var fondoEntidad = sequelize.define('fondoEntidad', {
        fondoEntidadId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'fondoEntidadId'
        },
        fondoEntidadDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field:'fondoEntidadDenominacion'
        },
        
    },{
        timestamps: false,
        tableName: 'fondo_entidad'
    });
    fondoEntidad.removeAttribute('id');
    return fondoEntidad;
};