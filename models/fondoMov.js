module.exports = function (sequelize, DataTypes) {
    var fondoMov = sequelize.define('fondoMov', {
        fondoMovId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'fondoMovId'
        },
        fondoMovFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        fondoMovFechaHora: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoMovEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        fondoMovCaracter: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
        },
        cajaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        transaccionId: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        fondoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov'
    });
    fondoMov.removeAttribute('id');
    return fondoMov;
};