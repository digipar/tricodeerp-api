module.exports = function (sequelize, DataTypes) {
    var fondoMovExt = sequelize.define('fondoMovExt', {
        fondoMovId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true,
        },
        fondoMovFechaHoraRegistro:{
            type: DataTypes.DATE
        },
        fondoMovFechaHora:{
            type: DataTypes.DATEONLY
        },
        cajaId: {
            type: DataTypes.INTEGER(11),
        },
        appuserId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        cajaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'cajaDenominacion'
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        fondoId: {
            type: DataTypes.INTEGER(11)
        },
        fondoDenominacion: {
            type: DataTypes.STRING(100)
        },
        fondoMovCaracter:{
            type: DataTypes.INTEGER(1)
        },
        fondoMovFecha:{
            type: DataTypes.DATEONLY
        },
        fondoMovTipoDetMonedaFuerteMontoTotal:{
            type: DataTypes.DECIMAL(14,2)
        },
        monedaFuerteDecimales:{
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod:{
            type: DataTypes.STRING(20)
        },
        fondoMovEstado:{
            type: DataTypes.TINYINT(1)
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_ext'
    });
    fondoMovExt.removeAttribute('id');
    return fondoMovExt;
};