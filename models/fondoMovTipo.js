module.exports = function (sequelize, DataTypes) {
    var fondoMovTipo = sequelize.define('fondoMovTipo', {
        fondoMovTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'fondoMovTipoId'
        },
        fondoMovTipoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'fondoMovTipoDenominacion'
        },
        fondoMovCaracter: {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            field: 'fondoMovCaracter'
        },
        fondoMovRequireId: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        fondoMovEntidad: {
            type: DataTypes.STRING(100),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_tipo'
    });
    fondoMovTipo.removeAttribute('id');
    return fondoMovTipo;
};