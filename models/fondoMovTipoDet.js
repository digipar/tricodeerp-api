module.exports = function (sequelize, DataTypes) {
    var fondoMovTipoDet = sequelize.define('fondoMovTipoDet', {
        fondoMovTipoDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'fondoMovTipoDetId'
        },
        fondoMovId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'fondoMovId'
        },
        fondoMovTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'fondoMovTipoId'
        },
        fondoMovTipoDetNumero: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'fondoMovTipoDetNumero'
        },
        transaccionId: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'transaccionId'
        },
        fondoMovTipoDetMonto: { 
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        },
        monedaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false
        },
        fondoMovTipoDetCotizacion: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_tipo_det'
    });
    fondoMovTipoDet.removeAttribute('id');
    return fondoMovTipoDet;
};