module.exports = function (sequelize, DataTypes) {
    var fondoMovTipoDetExt = sequelize.define('fondoMovTipoDetExt', {
        fondoMovTipoDetId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true,
        },
        fondoMovId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovTipoId: {
            type: DataTypes.INTEGER(11)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovTipoDetNumero: {
            type: DataTypes.STRING(100),
        },
        fondoMovTipoDetMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        fondoMovTipoDetCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false
        },
        fondoMovCaracter: {
            type: DataTypes.INTEGER(1)
        },
        cajaId: {
            type: DataTypes.INTEGER(11)
        },
        appuserId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovFechaHora: {
            type: DataTypes.DATE
        },
        cajaDenominacion: {
            type: DataTypes.STRING(100)
        },
        fondoMovTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        fondoMovEntidad: {
            type: DataTypes.STRING(100)
        },
        fondoMovTipoDetMonedaFuerteMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaFuerteId: {
            type: DataTypes.INTEGER(11)
        },
        monedaFuerteCod: {
            type: DataTypes.STRING(5)
        },
        monedaFuerteDecimales: {
            type: DataTypes.INTEGER(10)
        },
        fondoMovEstado: {
            type: DataTypes.TINYINT(1)  
        },
        fondoMovFecha: {
            type: DataTypes.DATEONLY
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_tipo_det_ext'
    });
    fondoMovTipoDetExt.removeAttribute('id');
    return fondoMovTipoDetExt;
};