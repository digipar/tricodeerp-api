module.exports = function (sequelize, DataTypes) {
    var fondoMovTipoExt = sequelize.define('fondoMovTipoExt', {
        fondoMovTipoId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        fondoMovTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        fondoMovCaracter: {
            type: DataTypes.INTEGER(1),
        },
        fondoMovRequireId: {
            type: DataTypes.TINYINT(1),
        },
        fondoMovEntidad: {
            type: DataTypes.STRING(100),
        },
        fondoMovTipoIdDenominacion: {
            type: DataTypes.STRING(100),
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_tipo_ext'
    });
    fondoMovTipoExt.removeAttribute('id');
    return fondoMovTipoExt;
};