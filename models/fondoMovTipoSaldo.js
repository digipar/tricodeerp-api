module.exports = function (sequelize, DataTypes) {
    var fondoMovTipoSaldo = sequelize.define('fondoMovTipoSaldo', {
        fondoMovTipoId: {
            type: DataTypes.STRING(50)
        },
        fondoMovTipoNro: {
            type: DataTypes.STRING(50)
        },
        fondoMovTipoFecha: {
            type: DataTypes.DATEONLY
        },
        fondoMovTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovTipoTotalMonto: {
            type: DataTypes.DECIMAL(14,2)
        },
        fondoMovTipoSaldadoMonto: {
            type: DataTypes.DECIMAL(14,2)
        },
        fondoMovTipoSaldoMonto: {
            type: DataTypes.DECIMAL(14,2)
        },
        fondoMovTipoDatosAdicionales: {
            type: DataTypes.STRING(100)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(20)
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_tipo_saldo'
    });
    fondoMovTipoSaldo.removeAttribute('id');
    return fondoMovTipoSaldo;
};