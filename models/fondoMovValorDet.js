module.exports = function (sequelize, DataTypes) {
    var fondoMovValorDet = sequelize.define('fondoMovValorDet', {
        fondoMovValorDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'fondoMovValorDetId'
        },
        fondoMovValorDetVencimiento: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        fondoMovValorDetNumero: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoMovValorDetCotizacion: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
        },
        fondoMovId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoValorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        fondoEntidadId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoMovValorDetMonto: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
        },
        fondoMovValorDetFacturaNumero: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoMovValorDetEmision: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        fondoMovValorDetMonedaPrincipal:{
            type: DataTypes.DECIMAL(13,0),
            allowNull: true,
        },
        fondoMovValorDetFirmanteRazonSocial: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoMovValorDetFirmanteDocNro: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoEntidadIdCredito: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoMovValorDetComprobante: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        fondoMovValorDetCertificado: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        transaccionId: {
            type: DataTypes.STRING(100),
            allowNull: false,
        }
    }, {
        timestamps: false,
        tableName: 'fondo_mov_valor_det'
    });
    fondoMovValorDet.removeAttribute('id');
    return fondoMovValorDet;
};