module.exports = function (sequelize, DataTypes) {
    var fondoMovValorDetExt = sequelize.define('fondoMovValorDetExt', {
        fondoMovValorDetId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
        fondoMovId: {
            type: DataTypes.INTEGER(11)
        },
        fondoValorId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovValorDetVencimiento: {
            type: DataTypes.DATEONLY
        },
        fondoMovValorDetNumero: {
            type: DataTypes.STRING(50)
        },
        fondoEntidadId: {
            type: DataTypes.INTEGER(11)
        },
        fondoMovValorDetCotizacion: {
            type: DataTypes.DECIMAL(14, 2)
        },
        fondoMovId: {
            type: DataTypes.INTEGER(11)
        },
        fondoValorDenominacion: {
            type: DataTypes.STRING(100)
        },
        fondoValorEntidad: {
            type: DataTypes.TINYINT(1)
        },
        fondoValorNumero: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorVencimiento: {
            type: DataTypes.TINYINT(1)
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        fondoMovValorDetMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        fondoMovValorDetMonedaFuerteMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaFuerteId: {
            type: DataTypes.INTEGER(11)
        },
        monedaFuerteCod: {
            type: DataTypes.STRING(5)
        },
        monedaFuerteDecimales: {
            type: DataTypes.INTEGER(10)
        },
        fondoMovEstado: {
            type: DataTypes.TINYINT(1)  
        },
        fondoMovFecha:{
            type: DataTypes.DATEONLY
        },
        cajaId:{
            type: DataTypes.INTEGER(11)
        },
        cajaDenominacion:{
            type: DataTypes.STRING(50)
        },
        fondoMovCaracter:{
            type: DataTypes.INTEGER(1)
        },
        fondoMovValorDetFacturaNumero: {
            type: DataTypes.STRING(100)
        },
        fondoMovValorDetEmision: {
            type: DataTypes.DATEONLY
        },
        fondoMovTipoDet: {
            type: DataTypes.STRING(1000)
        },
    }, {
        timestamps: false,
        tableName: 'fondo_mov_valor_det_ext'
    });
    fondoMovValorDetExt.removeAttribute('id');
    return fondoMovValorDetExt;
};