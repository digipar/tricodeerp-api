module.exports = function (sequelize, DataTypes) {
    var fondoTransf = sequelize.define('fondoTransf', {
        fondoTransfId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoTransfFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoTransfFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: true,
        },
        cajaIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        cajaIdDestino:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        fondoTransfEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        fondoTransfCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        fondoTransfMonto:{
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        fondoTransfIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
    }, {
        timestamps: false,
        tableName: 'fondo_transf'
    });
    fondoTransf.removeAttribute('id');
    return fondoTransf;
};