module.exports = function (sequelize, DataTypes) {
    var fondoTransfExt = sequelize.define('fondoTransfExt', {
        fondoTransfId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        fondoTransfFecha:{
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        fondoTransfFechaHoraRegistro:{
            type: DataTypes.DATE,
            allowNull: false,
        },
        cajaIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        cajaOrigenDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        cajaIdDestino:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        cajaDestinoDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        appuserId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        appuserNombre:{
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoTransfEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        fondoTransfCotizacion:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        fondoTransfMonto:{
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        fondoTransfIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
    }, {
        timestamps: false,
        tableName: 'fondo_transf_ext'
    });
    fondoTransfExt.removeAttribute('id');
    return fondoTransfExt;
};