module.exports = function (sequelize, DataTypes) {
    var fondoValor = sequelize.define('fondoValor', {
        fondoValorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'fondoValorId'
        },
        fondoValorDenominacion:{
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        fondoValorVencimiento:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorNumero:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorEntidad:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        monedaId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        fondoValorEmision:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorFacturaNumero:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorMonedaPrincipal:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorFirmante:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorPorFactura:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorCertificado:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorEntidadCredito:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorComprobante:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        fondoValorEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: false,
        }
    }, {
        timestamps: false,
        tableName: 'fondo_valor'
    });
    fondoValor.removeAttribute('id');
    return fondoValor;
};