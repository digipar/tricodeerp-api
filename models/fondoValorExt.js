const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
module.exports = function (sequelize, DataTypes) {
    if (config.esWaldbott) {
        
        var fondoValorExt = sequelize.define('fondoValorExt', {
            fondoValorId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'fondoValorId'
            },
            fondoValorDenominacion:{
                type: DataTypes.STRING(100),
                allowNull: false,
            },
            fondoValorVencimiento:{
                type: DataTypes.TINYINT,
                allowNull: false,
            },
            fondoValorNumero:{
                type: DataTypes.TINYINT,
                allowNull: false,
            },
            fondoValorEntidad:{
                type: DataTypes.TINYINT,
                allowNull: false,
            },
            monedaId:{
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50)
            },
            monedaDecimales: {
                type: DataTypes.INTEGER
            },
            monedaCod: {
                type: DataTypes.STRING(20)
            },
            fondoValorDenominacionMon:{
                type: DataTypes.STRING(100)
            },
        }, {
            timestamps: false,
            tableName: 'W_CM_fondo_valor_ext'
        });
    }else{

        var fondoValorExt = sequelize.define('fondoValorExt', {
            fondoValorId: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                field: 'fondoValorId'
            },
            fondoValorDenominacion:{
                type: DataTypes.STRING(100),
                allowNull: false,
            },
            fondoValorVencimiento:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorNumero:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorEntidad:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            monedaId:{
                type: DataTypes.INTEGER(11),
                allowNull: false,
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50)
            },
            monedaDecimales: {
                type: DataTypes.INTEGER(10)
            },
            monedaCod: {
                type: DataTypes.STRING(20)
            },
            fondoValorDenominacionMon:{
                type: DataTypes.STRING(100)
            },
            fondoValorEmision:{
                type: DataTypes.TINYINT(1),
            },
            fondoValorFacturaNumero:{
                type: DataTypes.TINYINT(1),
            },
            fondoValorMonedaPrincipal:{
                type: DataTypes.TINYINT(1),
            },
            monedaPrincipalDecimales:{
                type: DataTypes.INTEGER(10),
            },
            fondoValorFirmante:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorPorFactura:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorCertificado:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorEntidadCredito:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorComprobante:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            },
            fondoValorEstado:{
                type: DataTypes.TINYINT(1),
                allowNull: false,
            }
        }, {
            timestamps: false,
            tableName: 'fondo_valor_ext'
        });
    }
    fondoValorExt.removeAttribute('id');
    return fondoValorExt;
};