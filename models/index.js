'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "development_villmor";
let config = require(__dirname + '/../config/config.json')[env];
const db = {};
const Op = Sequelize.Op;

var operatorsAliases = {
  $gt: Op.gt,
  $gte: Op.gte,
  $lte: Op.lte,
  $between: Op.between,
  $in: Op.in,
  $lt: Op.lt,
  $or: Op.or,
  $not: Op.not,
  $like: Op.like
}

config = {
  ...config,
  operatorsAliases
}
let sequelize;
console.log('Environment: ' + env.toUpperCase());
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    // const model = sequelize['import'](path.join(__dirname, file));
    const model = require(path.join(__dirname, file))(sequelize, Sequelize);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

try {
  sequelize.authenticate();
  console.log('Connection has been established successfully.');
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

module.exports = db;
