module.exports = function (sequelize, DataTypes) {
    var loteDisponible = sequelize.define('loteDisponible', {
        movimientoFecha: {
            type: DataTypes.DATEONLY
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'categoriaDenominacion'
        },
        movimientoTipo: {
            type: DataTypes.STRING(50)
        },
        movimientoId: {
            type: DataTypes.INTEGER(10)
        },
        depositoId: {
            type: DataTypes.INTEGER(10)
        },
        depositoDenominacionEstado: {
            type: DataTypes.STRING(100),
        },
        articuloId: {
            type: DataTypes.INTEGER(10)
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        articuloSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        movimientoMontoUnit: {
            type: DataTypes.DECIMAL(14, 2)
        },
        movimientoMontoTotal: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaFuerteId: {
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod: {
            type: DataTypes.STRING(5)
        },
        monedaFuerteDecimales: {
            type: DataTypes.INTEGER(10)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
    }, {
        timestamps: false,
        tableName: 'lote_disponible'
    });
    loteDisponible.removeAttribute('id');
    return loteDisponible;
};