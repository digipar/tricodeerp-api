module.exports = function(sequelize, DataTypes) {
    var marca = sequelize.define('marca', {
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'marcaId'
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'marcaDenominacion'
        },
        marcaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
          },
      
    },{
        timestamps: false,
        tableName: 'marca'
    });
    marca.removeAttribute('id');
    return marca;
};