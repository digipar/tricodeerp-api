const env = process.env.NODE_ENV || "development_villmor";
const config = require(__dirname + '/../config/config.json')[env];
// console.log(`config`, config)
module.exports = function (sequelize, DataTypes) {
    if (config.esWaldbott) {
        var moneda = sequelize.define('moneda', {
            monedaId: {
                type: DataTypes.STRING(1),
                allowNull: false,
                primaryKey: true,
                field: 'monedaId'
            },
            monedaCod: {
                type: DataTypes.STRING(2),
                allowNull: true,
                field: 'monedaCod'
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50),
                allowNull: true,
                field: 'monedaDenominacion'
            },
            monedaDenominacionPlural: {
                type: DataTypes.STRING(100),
                allowNull: true,
                field: 'monedaDenominacionPlural'
            },
            monedaDecimales: {
                type: DataTypes.INTEGER,
                allowNull: true,
                defaultValue: '0',
                field: 'monedaDecimales'
            },
            monedaPrincipal: {
                type: DataTypes.TINYINT(1),
                allowNull: true,
                defaultValue: '0',
                field: 'monedaPrincipal'
            },
           
        }, {
            timestamps: false,
            tableName: 'W_CM_moneda'
        });
       
    }else{
        var moneda = sequelize.define('moneda', {
            monedaId: {
                type: DataTypes.INTEGER(10).UNSIGNED,
                allowNull: false,
                primaryKey: true,
                field: 'monedaId'
            },
            monedaCod: {
                type: DataTypes.STRING(5),
                allowNull: false,
                field: 'monedaCod'
            },
            monedaDenominacion: {
                type: DataTypes.STRING(50),
                allowNull: false,
                field: 'monedaDenominacion'
            },
            monedaDecimales: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
                defaultValue: '0',
                field: 'monedaDecimales'
            },
            monedaPrincipal: {
                type: DataTypes.TINYINT(1),
                allowNull: true,
                defaultValue: '0',
                field: 'monedaPrincipal'
            },
            monedaDenominacionPlural: {
                type: DataTypes.STRING(100),
                allowNull: false,
                field: 'monedaDenominacionPlural'
            },
        }, {
            timestamps: false,
            tableName: 'moneda'
        });
    }
    
    moneda.removeAttribute('id');
    return moneda;
};
