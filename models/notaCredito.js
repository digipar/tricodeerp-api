module.exports = function (sequelize, DataTypes) {
    var notaCredito = sequelize.define('notaCredito', {
        notaCreditoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'notaCreditoId'
        },
        notaCreditoFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            field: 'notaCreditoFecha'
        },
        notaCreditoFechaHora: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'notaCreditoFechaHora'
        },

        notaCreditoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'notaCreditoTipoId'
        },
        notaCreditoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'costoCentroId'
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaPedidoId'
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
        },
        notaCreditoCotizacion: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },

    }, {
        timestamps: false,
        tableName: 'nota_credito'
    });
    notaCredito.removeAttribute('id');
    return notaCredito;
};