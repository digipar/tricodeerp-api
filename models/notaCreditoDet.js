module.exports = function(sequelize, DataTypes) {
    var notaCreditoDet = sequelize.define('notaCreditoDet', {
        notaCreditoDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'notaCreditoDetId'
        },
        notaCreditoId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaCreditoId'
        },
        ventaPedidoDetId:{
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field:'ventaPedidoDetId'
        },
        notaCreditoDetCantidad:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaCreditoDetCantidad'
        },
        notaCreditoDetValorUnitario: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaCreditoDetValorUnitario'
        },
        notaCreditoDetIvaPorc: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'notaCreditoDetIvaPorc'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'nota_credito_det'
    });
    notaCreditoDet.removeAttribute('id');
    return notaCreditoDet;
};