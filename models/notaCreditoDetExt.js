module.exports = function (sequelize, DataTypes) {
    var notaCreditoDetExt = sequelize.define('notaCreditoDetExt', {
        notaCreditoDetId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true,
        },
        notaCreditoId: {
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoDetId: {
            type: DataTypes.INTEGER(10)
        },
        notaCreditoDetCantidad: {
            type: DataTypes.INTEGER(11)
        },
        notaCreditoDetValorUnitario: {
            type: DataTypes.INTEGER(11)
        },
        notaCreditoDetIvaPorc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        articuloId: {
            type: DataTypes.INTEGER(10)
        },
        articuloCodigo: {
            type: DataTypes.STRING(50)
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100)
        },
        notaCreditoDetRecibido: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoDetRecibidoCantidad: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoDetCantidadSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoDetTotal: {
            type: DataTypes.DECIMAL(14, 2)
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        notaCreditoDetMontoUnitMonedaFuerte: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoDetMontoTotalMonedaFuerte: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoDetTotal: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaFuerteId: {
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod: {
            type: DataTypes.STRING(5)
        },
        monedaFuerteDecimales: {
            type: DataTypes.INTEGER(10)
        },
    }, {
        timestamps: false,
        tableName: 'nota_credito_det_ext'
    });
    notaCreditoDetExt.removeAttribute('id');
    return notaCreditoDetExt;
};