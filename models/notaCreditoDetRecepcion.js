module.exports = function(sequelize, DataTypes) {
    var notaCreditoDetRecepcion = sequelize.define('notaCreditoDetRecepcion', {
        notaCreditoDetRecepcionId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'notaCreditoDetRecepcionId'
        },
        notaCreditoDetId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'notaCreditoDetId'
        },
        notaCreditoDetRecepcionCantidad:{
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'notaCreditoDetRecepcionCantidad'
        },
        notaCreditoDetRecepcionEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
            field:'notaCreditoDetRecepcionEstado'
        },
        depositoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'depositoId'
        }
    },{
        timestamps: false,
        tableName: 'nota_credito_det_recepcion'
    });
    notaCreditoDetRecepcion.removeAttribute('id');
    return notaCreditoDetRecepcion;
};