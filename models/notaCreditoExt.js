module.exports = function (sequelize, DataTypes) {
    var notaCreditoExt = sequelize.define('notaCreditoExt', {
        notaCreditoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'notaCreditoId'
        },
        notaCreditoFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            field: 'notaCreditoFecha'
        },
        notaCreditoFechaHora: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'notaCreditoFechaHora'
        },

        notaCreditoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'notaCreditoTipoId'
        },
        notaCreditoTipoDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'notaCreditoTipoDenominacion'
        },
        notaCreditoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'costoCentroId'
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaPedidoId'
        },
        ventaPedidoDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        notaCreditoTipoStock: {
            type: DataTypes.TINYINT(1)
        },
        notaCreditoRecibido: {
            type: DataTypes.TINYINT(1)
        },
        clienteId: {
            type: DataTypes.INTEGER(10)
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
        },
        ventaTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        ventaCondicionDescripcion: {
            type: DataTypes.STRING(100)
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaCondicionId'
        },
        ventaPedidoVencimiento: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoVencimiento'
        },
        ventaPedidoRecargoPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoRecargoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoDescPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        monedaId: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaDenominacionPlural: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'monedaDenominacionPlural'
        },
        notaCreditoCantidadSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoValorUnitarioSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaCreditoValorTotalSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        clienteDireccion: {
            type: DataTypes.STRING(100)
        },
        clienteTelefono: {
            type: DataTypes.STRING(100)
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'vendedorNombre'
        },
        notaCreditoTotalConDesc: {
            type: DataTypes.DECIMAL(14, 2)
        },
    }, {
        timestamps: false,
        tableName: 'nota_credito_ext'
    });
    notaCreditoExt.removeAttribute('id');
    return notaCreditoExt;
};