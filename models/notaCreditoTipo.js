module.exports = function (sequelize, DataTypes) {
    var notaCreditoTipo = sequelize.define('notaCreditoTipo', {
        notaCreditoTipoId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            primaryKey: true,
            field: 'notaCreditoTipoId'
        },
 
        notaCreditoTipoDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'notaCreditoTipoDenominacion'
        },

    }, {
        timestamps: false,
        tableName: 'nota_credito_tipo'
    });
    notaCreditoTipo.removeAttribute('id');
    return notaCreditoTipo;
};