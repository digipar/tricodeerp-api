module.exports = function (sequelize, DataTypes) {
    var notaDebito = sequelize.define('notaDebito', {
        notaDebitoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'notaDebitoId'
        },
        notaDebitoFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            field: 'notaDebitoFecha'
        },
        notaDebitoFechaHora: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'notaDebitoFechaHora'
        },

        notaDebitoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'notaDebitoTipoId'
        },
        notaDebitoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'costoCentroId'
        },
        compraId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'compraId'
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
        },
        notaDebitoCotizacion: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },

    }, {
        timestamps: false,
        tableName: 'nota_debito'
    });
    notaDebito.removeAttribute('id');
    return notaDebito;
};