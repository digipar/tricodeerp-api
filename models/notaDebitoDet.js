module.exports = function(sequelize, DataTypes) {
    var notaDebitoDet = sequelize.define('notaDebitoDet', {
        notaDebitoDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'notaDebitoDetId'
        },
        notaDebitoId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaDebitoId'
        },
        compraDetId:{
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field:'compraDetId'
        },
        notaDebitoDetCantidad:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaDebitoDetCantidad'
        },
        notaDebitoDetValorUnitario: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'notaDebitoDetValorUnitario'
        },
        notaDebitoDetIvaPorc: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'notaDebitoDetIvaPorc'
        },
        depositoId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'depositoId'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        }
    },{
        timestamps: false,
        tableName: 'nota_debito_det'
    });
    notaDebitoDet.removeAttribute('id');
    return notaDebitoDet;
};