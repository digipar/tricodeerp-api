module.exports = function(sequelize, DataTypes) {
    var notaDebitoDetEntrega = sequelize.define('notaDebitoDetEntrega', {
        notaDebitoDetEntregaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'notaDebitoDetEntregaId'
        },
        notaDebitoDetId:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'notaDebitoDetId'
        },
        notaDebitoDetEntregaCantidad:{
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'notaDebitoDetEntregaCantidad'
        },
        notaDebitoDetEntregaEstado:{
            type: DataTypes.TINYINT(1),
            allowNull: true,
            field:'notaDebitoDetEntregaEstado'
        },
        depositoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'depositoId'
        }
    },{
        timestamps: false,
        tableName: 'nota_debito_det_entrega'
    });
    notaDebitoDetEntrega.removeAttribute('id');
    return notaDebitoDetEntrega;
};