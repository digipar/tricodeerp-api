module.exports = function (sequelize, DataTypes) {
    var notaDebitoDetExt = sequelize.define('notaDebitoDetExt', {
        notaDebitoDetId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true,
        },
        notaDebitoId: {
            type: DataTypes.INTEGER(11)
        },
        compraDetId: {
            type: DataTypes.INTEGER(10)
        },
        notaDebitoDetCantidad: {
            type: DataTypes.INTEGER(11)
        },
        notaDebitoDetValorUnitario: {
            type: DataTypes.INTEGER(11)
        },
        notaDebitoDetIvaPorc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        articuloId: {
            type: DataTypes.INTEGER(10)
        },
        articuloCodigo: {
            type: DataTypes.STRING(50)
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100)
        },
        notaDebitoDetEntregado: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoDetEntregadoCantidad: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoDetCantidadSaldo: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoDetTotal: {
            type: DataTypes.DECIMAL(14, 2)
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        notaDebitoDetMontoUnitMonedaFuerte: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoDetMontoTotalMonedaFuerte: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoDetTotal: {
            type: DataTypes.DECIMAL(14, 2)
        },
        monedaFuerteId: {
            type: DataTypes.INTEGER(10)
        },
        monedaFuerteCod: {
            type: DataTypes.STRING(5)
        },
        monedaFuerteDecimales: {
            type: DataTypes.INTEGER(10)
        },
        notaDebitoTipoId: {
            type: DataTypes.INTEGER(10)
        },
        notaDebitoTipoDenominacion: {
            type: DataTypes.STRING(100)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        notaDebitoEstado: {
            type: DataTypes.INTEGER(10)
        },
        notaDebitoFecha: {
            type: DataTypes.DATEONLY
        },
    }, {
        timestamps: false,
        tableName: 'nota_debito_det_ext'
    });
    notaDebitoDetExt.removeAttribute('id');
    return notaDebitoDetExt;
};