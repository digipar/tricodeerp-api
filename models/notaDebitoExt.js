module.exports = function (sequelize, DataTypes) {
    var notaDebitoExt = sequelize.define('notaDebitoExt', {
        notaDebitoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'notaDebitoId'
        },
        notaDebitoFecha: {
            type: DataTypes.DATEONLY,
            allowNull: false,
            field: 'notaDebitoFecha'
        },
        notaDebitoFechaHora: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'notaDebitoFechaHora'
        },

        notaDebitoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'notaDebitoTipoId'
        },
        notaDebitoTipoDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'notaDebitoTipoDenominacion'
        },
        notaDebitoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'costoCentroId'
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        compraId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'compraId'
        },
    
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        notaDebitoTipoStock: {
            type: DataTypes.TINYINT(1)
        },
        notaDebitoEntregado: {
            type: DataTypes.TINYINT(1)
        },
        proveedorId: {
            type: DataTypes.INTEGER(10)
        },
        proveedorRazonSocial: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
        },
    
        compraCondicionDenominacion: {
            type: DataTypes.STRING(100)
        },
        compraCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'compraCondicionId'
        },
        compraVencimiento: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'compraVencimiento'
        },

        monedaId: {
            type: DataTypes.INTEGER(10)
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10)
        },
        monedaDenominacionPlural: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'monedaDenominacionPlural'
        },
        notaDebitoCantidadSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoValorUnitarioSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        notaDebitoValorTotalSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        proveedorDireccion: {
            type: DataTypes.STRING(100)
        },
        proveedorTelefono: {
            type: DataTypes.STRING(100)
        },
        proveedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'proveedorId'
        },
        proveedorRazonSocial: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'proveedorRazonSocial'
        },
        compraTipoDenominacion:{
            type: DataTypes.STRING(100),
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            field: 'appuserImagenTipo'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44)
        },
    }, {
        timestamps: false,
        tableName: 'nota_debito_ext'
    });
    notaDebitoExt.removeAttribute('id');
    return notaDebitoExt;
};