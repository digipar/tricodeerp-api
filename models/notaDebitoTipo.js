module.exports = function (sequelize, DataTypes) {
    var notaDebitoTipo = sequelize.define('notaDebitoTipo', {
        notaDebitoTipoId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            primaryKey: true,
            field: 'notaDebitoTipoId'
        },
 
        notaDebitoTipoDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'notaDebitoTipoDenominacion'
        },

    }, {
        timestamps: false,
        tableName: 'nota_debito_tipo'
    });
    notaDebitoTipo.removeAttribute('id');
    return notaDebitoTipo;
};