module.exports = function(sequelize, DataTypes) {
    var proveedor = sequelize.define('proveedor', {
        proveedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'proveedorId'
        },
     
        proveedorRazonSocial: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field: 'proveedorRazonSocial'
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'proveedorRuc'
        },
        proveedorDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'proveedorDireccion'
        },
        proveedorEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        proveedorTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'proveedorTelefono'
        },
            
    },{
        timestamps: false,
        tableName: 'proveedor'
    });
    proveedor.removeAttribute('id');
    return proveedor;
};