module.exports = function(sequelize, DataTypes) {
    var proveedorExt = sequelize.define('proveedorExt', {
        proveedorId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true
        },
     
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field: 'proveedorRazonSocialEstado'
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        proveedorRuc: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'proveedorRuc'
        },
        proveedorDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'proveedorDireccion'
        },
        proveedorEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        proveedorTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'proveedorTelefono'
        },
            
    },{
        timestamps: false,
        tableName: 'proveedor_ext'
    });
    proveedorExt.removeAttribute('id');
    return proveedorExt;
};