module.exports = function (sequelize, DataTypes) {
    var reciboCobro = sequelize.define('reciboCobro', {
        reciboCobroId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            primaryKey: true
        },
        reciboCobroFecha: {
            type: DataTypes.DATE,
            allowNull: false
        },
        reciboCobroFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboCobroDocNro: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: true
        },
        clienteId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboCobroEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        cobradorId: {
         type: DataTypes.INTEGER(10),
         allowNull: true   
        },
        transaccionId:{
            type: DataTypes.STRING(100),
            allowNull: false
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        
    }, {
        timestamps: false,
        tableName: 'recibo_cobro'
    });
    reciboCobro.removeAttribute('id');
    return reciboCobro;
};