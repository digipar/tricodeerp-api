module.exports = function (sequelize, DataTypes) {
    var reciboCobroExt = sequelize.define('reciboCobroExt', {
        reciboCobroId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true
        },
        reciboCobroFecha: {
            type: DataTypes.DATE,
        },
        reciboCobroFechaHoraRegistro: {
            type: DataTypes.DATE
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        reciboCobroDocNro: {
            type: DataTypes.DECIMAL(14, 2)
        },
        clienteId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        reciboCobroEstado: {
            type: DataTypes.TINYINT(1)
        },
        clienteRazonSocial: {
            type: DataTypes.STRING(100)
        },
        cuentaClienteCobroMontoSum: {
            type: DataTypes.DECIMAL(36, 2)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT(11)
        },
        monedaPrincipalId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaPrincipalDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaPrincipalDenominacion'
        },
        monedaPrincipalDenominacionPlural: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaPrincipalDenominacionPlural'
        },
        monedaPrincipalCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaPrincipalCod'
        },
        monedaPrincipalDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100)
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'documentoTipoId'
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        cobradorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoDocNro:{
            type: DataTypes.INTEGER(11)
        }
    }, {
        timestamps: false,
        tableName: 'recibo_cobro_ext'
    });
    reciboCobroExt.removeAttribute('id');
    return reciboCobroExt;
};