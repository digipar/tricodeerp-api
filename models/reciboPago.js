module.exports = function (sequelize, DataTypes) {
    var reciboPago = sequelize.define('reciboPago', {
        reciboPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true,
            primaryKey: true
        },
        reciboPagoFecha: {
            type: DataTypes.DATE,
            allowNull: false
        },
        reciboPagoFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboPagoDocNro: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: true
        },
        proveedorId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        reciboPagoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        transaccionId:{
            type: DataTypes.STRING(100),
            allowNull: false
        },
             
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
    }, {
        timestamps: false,
        tableName: 'recibo_pago'
    });
    reciboPago.removeAttribute('id');
    return reciboPago;
};