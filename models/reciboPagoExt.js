module.exports = function (sequelize, DataTypes) {
    var reciboPagoExt = sequelize.define('reciboPagoExt', {
        reciboPagoId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            primaryKey: true
        },
        reciboPagoFecha: {
            type: DataTypes.DATE,
        },
        reciboPagoFechaHoraRegistro: {
            type: DataTypes.DATE
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        reciboPagoDocNro: {
            type: DataTypes.DECIMAL(14, 2)
        },
        proveedorId: {
            type: DataTypes.INTEGER(10).UNSIGNED
        },
        reciboPagoEstado: {
            type: DataTypes.TINYINT(1)
        },
        proveedorRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        cuentaProveedorPagoMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        proveedorRuc: {
            type: DataTypes.STRING(100)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        monedaPrincipalId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaPrincipalDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaPrincipalDenominacion'
        },
        monedaPrincipalDenominacionPlural: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaPrincipalDenominacionPlural'
        },
        monedaPrincipalCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaPrincipalCod'
        },
        monedaPrincipalDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
    }, {
        timestamps: false,
        tableName: 'recibo_pago_ext'
    });
    reciboPagoExt.removeAttribute('id');
    return reciboPagoExt;
};