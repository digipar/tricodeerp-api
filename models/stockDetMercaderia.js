module.exports = function(sequelize, DataTypes) {
    var stockDetMercaderia = sequelize.define('stockDetMercaderia', {
        
        movimientoFecha: {
            type: DataTypes.DATE,
            allowNull: true
        },
        articuloId:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
        },
        articuloDenominacion:{
            type: DataTypes.STRING(150),
            allowNull: false
        },
        movimientoDetCantidad: {
            type: DataTypes.DECIMAL(32,0),
            allowNull: true,
        },
        movimientoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        movimientoTipoId: {
            type: DataTypes.STRING(17),
            allowNull: true,
        },
        depositoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
      
        depositoDenominacionEstado: {
            type: DataTypes.STRING(100),
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
        }
       
             
    },{
        timestamps: false,
        tableName: 'stock_det_mercaderia'
    });
    stockDetMercaderia.removeAttribute('id');
    return stockDetMercaderia;
};