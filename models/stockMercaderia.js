module.exports = function(sequelize, DataTypes) {
    var stockMercaderia = sequelize.define('stockMercaderia', {
        
        articuloId:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
            primaryKey:true
        },
        articuloDenominacion:{
            type: DataTypes.STRING(150),
            allowNull: false
        },
        stockMercaderiaStock: {
            type: DataTypes.DECIMAL(54,0),
            allowNull: true,
        },
        articuloEstado: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true,
        },
                   
    },{
        timestamps: false,
        tableName: 'stock_mercaderia'
    });
    stockMercaderia.removeAttribute('id');
    return stockMercaderia;
};