module.exports = function(sequelize, DataTypes) {
    var stockMercaderiaPorDeposito = sequelize.define('stockMercaderiaPorDeposito', {
        

        articuloId:{
            type: DataTypes.INTEGER(10),
        },
        articuloDenominacion:{
            type: DataTypes.STRING(150),
        },
        stockMercaderiaStock: {
            type: DataTypes.INTEGER(10)
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloEstado: {
            type: DataTypes.TINYINT(1),
        },
        categoriaId: {
            type: DataTypes.INTEGER(11),
        },
        categoriaDenominacion: {
            type: DataTypes.STRING(100),
        },
        marcaId: {
            type: DataTypes.INTEGER(11),
        },
        marcaDenominacion: {
            type: DataTypes.STRING(100),
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
        },
        depositoId: {
            type: DataTypes.INTEGER(11),
        },
        depositoDenominacionEstado: {
            type: DataTypes.STRING(100),
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
        }
       
             
    },{
        timestamps: false,
        tableName: 'stock_mercaderia_por_deposito'
    });
    stockMercaderiaPorDeposito.removeAttribute('id');
    return stockMercaderiaPorDeposito;
};