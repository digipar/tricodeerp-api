module.exports = function(sequelize, DataTypes) {
    var stockTransfer = sequelize.define('stockTransfer', {
        stockTransferId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'stockTransferId'
        },
        stockTransferFechaHora: {
            type: DataTypes.DATE,
            allowNull: true
        },
    
        stockTransferFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true
        },
        appuserIdOrigen:{
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field:'appuserIdOrigen'
        },
        depositoIdOrigen: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        depositoIdDestino: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        stockTransferEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        appuserIdDestino:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'appuserIdDestino'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
             
    },{
        timestamps: false,
        tableName: 'stock_transfer'
    });
    stockTransfer.removeAttribute('id');
    return stockTransfer;
};