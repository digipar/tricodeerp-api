module.exports = function(sequelize, DataTypes) {
    var stockTransferDet = sequelize.define('stockTransferDet', {
        stockTransferDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'stockTransferDetId'
        },
        stockTransferId:{
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'stockTransferId'
        },
        articuloId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field:'articuloId'
        },
        stockTransferCantidad: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
             
    },{
        timestamps: false,
        tableName: 'stock_transfer_det'
    });
    stockTransferDet.removeAttribute('id');
    return stockTransferDet;
};