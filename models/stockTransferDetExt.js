module.exports = function (sequelize, DataTypes) {
    var stockTransferDetExt = sequelize.define('stockTransferDetExt', {
        stockTransferDetId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'stockTransferDetId'
        },
        stockTransferId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'stockTransferId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'articuloId'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        stockTransferCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'stockTransferCantidad'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(200)
        }
   
    }, {
        timestamps: false,
        tableName: 'stock_transfer_det_ext'
    });
    stockTransferDetExt.removeAttribute('id');
    return stockTransferDetExt;
};