module.exports = function (sequelize, DataTypes) {
    var stockTransferExt = sequelize.define('stockTransferExt', {
        stockTransferId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'stockTransferId'
        },
        stockTransferFechaHora: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'stockTransferFechaHora'
        },
        stockTransferFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'stockTransferFechaHoraRegistro'
        },
        stockTransferEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        appuserIdOrigen: {
            type: DataTypes.INTEGER(11).UNSIGNED
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        depositoOrigenDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'depositoOrigenDenominacion'
        },
        depositoDestinoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'depositoDestinoDenominacion'
        },
        depositoIdOrigen: {
            type: DataTypes.INTEGER(10)
        },
        depositoIdDestino: {
            type: DataTypes.INTEGER(10)
        },
        appuserIdDestino: {
            type: DataTypes.INTEGER(11)
        },
        appuserOrigenImagenTipo: {
            type: DataTypes.STRING(30)
        },
        appuserDestinoImagenTipo: {
            type: DataTypes.STRING(30)
        },
        appuserOrigenImagenUrlNombre: {
            type: DataTypes.STRING(44)
        },
        appuserDestinoImagenUrlNombre: {
            type: DataTypes.STRING(44)
        },
        costoCentroDenominacionOrigen:{
            type: DataTypes.STRING(50)
        },
        costoCentroDenominacionDestino:{
            type: DataTypes.STRING(50)
        },
        appuserNombreOrigen:{
            type: DataTypes.STRING(50)
        },
        appuserNombreDestino:{
            type: DataTypes.STRING(50)
        }
    }, {
        timestamps: false,
        tableName: 'stock_transfer_ext'
    });
    stockTransferExt.removeAttribute('id');
    return stockTransferExt;
};