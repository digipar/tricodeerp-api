module.exports = function (sequelize, DataTypes) {
    var sucursal = sequelize.define('sucursal', {
        sucursalId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'sucursalId'
        },
        sucursalDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'sucursalDenominacion'
        },
        sucursalEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        }

    }, {
        timestamps: false,
        tableName: 'sucursal'
    });
    sucursal.removeAttribute('id');
    return sucursal;
};