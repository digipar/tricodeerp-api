module.exports = function(sequelize, DataTypes) {
    var timbrado = sequelize.define('timbrado', {
        timbradoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'timbradoId'
        },
        timbradoDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field:'timbradoDenominacion'
        },
        timbradoNumero: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
        },
        timbradoAutorizacionNro: {
            type: DataTypes.STRING(20),
            allowNull: true,
            field:'timbradoAutorizacionNro'
        },
        timbradoFechaDesde: {
            type: DataTypes.DATE,
            allowNull: true
        },
        timbradoFechaHasta: {
            type: DataTypes.DATE,
            allowNull: true
        },
        timbradoComprobanteTipo: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
        },
        timbradoEstablecimientoCod: {
            type: DataTypes.INTEGER(3),
            allowNull: true,
        },
        timbradoPuntoExpedNro: {
            type: DataTypes.INTEGER(3),
            allowNull: true,
        },
        timbradoPuntoExpedNroDesde: {
            type: DataTypes.DATE,
            allowNull: true
        },
        timbradoPuntoExpedNroHasta: {
            type: DataTypes.DATE,
            allowNull: true
        },
        timbradoPuntoExpedNroActual: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
        },
        timbradoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        appuserId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
    },{
        timestamps: false,
        tableName: 'timbrado'
    });
    timbrado.removeAttribute('id');
    return timbrado;
};