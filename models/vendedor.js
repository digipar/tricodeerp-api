module.exports = function(sequelize, DataTypes) {
    var vendedor = sequelize.define('vendedor', {
        vendedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'vendedorNombre'
        },
        vendedorEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        vendedorModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        vendedorErpId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: true
        },
     
    },{
        timestamps: false,
        tableName: 'vendedor'
    });
    vendedor.removeAttribute('id');
    return vendedor;
};