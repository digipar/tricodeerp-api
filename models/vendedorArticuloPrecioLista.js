module.exports = function(sequelize, DataTypes) {
    var vendedorArticuloPrecioLista = sequelize.define('vendedorArticuloPrecioLista', {
        vendedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field:'vendedorId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            field: 'articuloPrecioListaId'
        },
    },{
        timestamps: false,
        tableName: 'vendedor_articulo_precio_lista'
    });
    vendedorArticuloPrecioLista.removeAttribute('id');
    return vendedorArticuloPrecioLista;
};