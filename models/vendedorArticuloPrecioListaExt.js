module.exports = function(sequelize, DataTypes) {
    var vendedorArticuloPrecioListaExt = sequelize.define('vendedorArticuloPrecioListaExt', {
        vendedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            field:'vendedorId'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            field: 'articuloPrecioListaId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'vendedorNombre'
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        },
        articuloPrecioListaTasaFinancMensual: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaTasaFinancMensual'
        },
        articuloPrecioListaDescuentoPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaDescuentoPorcMax'
        }
    },{
        timestamps: false,
        tableName: 'vendedor_articulo_precio_lista_ext'
    });
    vendedorArticuloPrecioListaExt.removeAttribute('id');
    return vendedorArticuloPrecioListaExt;
};