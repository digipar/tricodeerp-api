module.exports = function(sequelize, DataTypes) {
    var vendedorExt = sequelize.define('vendedorExt', {
        vendedorId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'vendedorNombre'
        },
        vendedorEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        vendedorModificadoEn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        vendedorErpId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        sucursalId: {
            type: DataTypes.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        sucursalDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'sucursalDenominacion'
        },
     
    },{
        timestamps: false,
        tableName: 'vendedor_ext'
    });
    vendedorExt.removeAttribute('id');
    return vendedorExt;
};