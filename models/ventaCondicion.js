module.exports = function(sequelize, DataTypes) {
    var ventaCondicion = sequelize.define('ventaCondicion', {
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'ventaCondicionId'
        },
        ventaCondicionDescripcion: {
            type: DataTypes.STRING(200),
            allowNull: true,
            field: 'ventaCondicionDescripcion'
        },
        ventaCondicionEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 1,
        },
        ventaCondicionErpId: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
        ventaCondicionAcobrar: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        ventaCondicionPagare: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
    },{
        timestamps: false,
        tableName: 'venta_condicion'
    });
    ventaCondicion.removeAttribute('id');
    return ventaCondicion;
}