module.exports = function (sequelize, DataTypes) {
    var ventaFactura = sequelize.define('ventaFactura', {
        ventaFacturaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaFacturaId'
        },
        ventaFacturaFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaFacturaFechaHoraRegistro'
        },
        ventaFacturaFecha: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaFacturaFecha'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaDescPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: 0,
            field: 'articuloPrecioListaDescPorcMax'
        },
        ventaFacturaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        ventaFacturaDescPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'clienteId'
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaCondicionId'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        ventaFacturaCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaCotizacion'
        },
        ventaFacturaNumero: {
            type: DataTypes.STRING(20)
        },
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'clienteSucursalId'
        },
    }, {
        timestamps: false,
        tableName: 'venta_factura'
    });
    ventaFactura.removeAttribute('id');
    return ventaFactura;
};