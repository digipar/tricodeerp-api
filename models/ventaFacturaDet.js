module.exports = function(sequelize, DataTypes) {
    var ventaFacturaDet = sequelize.define('ventaFacturaDet', {
        ventaFacturaDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'ventaFacturaDetId'
        },
        ventaFacturaId:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'ventaFacturaId'
        },
        articuloId:{
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field:'articuloId'
        },
        ventaFacturaDetCantidad:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'ventaFacturaDetCantidad'
        },
        ventaFacturaDetPrecioUnit: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'ventaFacturaDetPrecioUnit'
        },
        ventaFacturaDetImpuestoPorc: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'ventaFacturaDetImpuestoPorc'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        ventaFacturaDetCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetCotizacion'
        },
    },{
        timestamps: false,
        tableName: 'venta_factura_det'
    });
    ventaFacturaDet.removeAttribute('id');
    return ventaFacturaDet;
};