module.exports = function (sequelize, DataTypes) {
    var ventaFacturaDetExt = sequelize.define('ventaFacturaDetExt', {
        ventaFacturaDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaFacturaDetId'
        },
        ventaFacturaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaFacturaId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        articuloDenominacionCodigo: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        ventaFacturaDetCantidad: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaFacturaDetCantidad'
        },
        ventaFacturaDetPrecioUnit: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetPrecioUnit'
        },
        ventaFacturaDetImpuestoPorc: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetImpuestoPorc'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        ventaFacturaDetTotal: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetTotal'
        },
        ventaFacturaDetImpuestoTotalMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetImpuestoTotalMonto'
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        ventaFacturaDetImpuestoUnitMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetImpuestoUnitMonto'
        },
        ventaFacturaNetoUnitMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaNetoUnitMonto'
        },
        monedaAnteriorId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaAnteriorId'
        },
        ventaFacturaDetCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetCotizacion'
        },

    }, {
        timestamps: false,
        tableName: 'venta_factura_det_ext'
    });
    ventaFacturaDetExt.removeAttribute('id');
    return ventaFacturaDetExt;
};