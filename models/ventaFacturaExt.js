module.exports = function (sequelize, DataTypes) {
    var ventaFacturaExt = sequelize.define('ventaFacturaExt', {
        ventaFacturaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaFacturaId'
        },
        ventaFacturaFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaFacturaFechaHoraRegistro'
        },
        ventaFacturaFecha: {
            type: DataTypes.DATEONLY,
            allowNull: true,
            field: 'ventaFacturaFecha'
        },
        ventaFacturaVencimiento: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaFacturaVencimiento'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        },
        articuloPrecioListaDescPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: 0,
            field: 'articuloPrecioListaDescPorcMax'
        },
        ventaFacturaEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        ventaFacturaDescPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'clienteId'
        },
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteRazonSocialEstado'
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'clienteDocNro'
        },
        clienteDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'clienteDireccion'
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'documentoTipoId'
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'vendedorNombre'
        },
        ventaFacturaImpuestoMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaImpuestoMonto'
        },
        ventaFacturaPrecioTotal: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaPrecioTotal'
        },

        clienteTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteTelefono'
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaCondicionId'
        },
        ventaCondicionDescripcion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'ventaCondicionDescripcion'
        },
      
    
        ventaFacturaDetImpuestoUnitMontoSum: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaDetImpuestoUnitMontoSum'
        },
        ventaFacturaNetoUnitMontoSum: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaNetoUnitMontoSum'
        },
        ventaFacturaPrecioUnitSum: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaPrecioUnitSum'
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: true,
            field: 'monedaDenominacion'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaCod'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        monedaDenominacionPlural: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'monedaDenominacionPlural'
        },
        ventaFacturaCotizacion: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaFacturaCotizacion'
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        ventaPedidoRecargoPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoRecargoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        ventaFacturaNumero: {
            type: DataTypes.STRING(20)
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        ventaFacturaTotalConDesc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
        },
        clienteSucursalNombreRazonSocial: {
            type: DataTypes.STRING(200)
        },
        clienteSucursalDireccion: {
            type: DataTypes.STRING(200)
        },
        clienteSucursalTelefono: {
            type: DataTypes.STRING(100)
        },
    }, {
        timestamps: false,
        tableName: 'venta_factura_ext'
    });
    ventaFacturaExt.removeAttribute('id');
    return ventaFacturaExt;
};