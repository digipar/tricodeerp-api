module.exports = function (sequelize, DataTypes) {
    var ventaPedido = sequelize.define('ventaPedido', {
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaPedidoId'
        },
        ventaPedidoFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoFechaHoraRegistro'
        },
        ventaPedidoFecha: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'ventaPedidoFecha'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaDescPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: true,
            defaultValue: 0,
            field: 'articuloPrecioListaDescPorcMax'
        },
 
        ventaPedidoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        ventaPedidoTasaFinancMensual: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            defaultValue: 0,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        ventaPedidoDescPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'clienteId'
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaCondicionId'
        },
        ventaPedidoDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        ventaPedidoAux1: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        ventaPedidoRecargoPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoRecargoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoDescripcion: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        ventaPedidoVencimiento: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoVencimiento'
        },
        ventaPedidoCotizacion: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            defaultValue: '1',
        },
        ventaTipoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        ventaFacturaId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
            allowNull: true
        },
    }, {
        timestamps: false,
        tableName: 'venta_pedido'
    });
    ventaPedido.removeAttribute('id');
    return ventaPedido;
};