module.exports = function (sequelize, DataTypes) {
    var ventaPedidoAut = sequelize.define('ventaPedidoAut', {
        ventaPedidoAutId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
        },
        ventaPedidoAutTipo: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        ventaPedidoAutEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
        },
        ventaPedidoAutCreadoFechaHora: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
        ventaPedidoAutProcesadoAppuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
        },
        ventaPedidoAutProcesadoFechaHora: {
            type: DataTypes.DATE,
            allowNull: false,
        }
    }, {
        timestamps: false,
        tableName: 'venta_pedido_aut'
    });
    ventaPedidoAut.removeAttribute('id');
    return ventaPedidoAut;
};