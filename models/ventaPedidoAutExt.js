module.exports = function (sequelize, DataTypes) {
    var ventaPedidoAutExt = sequelize.define('ventaPedidoAutExt', {
        ventaPedidoAutId: {
            type: DataTypes.INTEGER(11),
            primaryKey: true,
            field: 'ventaPedidoAutId'
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoAutTipo: {
            type: DataTypes.STRING(50)
        },
        ventaPedidoAutEstado: {
            type: DataTypes.TINYINT(1)
        },
        ventaPedidoAutCreadoFechaHora: {
            type: DataTypes.DATEONLY,
        },
        ventaPedidoAutProcesadoAppuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        ventaPedidoAutProcesadoFechaHora: {
            type: DataTypes.DATE,
        },
        ventaPedidoAutProcesadoAppuserNombre: {
            type: DataTypes.STRING(100),
        },
        ventaPedidoAutProcesadoAppuserImagenTipo: {
            type: DataTypes.STRING(30),
        },
        ventaPedidoAutProcesadoAppuserImagenUrlNombre: {
            type: DataTypes.STRING(44)
        },
        ventaPedidoDescPorc: {
            type: DataTypes.DECIMAL(14,2)
        },
        articuloPrecioListaDescPorcMax: {
            type: DataTypes.DECIMAL(14,2)
        },
        vendedorNombre:{
            type: DataTypes.STRING(100),
        },
        clienteRazonSocialEstado:{
            type: DataTypes.STRING(100),
        },
        ventaPedidoFecha: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoFecha'
        },
        monedaId: {
            type: DataTypes.INTEGER(11)
        },
        monedaCod:{
            type: DataTypes.STRING(20),
        },
        monedaDecimales:{
            type: DataTypes.INTEGER(10),
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(11)
        },
        ventaCondicionDescripcion:{
            type: DataTypes.STRING(100),
        },
        ventaPedidoPrecioTotal: {
            type: DataTypes.DECIMAL(14,2)
        },
        fondoMov:{
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoTotalConDescRecarMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
        },
        ventaPedidoAutProcesadoFechaHora: {
            type: DataTypes.DATE,
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44)
        },
        ventaPedidoDescMonto: {
            type: DataTypes.DECIMAL(14, 2)
        }
    }, {
        timestamps: false,
        tableName: 'venta_pedido_aut_ext'
    });
    ventaPedidoAutExt.removeAttribute('id');
    return ventaPedidoAutExt;
};