module.exports = function(sequelize, DataTypes) {
    var ventaPedidoDet = sequelize.define('ventaPedidoDet', {
        ventaPedidoDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'ventaPedidoDetId'
        },
        ventaPedidoId:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'ventaPedidoId'
        },
        articuloId:{
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field:'articuloId'
        },
        ventaPedidoDetCantidad:{
            type: DataTypes.DECIMAL(14,2),
            allowNull: true,
            field:'ventaPedidoDetCantidad'
        },
        ventaPedidoDetPrecioUnitBase: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'ventaPedidoDetPrecioUnitBase'
        },
        ventaPedidoDetImpuestoPorc: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field:'ventaPedidoDetImpuestoPorc'
        },
        transaccionId:{
            type: DataTypes.STRING(20),
            allowNull: false
        },
        ventaPedidoDetEspecificacion:{
            type: DataTypes.STRING(200),
            allowNull: false
        },
        ventaPedidoDetFinancMonto: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field: 'ventaPedidoDetFinancMonto'
        },
        ventaPedidoDetEntregado:{
            type: DataTypes.TINYINT(1),
            allowNull: true
        },
        depositoId:{
            type: DataTypes.INTEGER(10),
            allowNull: true
        }
    },{
        timestamps: false,
        tableName: 'venta_pedido_det'
    });
    ventaPedidoDet.removeAttribute('id');
    return ventaPedidoDet;
};