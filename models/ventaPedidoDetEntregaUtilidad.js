module.exports = function(sequelize, DataTypes) {
    var ventaPedidoDetEntregaUtilidad = sequelize.define('ventaPedidoDetEntregaUtilidad', {
        compraDetRecepcionId: {
            type: DataTypes.INTEGER(10)
        },
        ventaPedidoDetEntregaCantidadSum:{
            type: DataTypes.DECIMAL(14,2)
        },
        ventaPedidoDetPrecioUnitNeto: {
            type: DataTypes.DECIMAL(14,2)
        },
        ventaPedidoRecepcionPecioTotalNetoSum: {
            type: DataTypes.DECIMAL(14,2)
        },
        compraDetPrecioUnitario: {
            type: DataTypes.DECIMAL(14,2)
        },
        compraDetCostoTotal: {
            type: DataTypes.DECIMAL(14,2)
        },
        utilidadNeto:{
            type: DataTypes.DECIMAL(14,2)
        },
        utilidadBruta:{
            type: DataTypes.DECIMAL(14,2)
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100)
        },
        articuloCodigo: {
            type: DataTypes.STRING(100)
        },
        costoCentroId: { 
            type: DataTypes.INTEGER(10)
        },
        ventaPedidoDetTotalSinIva:{
            type: DataTypes.DECIMAL(14,2)
        }
    },{
        timestamps: false,
        tableName: 'venta_pedido_det_entrega_utilidad'
    });
    ventaPedidoDetEntregaUtilidad.removeAttribute('id');
    return ventaPedidoDetEntregaUtilidad;
};