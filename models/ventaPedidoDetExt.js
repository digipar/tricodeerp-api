module.exports = function (sequelize, DataTypes) {
    var ventaPedidoDetExt = sequelize.define('ventaPedidoDetExt', {
        ventaPedidoDetId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaPedidoDetId'
        },
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaPedidoId'
        },
        articuloId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'articuloId'
        },
        articuloDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        ventaPedidoDetCantidad: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'ventaPedidoDetCantidad'
        },
        ventaPedidoDetPrecioUnit: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaPedidoDetPrecioUnit'
        },
        ventaPedidoDetImpuestoPorc:{
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaPedidoDetImpuestoPorc'
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        ventaPedidoDetTotal: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            field: 'ventaPedidoDetTotal'
        },
        ventaPedidoDetImpuestoMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: false,
            field: 'ventaPedidoDetImpuestoMonto'
        },
        articuloCodigo: {
            type: DataTypes.STRING(64),
            allowNull: true
        },
        ventaPedidoDetPrecioUnitBase: {
            type: DataTypes.DECIMAL(14,2)
        },
        ventaPedidoDetEntregado:{
            type: DataTypes.TINYINT(1)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10)
        },
        monedaId: {
            type: DataTypes.INTEGER(10)
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
        },
        monedaCod: {
            type: DataTypes.STRING(5)
        },
        ventaPedidoDetEntregaCantidadSum:{
            type: DataTypes.DECIMAL(14,2)
        }
    }, {
        timestamps: false,
        tableName: 'venta_pedido_det_ext'
    });
    ventaPedidoDetExt.removeAttribute('id');
    return ventaPedidoDetExt;
};