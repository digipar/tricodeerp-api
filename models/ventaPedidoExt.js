const { TINYINT } = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    var ventaPedidoExt = sequelize.define('ventaPedidoExt', {
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'ventaPedidoId'
        },
        ventaPedidoFechaHoraRegistro: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoFechaHoraRegistro'
        },
        ventaPedidoFecha: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'ventaPedidoFecha'
        },
        articuloPrecioListaId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'articuloPrecioListaId'
        },
        articuloPrecioListaDenominacion: {
            type: DataTypes.STRING(150),
            allowNull: true,
            field: 'articuloPrecioListaDenominacion'
        },
        articuloPrecioListaDescPorcMax: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: 0,
            field: 'articuloPrecioListaDescPorcMax'
        },
        ventaPedidoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false,
            defaultValue: 0,
        },
        appuserId: {
            type: DataTypes.INTEGER(11).UNSIGNED,
            allowNull: false,
            field: 'appuserId'
        },
        ventaPedidoDescPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        transaccionId: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        clienteId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: 'clienteId'
        },
    
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(100)
        },
        clienteDocNro: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'clienteDocNro'
        },
        clienteDireccion: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'clienteDireccion'
        },
        documentoTipoId: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'documentoTipoId'
        },
        vendedorId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'vendedorId'
        },
        vendedorNombre: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'vendedorNombre'
        },
        ventaPedidoImpuestoMonto: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'ventaPedidoImpuestoMonto'
        },
        ventaPedidoPrecioTotal: {
            type: DataTypes.DECIMAL(14, 2),
            allowNull: true,
            field: 'ventaPedidoPrecioTotal'
        },
        ventaPedidoVencimiento: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'ventaPedidoVencimiento'
        },
        monedaDenominacionPlural: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'monedaDenominacionPlural'
        },
        clienteTelefono: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'clienteTelefono'
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            field: 'ventaCondicionId'
        },
        ventaCondicionDescripcion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field: 'ventaCondicionDescripcion'
        },

        ventaPedidoDenominacion: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        monedaId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            field: 'monedaId'
        },
        monedaDenominacion: {
            type: DataTypes.STRING(50),
            allowNull: false,
            field: 'monedaDenominacion'
        },
        monedaDecimales: {
            type: DataTypes.INTEGER(10),
            allowNull: true,
            defaultValue: '0',
            field: 'monedaDecimales'
        },
        monedaPrincipal: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: '1',
            field: 'monedaPrincipal'
        },
        monedaCod: {
            type: DataTypes.STRING(5),
            allowNull: true,
            field: 'monedaCod'
        },
        ventaPedidoAux1: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
        ventaPedidoRecargoPorc: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoRecargoMonto: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        ventaPedidoDescripcion: {
            type: DataTypes.STRING(200),
            allowNull: true,
        },
        ventaCondicionAcobrar: {
            type: DataTypes.TINYINT(1)
        },
        ventaPedidoTasaFinancMensual: {
            type: DataTypes.DECIMAL(14, 2)
        },
        costoCentroId: {
            type: DataTypes.INTEGER(10),
            allowNull: false
        },
        costoCentroDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        ventaPedidoCotizacion: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoCuentaGenerada: {
            type: DataTypes.TINYINT(1)
        },
        ventaTipoId: {
            type: DataTypes.INTEGER(10)
        },
        appuserImagenTipo: {
            type: DataTypes.STRING(30),
            allowNull: true,
            field: 'appuserImagenTipo'
        },
        appuserImagenUrlNombre: {
            type: DataTypes.STRING(44),
            allowNull: true
        },
        appuserNombre: {
            type: DataTypes.STRING(100),
            allowNull: false,
            field: 'appuserNombre'
        },
        ventaTipoStock: {
            type: DataTypes.TINYINT(1),
            allowNull: false
        },
        ventaPedidoEntregado: {
            type: DataTypes.TINYINT(1)
        },
        ventaPedidoTotalConDesc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoFacturado: {
            type: DataTypes.TINYINT(1)
        },
        ventaPedidoDescMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoTotalConDescRecarMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        fondoMov:{
            type: DataTypes.INTEGER(11)
        },
        ventaPedidoAutEstado:{
            type: DataTypes.TINYINT(1)
        },
        ventaPedidoTotalSinIva: {
            type: DataTypes.DECIMAL(14, 2)
        },
        clienteSucursalId: {
            type: DataTypes.INTEGER(10),
        },
        clienteSucursalNombreRazonSocial: {
            type: DataTypes.STRING(200)
        },
        clienteSucursalDireccion: {
            type: DataTypes.STRING(200)
        },
        clienteSucursalTelefono: {
            type: DataTypes.STRING(100)
        },
        ventaFacturaNumero: {
            type: DataTypes.STRING()
        },
        ventaFacturaId: {
            type: DataTypes.STRING()
        },
    }, {
        timestamps: false,
        tableName: 'venta_pedido_ext'
    });
    ventaPedidoExt.removeAttribute('id');
    return ventaPedidoExt;
};