module.exports = function (sequelize, DataTypes) {
    var ventaPedidoPorMes = sequelize.define('ventaPedidoPorMes', {
      
        ventaAnio: {
            type: DataTypes.DATE,
            allowNull: false,
            field: 'ventaAnio'
        },
        ventaMesNumero: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            field: 'ventaMesNumero'
        },
     
        ventaSuma: {
            type: DataTypes.DECIMAL(14,2),
            allowNull: false,
            defaultValue: 0,
        },
        ventaMes: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
    }, {
        timestamps: false,
        tableName: 'venta_pedido_sum_por_mes'
    });
    ventaPedidoPorMes.removeAttribute('id');
    return ventaPedidoPorMes;
};