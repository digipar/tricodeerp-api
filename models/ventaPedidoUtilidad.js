module.exports = function (sequelize, DataTypes) {
    var ventaPedidoUtilidad = sequelize.define('ventaPedidoUtilidad', {
        ventaPedidoId: {
            type: DataTypes.INTEGER(10),
            primaryKey: true
        },
        ventaPedidoFecha: {
            type: DataTypes.DATEONLY
        },
        ventaPedidoVencimiento: {
            type: DataTypes.DATEONLY
        },

        ventaPedidoTotalSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoCostoMontoSum: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoUtilidadMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaPedidoUtilidadBrutaMonto: {
            type: DataTypes.DECIMAL(14, 2)
        },
        utilidadBrutaPorc: {
            type: DataTypes.DECIMAL(14, 2)
        },
        ventaCondicionId: {
            type: DataTypes.INTEGER(10)
        },
        ventaCondicionDescripcion: {
            type: DataTypes.STRING(50)
        },
        clienteId: {
            type: DataTypes.INTEGER(10)
        },
        clienteRazonSocialEstado: {
            type: DataTypes.STRING(50)
        },
        vendedorId: {
            type: DataTypes.INTEGER(10)
        },
        vendedorNombre: {
            type: DataTypes.STRING(50)
        },
        ventaPedidoEstado: {
            type: DataTypes.TINYINT(1)
        },
        vendedorNombre: {
            type: DataTypes.STRING(100)
        },
        ventaPedidoTotalSinIva: {
            type: DataTypes.DECIMAL(14, 2)
        }
    }, {
        timestamps: false,
        tableName: 'venta_pedido_utilidad'
    });
    ventaPedidoUtilidad.removeAttribute('id');
    return ventaPedidoUtilidad;
};