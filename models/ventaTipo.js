module.exports = function(sequelize, DataTypes) {
    var ventaTipo = sequelize.define('ventaTipo', {
        ventaTipoId: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement:true,
            field:'ventaTipoId'
        },
        ventaTipoDenominacion: {
            type: DataTypes.STRING(100),
            allowNull: true,
            field:'ventaTipoDenominacion'
        },
        ventaTipoEstado: {
            type: DataTypes.TINYINT(1),
            allowNull: false
          },
        ventaTipoStock: {
            type: DataTypes.INTEGER(1),
            allowNull: false
          },
    },{
        timestamps: false,
        tableName: 'ventaTipo'
    });
    ventaTipo.removeAttribute('id');
    return ventaTipo;
};