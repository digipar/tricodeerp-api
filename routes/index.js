var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var appuser = require('../controllers/appuser');
const appaccess = require('../controllers/appaccess');
const appuserRol = require('../controllers/appuserRol');
const appuserRolDet = require('../controllers/appuserRolDet');
const appuserExt = require('../controllers/appuserExt');
const appuserRolExt = require('../controllers/appuserRolExt');
const appuserRolDetExt = require('../controllers/appuserRolDetExt');
const marca = require('../controllers/marca');
const articulo = require('../controllers/articulo');
const articuloCuExt = require('../controllers/articuloCuExt');
const categoria = require('../controllers/categoria');
const articuloCategoria = require('../controllers/articuloCategoria');
const vendedor = require('../controllers/vendedor');
const vendedorExt = require('../controllers/vendedorExt');
const appparam = require('../controllers/appparam');
const articuloPrecioLista = require('../controllers/articuloPrecioLista');
const articuloPrecioListaDet = require('../controllers/articuloPrecioListaDet');
const moneda = require('../controllers/moneda');
const articuloImagen = require('../controllers/articuloImagen');
const articuloPrecioListaDetExt = require('../controllers/articuloPrecioListaDetExt');
const articuloCatalogoExt = require('../controllers/articuloCatalogoExt');
const articuloPrecioListaFinancParam = require('../controllers/articuloPrecioListaFinancParam');
const articuloPrecioListaFinancParamExt = require('../controllers/articuloPrecioListaFinancParamExt');
const articuloPrecioListaFinanc = require('../controllers/articuloPrecioListaFinanc');
const vendedorArticuloPrecioLista = require('../controllers/vendedorArticuloPrecioLista');
const vendedorArticuloPrecioListaExt = require('../controllers/vendedorArticuloPrecioListaExt');
const deposito = require('../controllers/deposito');
const sucursal = require('../controllers/sucursal');
const cliente = require('../controllers/cliente');
const documentoTipo = require('../controllers/documentoTipo');
const ventaPedido = require('../controllers/ventaPedido');
const compra = require('../controllers/compra');
const fondoMovTipo = require('../controllers/fondoMovTipo');
const ventaPedidoExt = require('../controllers/ventaPedidoExt');
const ventaPedidoDetExt = require('../controllers/ventaPedidoDetExt');
const compraExt = require('../controllers/compraExt');
const articuloExt = require('../controllers/articuloExt');
const fondoMovExt = require('../controllers/fondoMovExt');
const ventaFactura = require('../controllers/ventaFactura');
const ventaFacturaDetExt = require('../controllers/ventaFacturaDetExt');
const ventaFacturaExt = require('../controllers/ventaFacturaExt');
const cotizacion = require('../controllers/cotizacion');
const cotizacionExt = require('../controllers/cotizacionExt');
const cotizacionUltimaFecha = require('../controllers/cotizacionUltimaFecha');
const ventaCondicion = require('../controllers/ventaCondicion');
const proveedor = require('../controllers/proveedor');
const compraTipo = require('../controllers/compraTipo');
const compraCondicion = require('../controllers/compraCondicion');
const stockDetMercaderia = require('../controllers/stockDetMercaderia');
const stockMercaderia = require('../controllers/stockMercaderia');
const cuentaProveedorEstado = require('../controllers/cuentaProveedorEstado');
const cuentaProveedorPago = require('../controllers/cuentaProveedorPago');
const fondo = require('../controllers/fondo');
const reciboPago = require('../controllers/reciboPago');
const reciboCobro = require('../controllers/reciboCobro');
const reciboCobroExt = require('../controllers/reciboCobroExt');
const reciboPagoExt = require('../controllers/reciboPagoExt');
const cuentaProveedorPagoExt = require('../controllers/cuentaProveedorPagoExt');
const cuentaClienteCobroExt = require('../controllers/cuentaClienteCobroExt');
const cuentaClienteEstado = require('../controllers/cuentaClienteEstado');
const cuentaClienteCobro = require('../controllers/cuentaClienteCobro');
const clienteExt = require('../controllers/clienteExt');
const proveedorExt = require('../controllers/proveedorExt');
const cuentaClienteAcobrar = require('../controllers/cuentaClienteAcobrar');
const costoCentro = require('../controllers/costoCentro');
const cobrador = require('../controllers/cobrador');
const cuentaClienteAcobrarExt = require('../controllers/cuentaClienteAcobrarExt');
const stockTransferExt = require('../controllers/stockTransferExt');
const stockTransferDetExt = require('../controllers/stockTransferDetExt');
const stockTransfer = require('../controllers/stockTransfer');
const cuentaClienteAcobrarEstado = require('../controllers/cuentaClienteAcobrarEstado');
const depositoExt = require('../controllers/depositoExt');
const cajaExt = require('../controllers/cajaExt');
const stockMercaderiaPorDeposito = require('../controllers/stockMercaderiaPorDeposito');
const notaCreditoTipo = require('../controllers/notaCreditoTipo');
const notaDebitoTipo = require('../controllers/notaDebitoTipo');
const notaCredito = require('../controllers/notaCredito');
const notaDebito = require('../controllers/notaDebito');
const fondoEntidad = require('../controllers/fondoEntidad');
const notaCreditoDet = require('../controllers/notaCreditoDet');
const notaCreditoExt = require('../controllers/notaCreditoExt');
const notaDebitoExt = require('../controllers/notaDebitoExt');
const ventaPedidoDetEntregaUtilidad = require('../controllers/ventaPedidoDetEntregaUtilidad');
const ventaPedidoUtilidad = require('../controllers/ventaPedidoUtilidad');
const cuentaProveedorApagarEstado = require('../controllers/cuentaProveedorApagarEstado');
const notaCreditoDetExt = require('../controllers/notaCreditoDetExt');
const notaDebitoDetExt = require('../controllers/notaDebitoDetExt');
const notaCreditoDetRecepcion = require('../controllers/notaCreditoDetRecepcion');
const loteDisponible = require('../controllers/loteDisponible');
const fondoMov = require('../controllers/fondoMov');
const fondoMovValorDet = require('../controllers/fondoMovValorDet');
const fondoValor = require('../controllers/fondoValor');
const caja = require('../controllers/caja');
const compraDetExt = require('../controllers/compraDetExt');
const fondoValorExt = require('../controllers/fondoValorExt');
const fondoMovValorDetExt = require('../controllers/fondoMovValorDetExt');
const fondoMovTipoDet = require('../controllers/fondoMovTipoDet');
const fondoMovTipoDetExt = require('../controllers/fondoMovTipoDetExt');
const fondoMovTipoSaldo = require('../controllers/fondoMovTipoSaldo');
const fondoMovTipoExt = require('../controllers/fondoMovTipoExt');
const cuentaCobro = require('../controllers/cuentaCobro');
const cuentaCobroExt = require('../controllers/cuentaCobroExt');
const appuserCaja = require('../controllers/appuserCaja');
const appuserCajaExt = require('../controllers/appuserCajaExt');
const appuserSucursal = require('../controllers/appuserSucursal');
const appuserSucursalExt = require('../controllers/appuserSucursalExt');
const compensaCliente = require('../controllers/compensaCliente');
const compensaClienteExt = require('../controllers/compensaClienteExt');
const compensaClienteDet = require('../controllers/compensaClienteDet');
const compensaClienteDetExt = require('../controllers/compensaClienteDetExt');
const cuentaPago = require('../controllers/cuentaPago');
const cuentaPagoExt = require('../controllers/cuentaPagoExt');
const fondoTransf = require('../controllers/fondoTransf');
const fondoTransfExt = require('../controllers/fondoTransfExt');
const fondoDeposito = require('../controllers/fondoDeposito');
const fondoDepositoExt = require('../controllers/fondoDepositoExt');
const compraGastoCuenta = require('../controllers/compraGastoCuenta');
const cuentaClienteAcobrarEstadoV2 = require('../controllers/cuentaClienteAcobrarEstadoV2');
const cuentaClienteEstadoV2 = require('../controllers/cuentaClienteEstadoV2');
const fondoCapitalizacion = require('../controllers/fondoCapitalizacion');
const fondoCapitalizacionExt = require('../controllers/fondoCapitalizacionExt');
const cuentaProveedorEstadoV2 = require('../controllers/cuentaProveedorEstadoV2');
const cuentaProveedorApagarEstadoV2 = require('../controllers/cuentaProveedorApagarEstadoV2');
const compraDetRecepcion = require('../controllers/compraDetRecepcion');
const compensaProveedor = require('../controllers/compensaProveedor');
const compensaProveedorDet = require('../controllers/compensaProveedorDet');
const compensaProveedorExt = require('../controllers/compensaProveedorExt');
const compensaProveedorDetExt = require('../controllers/compensaProveedorDetExt');
const ventaPedidoAutExt = require('../controllers/ventaPedidoAutExt');
const ventaPedidoAut = require('../controllers/ventaPedidoAut');
const notaDebitoDetEntrega = require('../controllers/notaDebitoDetEntrega');
const clienteSucursal = require('../controllers/clienteSucursal');
const clienteSucursalExt = require('../controllers/clienteSucursalExt');

// Transacciones
const reciboPagoTrans = require('../transaction/reciboPago');
const reciboCobroTrans = require('../transaction/reciboCobro');
const compraRecepcionTrans = require('../transaction/compraRecepcion');
const ventaFacturaTrans = require('../transaction/ventaFactura');
const ventaPedidoTrans = require('../transaction/ventaPedido');
const compraTrans = require('../transaction/compra');
const stockTransferTrans = require('../transaction/stockTransfer');
const notaCreditoTrans = require('../transaction/notaCredito');
const notaDebitoTrans = require('../transaction/notaDebito');
const fondoMovTrans = require('../transaction/fondoMov');
const compensaClienteTrans = require('../transaction/compensaCliente');
const compensaProveedorTrans = require('../transaction/compensaProveedor');
const cobroClienteMovilTrans = require('../transaction/cobroClienteMovil');
const compensaClienteMovTipo = require('../controllers/compensaClienteMovTipo');
const ventaPedidoPorMes = require('../controllers/ventaPedidoPorMes');
const authentication = require('../authentication/authentication');



module.exports = function (router) {
  // cliente sucursal ext
  router.get('/cliente-sucursal-ext', clienteSucursalExt.findAll);
  router.post('/cliente-sucursal-ext-filter', clienteSucursalExt.findAllByFilter);
  router.get('/cliente-sucursal-ext/:id', clienteSucursalExt.findById);

  // cliente entrega
  router.get('/cliente-sucursal', clienteSucursal.findAll);
  router.get('/cliente-sucursal/:id', clienteSucursal.findById);
  router.post('/cliente-sucursal-filter', clienteSucursal.findAllByFilter);
  router.post('/cliente-sucursal', clienteSucursal.create);
  router.put('/cliente-sucursal/:id', clienteSucursal.update);

  // nota debito det entrega
  router.get('/nota-debito-det-entrega', notaDebitoDetEntrega.findAll);
  router.get('/nota-debito-det-entrega/:id', notaDebitoDetEntrega.findById);
  router.post('/nota-debito-det-entrega-filter', notaDebitoDetEntrega.findAllByFilter);
  router.post('/nota-debito-det-entrega', notaDebitoDetEntrega.create);
  router.put('/nota-debito-det-entrega/:id', notaDebitoDetEntrega.update);
  router.post('/nota-debito-det-entrega-bulkcreate', notaDebitoDetEntrega.bulkCreate);

  //VentaPedidoPorMes
  router.get('/venta-pedido-sum-por-mes/:anio', ventaPedidoPorMes.findAll);
  router.get('/venta-pedido-sum-por-mes/:id', ventaPedidoPorMes.findById);
  router.post('/venta-pedido-sum-por-mes-filter', ventaPedidoPorMes.findAllByFilter);

  // compensa cliente mov tipo
  router.get('/compensa-cliente-mov-tipo', compensaClienteMovTipo.findAll);
  router.get('/compensa-cliente-mov-tipo/:id', compensaClienteMovTipo.findById);
  router.post('/compensa-cliente-mov-tipo-filter', compensaClienteMovTipo.findAllByFilter);
  router.post('/compensa-cliente-mov-tipo', compensaClienteMovTipo.create);
  router.put('/compensa-cliente-mov-tipo/:id', compensaClienteMovTipo.update);

  //venta pedido aut
  router.get('/venta-pedido-aut', ventaPedidoAut.findAll);
  router.get('/venta-pedido-aut/:id', ventaPedidoAut.findById);
  router.post('/venta-pedido-aut-filter', ventaPedidoAut.findAllByFilter);
  router.post('/venta-pedido-aut', ventaPedidoAut.create);
  router.put('/venta-pedido-aut/:id', ventaPedidoAut.update);

  //venta pedido aut ext
  router.get('/venta-pedido-aut-ext', ventaPedidoAutExt.findAll);
  router.get('/venta-pedido-aut-ext/:id', ventaPedidoAutExt.findById);
  router.post('/venta-pedido-aut-ext-filter', ventaPedidoAutExt.findAllByFilter);
  router.post('/venta-pedido-aut-ext-length', ventaPedidoAutExt.getLength);
  router.post('/venta-pedido-aut-ext-paginado-filter', ventaPedidoAutExt.findAllByFilterPaginado);

  //compensa proveedor det ext
  router.get('/compensa-proveedor-det-ext', compensaProveedorDetExt.findAll);
  router.get('/compensa-proveedor-det-ext/:id', compensaProveedorDetExt.findById);
  router.post('/compensa-proveedor-det-ext-filter', compensaProveedorDetExt.findAllByFilter);

  //compensa proveedor det
  router.get('/compensa-proveedor-det', compensaProveedorDet.findAll);
  router.post('/compensa-proveedor-det-filter', compensaProveedorDet.findAllByFilter);
  router.post('/compensa-proveedor-det', compensaProveedorDet.create);
  router.put('/compensa-proveedor-det/:id', compensaProveedorDet.update);

  //compensa proveedor ext
  router.get('/compensa-proveedor-ext', compensaProveedorExt.findAll);
  router.get('/compensa-proveedor-ext/:id', compensaProveedorExt.findById);
  router.post('/compensa-proveedor-ext-filter', compensaProveedorExt.findAllByFilter);
  router.post('/compensa-proveedor-ext-length', compensaProveedorExt.getLength);
  router.post('/compensa-proveedor-ext-paginado-filter', compensaProveedorExt.findAllByFilterPaginado);

  //compensa proveedor
  router.get('/compensa-proveedor', compensaProveedor.findAll);
  router.post('/compensa-proveedor-filter', compensaProveedor.findAllByFilter);
  router.post('/compensa-proveedor', compensaProveedor.create);
  router.put('/compensa-proveedor/:id', compensaProveedor.update);
  // compra det recepcion
  router.get('/compra-det-recepcion', compraDetRecepcion.findAll);
  router.post('/compra-det-recepcion-filter', compraDetRecepcion.findAllByFilter);
  router.post('/compra-det-recepcion', compraDetRecepcion.create);
  router.put('/compra-det-recepcion/:id', compraDetRecepcion.update);
  router.put('/compra-det-recepcion-bulkUpdate', compraDetRecepcion.bulkUpdate);

  //cuenta proveedor estado v2 (Detalle)
  router.get('/cuenta-proveedor-estado-v2', cuentaProveedorEstadoV2.findAll);
  router.get('/cuenta-proveedor-estado-v2/:id', cuentaProveedorEstadoV2.findById);
  router.post('/cuenta-proveedor-estado-v2-filter', cuentaProveedorEstadoV2.findAllByFilter);

  //cuenta proveedor a pagar estado v2 (Encabezado)
  router.get('/cuenta-proveedor-a-pagar-estado-v2', cuentaProveedorApagarEstadoV2.findAll);
  router.get('/cuenta-proveedor-a-pagar-estado-v2/:id', cuentaProveedorApagarEstadoV2.findById);
  router.post('/cuenta-proveedor-a-pagar-estado-v2-filter', cuentaProveedorApagarEstadoV2.findAllByFilter);

  //fondo Capitalizacion ext
  router.get('/fondo-capitalizacion-ext', fondoCapitalizacionExt.findAll);
  router.get('/fondo-capitalizacion-ext/:id', fondoCapitalizacionExt.findById);
  router.post('/fondo-capitalizacion-ext-filter', fondoCapitalizacionExt.findAllByFilter);

  //fondo Capitalizacion
  router.get('/fondo-capitalizacion', fondoCapitalizacion.findAll);
  router.get('/fondo-capitalizacion/:id', fondoCapitalizacion.findById);
  router.post('/fondo-capitalizacion-filter', fondoCapitalizacion.findAllByFilter);
  router.post('/fondo-capitalizacion', fondoCapitalizacion.create);
  router.put('/fondo-capitalizacion/:id', fondoCapitalizacion.update);

  //cuenta cliente estado v2 (detalle)
  router.get('/cuenta-cliente-estado-v2', cuentaClienteEstadoV2.findAll);
  router.get('/cuenta-cliente-estado-v2/:id', cuentaClienteEstadoV2.findById);
  router.post('/cuenta-cliente-estado-v2-filter', cuentaClienteEstadoV2.findAllByFilter);
  router.post('/cuenta-cliente-estado-v2-filter-limit', cuentaClienteEstadoV2.findAllByFilterLimit);

  //cuenta cliente a cobrar estado v2 (encabezado)
  router.get('/cuenta-cliente-a-cobrar-estado-v2', cuentaClienteAcobrarEstadoV2.findAll);
  router.get('/cuenta-cliente-a-cobrar-estado-v2/:id', cuentaClienteAcobrarEstadoV2.findById);
  router.post('/cuenta-cliente-a-cobrar-estado-v2-filter', cuentaClienteAcobrarEstadoV2.findAllByFilter);
  router.post('/cuenta-cliente-a-cobrar-estado-v2-filter-limit', cuentaClienteAcobrarEstadoV2.findAllByFilterLimit);

  //compra Gasto Cuenta
  router.get('/compra-gasto-cuenta', compraGastoCuenta.findAll);
  router.get('/compra-gasto-cuenta/:id', compraGastoCuenta.findById);
  router.post('/compra-gasto-cuenta-filter', compraGastoCuenta.findAllByFilter);
  router.post('/compra-gasto-cuenta', compraGastoCuenta.create);
  router.put('/compra-gasto-cuenta/:id', compraGastoCuenta.update);

  //fondo deposito ext
  router.get('/fondo-deposito-ext', fondoDepositoExt.findAll);
  router.get('/fondo-deposito-ext/:id', fondoDepositoExt.findById);
  router.post('/fondo-deposito-ext-filter', fondoDepositoExt.findAllByFilter);

  //fondo deposito
  router.get('/fondo-deposito', fondoDeposito.findAll);
  router.get('/fondo-deposito/:id', fondoDeposito.findById);
  router.post('/fondo-deposito-filter', fondoDeposito.findAllByFilter);
  router.post('/fondo-deposito', fondoDeposito.create);
  router.put('/fondo-deposito/:id', fondoDeposito.update);
  //cuenta pago ext
  router.get('/cuenta-pago-ext', cuentaPagoExt.findAll);
  router.get('/cuenta-pago-ext/:id', cuentaPagoExt.findById);
  router.post('/cuenta-pago-ext-filter', cuentaPagoExt.findAllByFilter);
  router.post('/cuenta-pago-ext-length', cuentaPagoExt.getLength);
  router.post('/cuenta-pago-ext-paginado-filter', cuentaPagoExt.findAllByFilterPaginado);

  //cuenta pago
  router.get('/cuenta-pago', cuentaPago.findAll);
  router.post('/cuenta-pago-filter', cuentaPago.findAllByFilter);
  router.post('/cuenta-pago', cuentaPago.create);
  router.put('/cuenta-pago/:id', cuentaPago.update);

  //compensa cliente det ext
  router.get('/compensa-cliente-det-ext', compensaClienteDetExt.findAll);
  router.get('/compensa-cliente-det-ext/:id', compensaClienteDetExt.findById);
  router.post('/compensa-cliente-det-ext-filter', compensaClienteDetExt.findAllByFilter);

  //compensa cliente det
  router.get('/compensa-cliente-det', compensaClienteDet.findAll);
  router.post('/compensa-cliente-det-filter', compensaClienteDet.findAllByFilter);
  router.post('/compensa-cliente-det', compensaClienteDet.create);
  router.put('/compensa-cliente-det/:id', compensaClienteDet.update);

  //compensa cliente ext
  router.get('/compensa-cliente-ext', compensaClienteExt.findAll);
  router.get('/compensa-cliente-ext/:id', compensaClienteExt.findById);
  router.post('/compensa-cliente-ext-filter', compensaClienteExt.findAllByFilter);
  router.post('/compensa-cliente-ext-length', compensaClienteExt.getLength);
  router.post('/compensa-cliente-ext-paginado-filter', compensaClienteExt.findAllByFilterPaginado);

  //compensa cliente
  router.get('/compensa-cliente', compensaCliente.findAll);
  router.post('/compensa-cliente-filter', compensaCliente.findAllByFilter);
  router.post('/compensa-cliente', compensaCliente.create);
  router.put('/compensa-cliente/:id', compensaCliente.update);

  //fondo transf Ext
  router.get('/fondo-transf-ext', fondoTransfExt.findAll);
  router.get('/fondo-transf-ext/:id', fondoTransfExt.findById);
  router.post('/fondo-transf-ext-filter', fondoTransfExt.findAllByFilter);
  //fondo Transf
  router.get('/fondo-transf', fondoTransf.findAll);
  router.get('/fondo-transf/:id', fondoTransf.findById);
  router.post('/fondo-transf-filter', fondoTransf.findAllByFilter);
  router.post('/fondo-transf', fondoTransf.create);
  router.put('/fondo-transf/:id', fondoTransf.update);
    //appuser Sucursal Ext
    router.get('/appuser-sucursal-ext', appuserSucursalExt.findAll);
    router.get('/appuser-sucursal-ext/:id', appuserSucursalExt.findById);
    router.post('/appuser-sucursal-ext-filter', appuserSucursalExt.findAllByFilter);
    //appuser sucursal
    router.get('/appuser-sucursal', appuserSucursal.findAll);
    router.get('/appuser-sucursal/:id', appuserSucursal.findByPk);
    router.post('/appuser-sucursal-filter', appuserSucursal.findAllByFilter);
    router.post('/appuser-sucursal', appuserSucursal.create);
    router.put('/appuser-sucursal/:id', appuserSucursal.update);
    router.delete('/appuser-sucursal/:id', appuserSucursal.delete);

  //appuser Caja Ext
  router.get('/appuser-caja-ext', appuserCajaExt.findAll);
  router.get('/appuser-caja-ext/:id', appuserCajaExt.findById);
  router.post('/appuser-caja-ext-filter', appuserCajaExt.findAllByFilter);
  //appuser caja
  router.get('/appuser-caja', appuserCaja.findAll);
  router.get('/appuser-caja/:id', appuserCaja.findByPk);
  router.post('/appuser-caja-filter', appuserCaja.findAllByFilter);
  router.post('/appuser-caja', appuserCaja.create);
  router.put('/appuser-caja/:id', appuserCaja.update);
  router.delete('/appuser-caja/:id', appuserCaja.delete);

  //Cuenta cobro ext
  router.get('/cuenta-cobro-ext', cuentaCobroExt.findAll);
  router.get('/cuenta-cobro-ext/:id', cuentaCobroExt.findById);
  router.post('/cuenta-cobro-ext-filter', cuentaCobroExt.findAllByFilter);
  router.post('/cuenta-cobro-ext-length', cuentaCobroExt.getLength);
  router.post('/cuenta-cobro-ext-paginado-filter', cuentaCobroExt.findAllByFilterPaginado);
  router.post('/cuenta-cobro-ext-filter-limit', cuentaCobroExt.findAllByFilterLimit);

  //Cuenta cobro
  router.get('/cuenta-cobro', cuentaCobro.findAll);
  router.get('/cuenta-cobro/:id', cuentaCobro.findById);
  router.post('/cuenta-cobro-filter', cuentaCobro.findAllByFilter);
  router.post('/cuenta-cobro', cuentaCobro.create);
  router.put('/cuenta-cobro/:id', cuentaCobro.update);
  router.post('/cuenta-cobro-bulkcreate', cuentaCobro.bulkCreate);

  //Fondo mov tipo ext
  router.get('/fondo-mov-tipo-ext', fondoMovTipoExt.findAll);
  router.get('/fondo-mov-tipo-ext/:id', fondoMovTipoExt.findById);
  router.post('/fondo-mov-tipo-ext-filter', fondoMovTipoExt.findAllByFilter);
  //Fondo mov tipo venta contado
  router.get('/fondo-mov-tipo-saldo', fondoMovTipoSaldo.findAll);
  router.post('/fondo-mov-tipo-saldo-filter', fondoMovTipoSaldo.findAllByFilter);
  //Fondo mov tipo det ext
  router.get('/fondo-mov-tipo-det-ext', fondoMovTipoDetExt.findAll);
  router.get('/fondo-mov-tipo-det-ext-ext/:id', fondoMovTipoDetExt.findById);
  router.post('/fondo-mov-tipo-det-ext-filter', fondoMovTipoDetExt.findAllByFilter);
  //Fondo mov tipo det
  router.get('/fondo-mov-tipo-det', fondoMovTipoDet.findAll);
  router.get('/fondo-mov-tipo-det/:id', fondoMovTipoDet.findById);
  router.post('/fondo-mov-tipo-det-filter', fondoMovTipoDet.findAllByFilter);
  router.post('/fondo-mov-tipo-det', fondoMovTipoDet.create);
  router.put('/fondo-mov-tipo-det/:id', fondoMovTipoDet.update);
  //Fondo valor
  router.get('/fondo-valor', fondoValor.findAll);
  router.get('/fondo-valor/:id', fondoValor.findById);
  router.post('/fondo-valor-filter', fondoValor.findAllByFilter);
  router.post('/fondo-valor', fondoValor.create);
  router.put('/fondo-valor/:id', fondoValor.update);
  //fondo mov valor det ext
  router.get('/fondo-mov-valor-det-ext', fondoMovValorDetExt.findAll);
  router.get('/fondo-mov-valor-det-ext-ext/:id', fondoMovValorDetExt.findById);
  router.post('/fondo-mov-valor-det-ext-filter', fondoMovValorDetExt.findAllByFilter);
  //Fondo mov valor det
  router.get('/fondo-mov-valor-det', fondoMovValorDet.findAll);
  router.get('/fondo-mov-valor-det/:id', fondoMovValorDet.findById);
  router.post('/fondo-mov-valor-det-filter', fondoMovValorDet.findAllByFilter);
  router.post('/fondo-mov-valor-det', fondoMovValorDet.create);
  router.put('/fondo-mov-valor-det/:id', fondoMovValorDet.update);
  //Fondo mov
  router.get('/fondo-mov', fondoMov.findAll);
  router.get('/fondo-mov/:id', fondoMov.findById);
  router.post('/fondo-mov-filter', fondoMov.findAllByFilter);
  router.post('/fondo-mov', fondoMov.create);
  router.put('/fondo-mov/:id', fondoMov.update);
  //Nota de crédito det recepción
  router.get('/lote-disponible', loteDisponible.findAll);
  router.get('/lote-disponible/:id', loteDisponible.findById);
  router.post('/lote-disponible-filter', loteDisponible.findAllByFilter);
  //Nota de crédito det recepción
  router.get('/nota-credito-det-recepcion', notaCreditoDetRecepcion.findAll);
  router.get('/nota-credito-det-recepcion/:id', notaCreditoDetRecepcion.findById);
  router.post('/nota-credito-det-recepcion-filter', notaCreditoDetRecepcion.findAllByFilter);
  // router.post('/nota-credito-det-recepcion', notaCreditoDetRecepcion.create);
  router.put('/nota-credito-det-recepcion/:id', notaCreditoDetRecepcion.update);
  router.post('/nota-credito-det-recepcion', notaCreditoDetRecepcion.bulkCreate);
  //Nota de crédito det ext
  router.get('/nota-credito-det-ext', notaCreditoDetExt.findAll);
  router.get('/nota-credito-det-ext/:id', notaCreditoDetExt.findById);
  router.post('/nota-credito-det-ext-filter', notaCreditoDetExt.findAllByFilter);

  //Nota de debito det ext
  router.get('/nota-debito-det-ext', notaDebitoDetExt.findAll);
  router.get('/nota-debito-det-ext/:id', notaDebitoDetExt.findById);
  router.post('/nota-debito-det-ext-filter', notaDebitoDetExt.findAllByFilter);

  //cuenta proveedor a pagar estado
  router.get('/cuenta-proveedor-a-pagar-estado', cuentaProveedorApagarEstado.findAll);
  router.get('/cuenta-proveedor-a-pagar-estado/:id', cuentaProveedorApagarEstado.findById);
  router.post('/cuenta-proveedor-a-pagar-estado-filter', cuentaProveedorApagarEstado.findAllByFilter);
  //venta pedido utilidad
  router.get('/venta-pedido-utilidad', ventaPedidoUtilidad.findAll);
  router.get('/venta-pedido-utilidad/:id', ventaPedidoUtilidad.findById);
  router.post('/venta-pedido-utilidad-filter', ventaPedidoUtilidad.findAllByFilter);

  //venta pedido det entrega utilidad
  router.get('/venta-pedido-det-entrega-utilidad', ventaPedidoDetEntregaUtilidad.findAll);
  router.get('/venta-pedido-det-entrega-utilidad/:id', ventaPedidoDetEntregaUtilidad.findById);
  router.post('/venta-pedido-det-entrega-utilidad-filter', ventaPedidoDetEntregaUtilidad.findAllByFilter);

  //Nota credito ext
  router.get('/nota-credito-ext', notaCreditoExt.findAll);
  router.get('/nota-credito-ext/:id', notaCreditoExt.findById);
  router.post('/nota-credito-ext-filter', notaCreditoExt.findAllByFilter);
  router.post('/nota-credito-ext-length', notaCreditoExt.getLength);
  router.post('/nota-credito-ext-paginado-filter', notaCreditoExt.findAllByFilterPaginado);

  //Nota debito ext
  router.get('/nota-debito-ext', notaDebitoExt.findAll);
  router.get('/nota-debito-ext/:id', notaDebitoExt.findById);
  router.post('/nota-debito-ext-filter', notaDebitoExt.findAllByFilter);
  router.post('/nota-debito-ext-length', notaDebitoExt.getLength);
  router.post('/nota-debito-ext-paginado-filter', notaDebitoExt.findAllByFilterPaginado);

  //nota credito det
  router.get('/nota-credito-det', notaCreditoDet.findAll);
  router.get('/nota-credito-det/:id', notaCreditoDet.findById);
  router.post('/nota-credito-det-filter', notaCreditoDet.findAllByFilter);
  router.post('/nota-credito-det', notaCreditoDet.create);
  router.put('/nota-credito-det/:id', notaCreditoDet.update);

  //nota credito
  router.get('/nota-credito', notaCredito.findAll);
  router.get('/nota-credito/:id', notaCredito.findById);
  router.post('/nota-credito-filter', notaCredito.findAllByFilter);
  router.post('/nota-credito', notaCredito.create);
  router.put('/nota-credito/:id', notaCredito.update);

  //nota debito
  router.get('/nota-debito', notaDebito.findAll);
  router.get('/nota-debito/:id', notaDebito.findById);
  router.post('/nota-debito-filter', notaDebito.findAllByFilter);
  router.post('/nota-debito', notaDebito.create);
  router.put('/nota-debito/:id', notaDebito.update);
  router.put('/nota-debito-entregar/:id', notaDebito.updateNotaDebitoDetAentregado);

  //nota debito tipo
  router.get('/nota-debito-tipo', notaDebitoTipo.findAll);
  router.get('/nota-debito-tipo/:id', notaDebitoTipo.findById);
  router.post('/nota-debito-tipo-filter', notaDebitoTipo.findAllByFilter);
  router.post('/nota-debito-tipo', notaDebitoTipo.create);
  router.put('/nota-debito-tipo/:id', notaDebitoTipo.update);

  //nota credito tipo
  router.get('/nota-credito-tipo', notaCreditoTipo.findAll);
  router.get('/nota-credito-tipo/:id', notaCreditoTipo.findById);
  router.post('/nota-credito-tipo-filter', notaCreditoTipo.findAllByFilter);
  router.post('/nota-credito-tipo', notaCreditoTipo.create);
  router.put('/nota-credito-tipo/:id', notaCreditoTipo.update);

  //stock mercaderia por deposito
  router.get('/stock-mercaderia-por-deposito', stockMercaderiaPorDeposito.findAll);
  router.get('/stock-mercaderia-por-deposito/:id', stockMercaderiaPorDeposito.findById);
  router.post('/stock-mercaderia-por-deposito-filter', stockMercaderiaPorDeposito.findAllByFilter);
  //Deposito ext
  router.get('/deposito-ext', depositoExt.findAll);
  router.get('/deposito-ext/:id', depositoExt.findById);
  router.post('/deposito-ext-filter', depositoExt.findAllByFilter);

  //Caja ext
  router.get('/caja-ext', cajaExt.findAll);
  router.get('/caja-ext/:id', cajaExt.findById);
  router.post('/caja-ext-filter', cajaExt.findAllByFilter);

  //fondo valor ext
  router.get('/fondo-valor-ext', fondoValorExt.findAll);
  router.get('/fondo-valor-ext/:id', fondoValorExt.findById);
  router.post('/fondo-valor-ext-filter', fondoValorExt.findAllByFilter);

  //Cuenta cliente a cobrar estado
  router.get('/cuenta-cliente-a-cobrar-estado', cuentaClienteAcobrarEstado.findAll);
  router.post('/cuenta-cliente-a-cobrar-estado-filter', cuentaClienteAcobrarEstado.findAllByFilter);
  //Cuenta cliente a cobrar ext
  router.get('/cuenta-cliente-a-cobrar-ext', cuentaClienteAcobrarExt.findAll);
  router.get('/cuenta-cliente-a-cobrar-ext/:id', cuentaClienteAcobrarExt.findById);
  router.post('/cuenta-cliente-a-cobrar-ext-filter', cuentaClienteAcobrarExt.findAllByFilter);
  //articulo precio lista 

  //Cobrador
  router.get('/cobrador', cobrador.findAll);
  router.get('/cobrador/:id', cobrador.findById);
  router.post('/cobrador-filter', cobrador.findAllByFilter);
  router.post('/cobrador', cobrador.create);
  router.put('/cobrador/:id', cobrador.update);
  //Centro de costo
  router.get('/costo-centro', costoCentro.findAll);
  router.get('/costo-centro/:id', costoCentro.findById);
  router.post('/costo-centro-filter', costoCentro.findAllByFilter);
  router.post('/costo-centro', costoCentro.create);
  router.put('/costo-centro/:id', costoCentro.update);
  //Cuenta cliente a cobrar
  router.get('/cuenta-cliente-a-cobrar', cuentaClienteAcobrar.findAll);
  router.get('/cuenta-cliente-a-cobrar/:id', cuentaClienteAcobrar.findById);
  router.post('/cuenta-cliente-a-cobrar-filter', cuentaClienteAcobrar.findAllByFilter);
  router.post('/cuenta-cliente-a-cobrar', cuentaClienteAcobrar.create);
  router.put('/cuenta-cliente-a-cobrar/:id', cuentaClienteAcobrar.update);
  //Cliente ext
  router.get('/cliente-ext', clienteExt.findAll);
  router.get('/cliente-ext/:id', clienteExt.findById);
  router.post('/cliente-ext-filter', clienteExt.findAllByFilter);
  //Proveedor ext
  router.get('/proveedor-ext', proveedorExt.findAll);
  router.get('/proveedor-ext/:id', proveedorExt.findById);
  router.post('/proveedor-ext-filter', proveedorExt.findAllByFilter);

  //Stock Mercaderia
  router.get('/stock-mercaderia', stockMercaderia.findAll);
  router.get('/stock-mercaderia/:id', stockMercaderia.findById);
  router.post('/stock-mercaderia-filter', stockMercaderia.findAllByFilter);

  //Cuenta cliente cobro
  router.get('/cuenta-cliente-cobro', cuentaClienteCobro.findAll);
  router.get('/cuenta-cliente-cobro/:id', cuentaClienteCobro.findById);
  router.post('/cuenta-cliente-cobro-filter', cuentaClienteCobro.findAllByFilter);
  router.post('/cuenta-cliente-cobro', cuentaClienteCobro.create);
  router.put('/cuenta-cliente-cobro/:id', cuentaClienteCobro.update);

  //Cuenta cliente estado
  router.get('/cuenta-cliente-estado', cuentaClienteEstado.findAll);
  router.post('/cuenta-cliente-estado-filter', cuentaClienteEstado.findAllByFilter);

  //Cuenta cliente cobro ext
  router.get('/cuenta-cliente-cobro-ext', cuentaClienteCobroExt.findAll);
  router.get('/cuenta-cliente-cobro-ext/:id', cuentaClienteCobroExt.findById);
  router.post('/cuenta-cliente-cobro-ext-filter', cuentaClienteCobroExt.findAllByFilter);
  router.post('/cuenta-cliente-cobro-ext-length', cuentaClienteCobroExt.getLength);
  router.post('/cuenta-cliente-cobro-ext-paginado-filter', cuentaClienteCobroExt.findAllByFilterPaginado);


  //Recibo cobro ext
  router.get('/recibo-cobro-ext', reciboCobroExt.findAll);
  router.get('/recibo-cobro-ext/:id', reciboCobroExt.findById);
  router.post('/recibo-cobro-ext-filter', reciboCobroExt.findAllByFilter);
  router.post('/recibo-cobro-ext-length', reciboCobroExt.getLength);
  router.post('/recibo-cobro-ext-paginado-filter', reciboCobroExt.findAllByFilterPaginado);


  //Recibo pago ext
  router.get('/recibo-pago-ext', reciboPagoExt.findAll);
  router.get('/recibo-pago-ext/:id', reciboPagoExt.findById);
  router.post('/recibo-pago-ext-filter', reciboPagoExt.findAllByFilter);
  router.post('/recibo-pago-ext-length', reciboPagoExt.getLength);
  router.post('/recibo-pago-ext-paginado-filter', reciboPagoExt.findAllByFilterPaginado);

  //Cuenta proveedor pago ext
  router.get('/cuenta-proveedor-pago-ext', cuentaProveedorPagoExt.findAll);
  router.get('/cuenta-proveedor-pago-ext/:id', cuentaProveedorPagoExt.findById);
  router.post('/cuenta-proveedor-pago-ext-filter', cuentaProveedorPagoExt.findAllByFilter);
  router.post('/cuenta-proveedor-pago-ext-length', cuentaProveedorPagoExt.getLength);
  router.post('/cuenta-proveedor-pago-ext-paginado-filter', cuentaProveedorPagoExt.findAllByFilterPaginado);

  //recibo pago
  router.get('/recibo-pago', reciboPago.findAll);
  router.get('/recibo-pago/:id', reciboPago.findById);
  router.post('/recibo-pago-filter', reciboPago.findAllByFilter);
  router.post('/recibo-pago', reciboPago.create);
  router.put('/recibo-pago/:id', reciboPago.update);

  //fondo mov tipo
  router.get('/fondo-mov-tipo', fondoMovTipo.findAll);
  router.get('/fondo-mov-tipo/:id', fondoMovTipo.findById);
  router.post('/fondo-mov-tipo-filter', fondoMovTipo.findAllByFilter);
  router.post('/fondo-mov-tipo', fondoMovTipo.create);
  router.put('/fondo-mov-tipo/:id', fondoMovTipo.update);

  //fondo
  router.get('/fondo', fondo.findAll);
  router.get('/fondo/:id', fondo.findById);
  router.post('/fondo-filter', fondo.findAllByFilter);
  router.post('/fondo', fondo.create);
  router.put('/fondo/:id', fondo.update);

  //fondo entidad
  router.get('/fondo-entidad', fondoEntidad.findAll);
  router.get('/fondo-entidad/:id', fondoEntidad.findById);
  router.post('/fondo-entidad-filter', fondoEntidad.findAllByFilter);
  router.post('/fondo-entidad', fondoEntidad.create);
  router.put('/fondo-entidad/:id', fondoEntidad.update);

  //caja
  router.get('/caja', caja.findAll);
  router.get('/caja/:id', caja.findById);
  router.post('/caja-filter', caja.findAllByFilter);
  router.post('/caja', caja.create);
  router.put('/caja/:id', caja.update);

  //recibo cobro
  router.get('/recibo-cobro', reciboCobro.findAll);
  router.get('/recibo-cobro/:id', reciboCobro.findById);
  router.post('/recibo-cobro-filter', reciboCobro.findAllByFilter);
  router.post('/recibo-cobro', reciboCobro.create);
  router.put('/recibo-cobro/:id', reciboCobro.update);

  //cuenta proveedor pago
  router.get('/cuenta-proveedor-pago', cuentaProveedorPago.findAll);
  router.get('/cuenta-proveedor-pago/:id', cuentaProveedorPago.findById);
  router.post('/cuenta-proveedor-pago-filter', cuentaProveedorPago.findAllByFilter);
  router.post('/cuenta-proveedor-pago', cuentaProveedorPago.create);
  router.put('/cuenta-proveedor-pago/:id', cuentaProveedorPago.update);

  //Cuenta cliente estado
  router.get('/cuenta-cliente-estado', cuentaClienteEstado.findAll);
  router.post('/cuenta-cliente-estado-filter', cuentaClienteEstado.findAllByFilter);

  //Cuenta proveedor estado
  router.get('/cuenta-proveedor-estado', cuentaProveedorEstado.findAll);
  router.post('/cuenta-proveedor-estado-filter', cuentaProveedorEstado.findAllByFilter);
  //stock Det Mercaderia
  router.get('/stock-det-mercaderia', stockDetMercaderia.findAll);
  router.post('/stock-det-mercaderia-filter', stockDetMercaderia.findAllByFilter);
  //Cotización última fecha
  router.get('/cotizacion-ultima-fecha', cotizacionUltimaFecha.findAll);
  router.get('/cotizacion-ultima-fecha/:id', cotizacionUltimaFecha.findById);
  router.post('/cotizacion-ultima-fecha-filter', cotizacionUltimaFecha.findAllByFilter);

  //articulo catalogo
  router.get('/cotizacion-ext', cotizacionExt.findAll);
  router.get('/cotizacion-ext/:id', cotizacionExt.findById);
  router.post('/cotizacion-ext-filter', cotizacionExt.findAllByFilter);

  //cotizacion
  router.get('/cotizacion', cotizacion.findAll);
  router.get('/cotizacion/:id', cotizacion.findById);
  router.post('/cotizacion-filter', cotizacion.findAllByFilter);
  router.post('/cotizacion', cotizacion.create);
  router.put('/cotizacion/:id', cotizacion.update);

  //proveedor
  router.get('/proveedor', proveedor.findAll);
  router.get('/proveedor/:id', proveedor.findById);
  router.post('/proveedor-filter', proveedor.findAllByFilter);
  router.post('/proveedor', proveedor.create);
  router.put('/proveedor/:id', proveedor.update);

  //compraTipo
  router.get('/compra-tipo', compraTipo.findAll);
  router.get('/compra-tipo/:id', compraTipo.findById);
  router.post('/compra-tipo-filter', compraTipo.findAllByFilter);
  router.post('/compra-tipo', compraTipo.create);
  router.put('compra-tipo/:id', compraTipo.update);

  //compraCondicion
  router.get('/compra-condicion', compraCondicion.findAll);
  router.get('/compra-condicion/:id', compraCondicion.findById);
  router.post('/compra-condicion-filter', compraCondicion.findAllByFilter);
  router.post('/compra-condicion', compraCondicion.create);
  router.put('/compra-condicion/:id', compraCondicion.update);


  //VentaFactura Ext
  router.get('/venta-factura-ext', ventaFacturaExt.findAll);
  router.get('/venta-factura-ext/:id', ventaFacturaExt.findById);
  router.post('/venta-factura-ext-filter', ventaFacturaExt.findAllByFilter);
  router.post('/venta-factura-ext-length', ventaFacturaExt.getLength);
  router.post('/venta-factura-ext-paginado-filter', ventaFacturaExt.findAllByFilterPaginado);

  //VentaFacturaDet ext
  router.get('/venta-factura-det-ext', ventaFacturaDetExt.findAll);
  router.get('/venta-factura-det-ext/:id', ventaFacturaDetExt.findById);
  router.post('/venta-factura-det-ext-filter', ventaFacturaDetExt.findAllByFilter);

  //VentaFactura
  router.get('/venta-factura', ventaFactura.findAll);
  router.get('/venta-factura/:id', ventaFactura.findById);
  router.post('/venta-factura-filter', ventaFactura.findAllByFilter);
  router.post('/venta-factura', ventaFactura.create);
  router.put('/venta-factura/:id', ventaFactura.update);

  //VentaCondicion
  router.get('/venta-condicion', ventaCondicion.findAll);
  router.get('/venta-condicion/:id', ventaCondicion.findById);
  router.post('/venta-condicion-filter', ventaCondicion.findAllByFilter);
  router.post('/venta-condicion', ventaCondicion.create);
  router.put('/venta-condicion/:id', ventaCondicion.update);

  //VentaPedidoDet ext
  router.get('/venta-pedido-det-ext', ventaPedidoDetExt.findAll);
  router.get('/venta-pedido-det-ext/:id', ventaPedidoDetExt.findById);
  router.post('/venta-pedido-det-ext-filter', ventaPedidoDetExt.findAllByFilter);

  //VentaPedido Ext
  router.get('/venta-pedido-ext', ventaPedidoExt.findAll);
  router.get('/venta-pedido-ext/:id', ventaPedidoExt.findById);
  router.post('/venta-pedido-ext-filter', ventaPedidoExt.findAllByFilter);
  router.post('/venta-pedido-ext-paginado-filter', ventaPedidoExt.findAllByFilterPaginado);
  router.post('/venta-pedido-ext-length', ventaPedidoExt.getLength);

  //CompraDet ext
  router.get('/compra-det-ext', compraDetExt.findAll);
  router.get('/compra-det-ext/:id', compraDetExt.findById);
  router.post('/compra-det-ext-filter', compraDetExt.findAllByFilter);

  //fondoMov ext
  router.get('/fondo-mov-ext', fondoMovExt.findAll);
  router.get('/fondo-mov-ext/:id', fondoMovExt.findById);
  router.post('/fondo-mov-ext-filter', fondoMovExt.findAllByFilter);

  //stockTransferDet ext
  router.get('/stockTransfer-det-ext', stockTransferDetExt.findAll);
  router.get('/stockTransfer-det-ext/:id', stockTransferDetExt.findById);
  router.post('/stockTransfer-det-ext-filter', stockTransferDetExt.findAllByFilter);

  //Compra Ext
  router.get('/compra-ext', compraExt.findAll);
  router.get('/compra-ext/:id', compraExt.findById);
  router.post('/compra-ext-filter-paginado', compraExt.findAllByFilterPaginado);
  router.post('/compra-ext-filter', compraExt.findAllByFilter);
  router.post('/compra-ext-length', compraExt.getLength);


  //StockTransfer Ext
  router.get('/stockTransfer-ext', stockTransferExt.findAll);
  router.get('/stockTransfer-ext/:id', stockTransferExt.findById);
  router.post('/stockTransfer-ext-filter', stockTransferExt.findAllByFilter);
  router.post('/stockTransfer-ext-length', stockTransferExt.getLength);
  router.post('/stockTransfer-ext-paginado-filter', stockTransferExt.findAllByFilterPaginado);

  //VentaPedido
  router.get('/venta-pedido', ventaPedido.findAll);
  router.get('/venta-pedido/:id', ventaPedido.findById);
  router.post('/venta-pedido-filter', ventaPedido.findAllByFilter);
  router.post('/venta-pedido', ventaPedido.create);
  router.put('/venta-pedido/:id', ventaPedido.update);
  router.put('/venta-entregar/:id', ventaPedido.updateVentaPedidoDetAentregado);

  //Compra
  router.get('/compra', compra.findAll);
  router.get('/compra/:id', compra.findById);
  router.post('/compra-filter', compra.findAllByFilter);
  router.post('/compra', compra.create);
  router.put('/compra/:id', compra.update);

  //StockTransfer
  router.get('/stockTransfer', stockTransfer.findAll);
  router.get('/stockTransfer/:id', stockTransfer.findById);
  router.post('/stockTransfer-filter', stockTransfer.findAllByFilter);
  router.post('/stockTransfer', stockTransfer.create);
  router.put('/stockTransfer/:id', stockTransfer.update);

  //documentoTipo
  router.get('/documentoTipo', documentoTipo.findAll);
  router.get('/documentoTipo/:id', documentoTipo.findById);
  router.post('/documentoTipo-filter', documentoTipo.findAllByFilter);
  router.post('/documentoTipo', documentoTipo.create);
  router.put('/documentoTipo/:id', documentoTipo.update);

  //cliente
  router.get('/cliente', cliente.findAll);
  router.get('/cliente/:id', cliente.findById);
  router.post('/cliente-filter', cliente.findAllByFilter);
  router.post('/cliente', cliente.create);
  router.put('/cliente/:id', cliente.update);

  //deposito
  router.get('/deposito', deposito.findAll);
  router.get('/deposito/:id', deposito.findById);
  router.post('/deposito-filter', deposito.findAllByFilter);
  router.post('/deposito', deposito.create);
  router.put('/deposito/:id', deposito.update);

  //sucursal
  router.get('/sucursal', sucursal.findAll);
  router.get('/sucursal/:id', sucursal.findById);
  router.post('/sucursal-filter', sucursal.findAllByFilter);
  router.post('/sucursal', sucursal.create);
  router.put('/sucursal/:id', sucursal.update);

  //articulo precio lista financ param ext
  router.get('/articulo-precio-lista-financ-param-ext', articuloPrecioListaFinancParamExt.findAll);
  router.get('/articulo-precio-lista-financ-param-ext/:id', articuloPrecioListaFinancParamExt.findById);
  router.post('/articulo-precio-lista-financ-param-ext-filter', articuloPrecioListaFinancParamExt.findAllByFilter);

  //articulo precio lista financ param det ext
  router.get('/articulo-precio-lista-financ', articuloPrecioListaFinanc.findAll);
  router.get('/articulo-precio-lista-financ/:id', articuloPrecioListaFinanc.findById);
  router.post('/articulo-precio-lista-financ-filter', articuloPrecioListaFinanc.findAllByFilter);

  //articulo precio lista financ param
  router.get('/articulo-precio-lista-financ-param', articuloPrecioListaFinancParam.findAll);
  router.get('/articulo-precio-lista-financ-param/:id', articuloPrecioListaFinancParam.findById);
  router.post('/articulo-precio-lista-financ-param-filter', articuloPrecioListaFinancParam.findAllByFilter);
  router.post('/articulo-precio-lista-financ-param', articuloPrecioListaFinancParam.create);
  router.put('/articulo-precio-lista-financ-param/:id', articuloPrecioListaFinancParam.update);
  router.delete('/articulo-precio-lista-financ-param/:id', articuloPrecioListaFinancParam.delete);

  //articulo catalogo
  router.get('/articulo-catalogo-ext', articuloCatalogoExt.findAll);
  router.get('/articulo-catalogo-ext/:id', articuloCatalogoExt.findById);
  router.post('/articulo-catalogo-ext-filter', articuloCatalogoExt.findAllByFilter);


  //articulo precio lista det ext
  router.get('/articulo-precio-lista-det-ext', articuloPrecioListaDetExt.findAll);
  router.get('/articulo-precio-lista-det-ext/:id', articuloPrecioListaDetExt.findById);
  router.post('/articulo-precio-lista-det-ext-filter', articuloPrecioListaDetExt.findAllByFilter);

  //articulo imagen
  router.get('/articulo-imagen', articuloImagen.findAll);
  router.get('/articulo-imagen/:id', articuloImagen.findById);
  router.post('/articulo-imagen-filter', articuloImagen.findAllByFilter);
  router.post('/articulo-imagen', articuloImagen.create);
  router.put('/articulo-imagen/:id', articuloImagen.update);
  // router.put('/articulo-imagen-principal/:id', articuloImagen.updateImagenPrincipal);

  //moneda
  router.get('/moneda', moneda.findAll);
  router.get('/moneda/:id', moneda.findById);
  router.post('/moneda-filter', moneda.findAllByFilter);
  router.post('/moneda', moneda.create);
  router.put('/moneda/:id', moneda.update);

  //articulo precio lista det
  router.get('/articulo-precio-lista-det', articuloPrecioListaDet.findAll);
  router.get('/articulo-precio-lista-det/:id', articuloPrecioListaDet.findById);
  router.post('/articulo-precio-lista-det-filter', articuloPrecioListaDet.findAllByFilter);
  router.post('/articulo-precio-lista-det', articuloPrecioListaDet.create);
  router.put('/articulo-precio-lista-det/:id', articuloPrecioListaDet.update);
  router.put('/articulo-precio-lista-det-padre/:id', articuloPrecioListaDet.updateArticuloPrecioLista);
  router.delete('/articulo-precio-lista-det/:id', articuloPrecioListaDet.delete);


  //articulo precio lista
  router.get('/articulo-precio-lista', articuloPrecioLista.findAll);
  router.get('/articulo-precio-lista/:id', articuloPrecioLista.findById);
  router.post('/articulo-precio-lista-filter', articuloPrecioLista.findAllByFilter);
  router.post('/articulo-precio-lista', articuloPrecioLista.create);
  router.put('/articulo-precio-lista/:id', articuloPrecioLista.update);

  //articulo categoria
  router.get('/appparam', appparam.findAll);
  router.get('/appparam/:id', appparam.findById);
  router.post('/appparam-filter', appparam.findAllByFilter);

  //vendedor
  router.get('/vendedor', vendedor.findAll);
  router.get('/vendedor/:id', vendedor.findById);
  router.post('/vendedor-filter', vendedor.findAllByFilter);
  router.post('/vendedor', vendedor.create);
  router.put('/vendedor/:id', vendedor.update);

  //vendedor-ext
  router.get('/vendedor-ext', vendedorExt.findAll);
  router.get('/vendedor-ext/:id', vendedorExt.findById);
  router.post('/vendedor-ext-filter', vendedorExt.findAllByFilter);

  //articulo categoria
  router.get('/articulo-categoria', articuloCategoria.findAll);
  router.get('/articulo-categoria/:id', articuloCategoria.findById);
  router.post('/articulo-categoria-filter', articuloCategoria.findAllByFilter);
  router.post('/articulo-categoria', articuloCategoria.create);
  router.put('/articulo-categoria/:id', articuloCategoria.update);
  //articulo categoria
  router.get('/articulo-categoria', articuloCategoria.findAll);
  router.get('/articulo-categoria/:id', articuloCategoria.findById);
  router.post('/articulo-categoria-filter', articuloCategoria.findAllByFilter);
  router.post('/articulo-categoria', articuloCategoria.create);
  router.put('/articulo-categoria/:id', articuloCategoria.update);

  //categoria
  router.get('/categoria', categoria.findAll);
  router.get('/categoria/:id', categoria.findById);
  router.post('/categoria-filter', categoria.findAllByFilter);
  router.post('/categoria', categoria.create);
  router.put('/categoria/:id', categoria.update);

  //marca
  router.get('/marca', marca.findAll);
  router.get('/marca/:id', marca.findById);
  router.post('/marca-filter', marca.findAllByFilter);
  router.post('/marca', marca.create);
  router.put('/marca/:id', marca.update);

  //articuloCuExt
  router.get('/articulo-cu-ext', articuloCuExt.findAll);
  router.get('/articulo-cu-ext/:id', articuloCuExt.findById);
  router.post('/articulo-cu-ext-filter', articuloCuExt.findAllByFilter);
  router.get('/articulo-cu-ext-length', articuloCuExt.getLength);

  //articuloExt
  router.get('/articulo-ext', articuloExt.findAll);
  router.get('/articulo-ext/:id', articuloExt.findById);
  router.post('/articulo-ext-filter', articuloExt.findAllByFilter);


  //articulo
  router.get('/articulo', articulo.findAll);
  router.get('/articulo/:id', articulo.findById);
  router.post('/articulo-filter', articulo.findAllByFilter);
  router.post('/articulo', articulo.create);
  router.put('/articulo/:id', articulo.update);
  router.post('/articulo-categorias', articulo.createArticuloCategorias);
  router.put('/articulo-categorias/:id', articulo.updateArticuloCategorias);

  //appuser
  router.get('/appuser', appuser.findAll);
  router.get('/appuser/:id', appuser.findById);
  router.post('/appuser-filter', appuser.findAllByFilter);
  router.post('/appuser', appuser.create);
  router.put('/appuser/:id', appuser.update);

  // Appuser Ext
  router.get('/appuser-ext', appuserExt.findAll);
  router.get('/appuser-ext/:id', appuserExt.findByPk);
  router.post('/appuser-ext-filter', appuserExt.findAllByFilter);
  router.post('/appuser-ext', appuserExt.create);
  router.put('/appuser-ext/:id', appuserExt.update);
  router.delete('/appuser-ext/:id', appuserExt.delete);

  // appaccess
  router.get('/appaccess', appaccess.findAll);
  router.get('/appaccess/:id', appaccess.findByPk);
  router.post('/appaccess-filter', appaccess.findAllByFilter);
  router.post('/appaccess', appaccess.create);
  router.put('/appaccess/:id', appaccess.update);
  router.delete('/appaccess/:id', appaccess.delete);

  //appuser rol
  router.get('/appuser-rol', appuserRol.findAll);
  router.get('/appuser-rol/:id', appuserRol.findByPk);
  router.post('/appuser-rol-filter', appuserRol.findAllByFilter);
  router.post('/appuser-rol', appuserRol.create);
  router.put('/appuser-rol/:id', appuserRol.update);
  router.delete('/appuser-rol/:id', appuserRol.delete);

  // appuserRolDet
  router.get('/appuser-rol-det', appuserRolDet.findAll);
  router.get('/appuser-rol-det/:id', appuserRolDet.findByPk);
  router.post('/appuser-rol-det-filter', appuserRolDet.findAllByFilter);
  router.post('/appuser-rol-det', appuserRolDet.create);
  router.put('/appuser-rol-det/:id', appuserRolDet.update);
  router.delete('/appuser-rol-det/:appaccessId/:appuserRolId', appuserRolDet.delete);

  // Appuser Rol Ext
  router.get('/appuser-rol-ext', appuserRolExt.findAll);
  router.get('/appuser-rol-ext/:id', appuserRolExt.findByPk);
  router.post('/appuser-rol-ext-filter', appuserRolExt.findAllByFilter);
  router.post('/appuser-rol-ext', appuserRolExt.create);
  router.put('/appuser-rol-ext/:id', appuserRolExt.update);
  router.delete('/appuser-rol-ext/:id', appuserRolExt.delete);

  // Appuser Rol Det Ext
  router.get('/appuser-rol-det-ext', appuserRolDetExt.findAll);
  router.get('/appuser-rol-det-ext/:id', appuserRolDetExt.findByPk);
  router.post('/appuser-rol-det-ext-filter', appuserRolDetExt.findAllByFilter);
  router.post('/appuser-rol-det-ext', appuserRolDetExt.create);
  router.put('/appuser-rol-det-ext/:id', appuserRolDetExt.update);

  // Vendedor Articulo Precio Lista
  router.get('/vendedor-articulo-precio-lista', vendedorArticuloPrecioLista.findAll);
  router.post('/vendedor-articulo-precio-lista-filter', vendedorArticuloPrecioLista.findAllByFilter);
  router.post('/vendedor-articulo-precio-lista', vendedorArticuloPrecioLista.create);
  router.put('/vendedor-articulo-precio-lista/:id', vendedorArticuloPrecioLista.update);
  router.delete('/vendedor-articulo-precio-lista/:articuloPrecioListaId/:vendedorId', vendedorArticuloPrecioLista.delete);


  // Vendedor Articulo Precio Lista Ext
  router.get('/vendedor-articulo-precio-lista-ext/:articuloPrecioListaId/:vendedorId', vendedorArticuloPrecioLista.findById);
  router.get('/vendedor-articulo-precio-lista-ext', vendedorArticuloPrecioListaExt.findAll);
  router.post('/vendedor-articulo-precio-lista-ext-filter', vendedorArticuloPrecioListaExt.findAllByFilter);


  // Login
  router.post('/authentication-login', authentication.login);

  // TRANSACCIONES
  // Cobro cliente móvil
  router.post('/cobro-cliente-movil-trans', cobroClienteMovilTrans.createTrans);
  // Compensa proveedor
  router.post('/compensa-proveedor-trans', compensaProveedorTrans.createTrans);
  // Compensa cliente
  router.post('/compensa-cliente-trans', compensaClienteTrans.createTrans);

  // Fondo Mov
  router.post('/fondo-mov-trans', fondoMovTrans.createTrans);

  // Nota crédito
  router.post('/nota-credito-trans', notaCreditoTrans.createTrans);

  // Nota debito
  router.post('/nota-debito-trans', notaDebitoTrans.createTrans);

  // Recibo pago
  router.post('/recibo-pago-trans', reciboPagoTrans.createTrans);

  // Recibo cobro
  router.post('/recibo-cobro-trans', reciboCobroTrans.createTrans);

  // Compra recepcion
  router.post('/compra-recepcion-trans', compraRecepcionTrans.createTrans);

  // Venta factura
  router.post('/venta-factura-trans', ventaFacturaTrans.createTrans);
  router.put('/venta-factura-trans-update/:id', ventaFacturaTrans.updateTrans);

  // Venta pedido
  router.post('/venta-pedido-trans', ventaPedidoTrans.createTrans);
  router.put('/venta-pedido-trans-update/:id', ventaPedidoTrans.updateTrans);

  //Compra
  router.post('/compra-trans', compraTrans.createTrans);
  router.put('/compra-trans-update/:id', compraTrans.updateTrans);

  //StockTransfer
  router.post('/stockTransfer-trans', stockTransferTrans.createTrans);
  router.put('/stockTransfer-trans-update/:id', stockTransferTrans.updateTrans);

  //PROCEDURES

  router.get('/articulo-lista-insertar/:id', articuloPrecioLista.insertarDetalleArticulos);

  router.get('/venta-factura-insertar/:ventaPedidoId/:appuserId', ventaFactura.insertarVentaFactura);


  return router;
};