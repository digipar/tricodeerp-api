const fondoMov = require('../controllers/fondoMov');
const fondoMovTipoDet = require('../controllers/fondoMovTipoDet');
const fondoMovValorDet = require('../controllers/fondoMovValorDet');
const cuentaCobro = require('../controllers/cuentaCobro');
const compensaClienteDet = require('../controllers/compensaClienteDet');
const compensaCliente = require('../controllers/compensaCliente');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        // console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return cuentaCobro.create2(req.body.cuentaCobro, { transaction: t })
                .then(function (cuentaCobroRes) {
                    req.body.fondoMovTipoDet.find(item => item.fondoMovTipoDetNumero == null).fondoMovTipoDetNumero = cuentaCobroRes.cuentaCobroId;

                    let indexRecibo = req.body.compensaClienteDetalle.findIndex(item => item.compensaClienteMovId === null);
                    if (indexRecibo > 0) {
                        req.body.compensaClienteDetalle[indexRecibo].compensaClienteMovId = cuentaCobroRes.cuentaCobroId;
                    }
                    return fondoMovTipoDet.bulkCreate(req.body.fondoMovTipoDet, { transaction: t })
                        .then(() => {
                            return fondoMovValorDet.bulkCreate(req.body.fondoMovValorDet, { transaction: t })
                                .then(() => {
                                    return fondoMov.create2(req.body, { transaction: t })
                                        .then(() => {
                                            return compensaClienteDet.bulkCreate(req.body.compensaClienteDetalle, { transaction: t })
                                                .then(function () {
                                                    compensaCliente.create2(req.body.compensacion, { transaction: t })
                                                    cuentaCobroRes.transaccionId = req.body.transaccionId
                                                    return cuentaCobroRes
                                                })
                                        })
                                })
                        })
                })

        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
}