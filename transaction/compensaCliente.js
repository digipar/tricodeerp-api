const compensaCliente = require('../controllers/compensaCliente');
const compensaClienteDet = require('../controllers/compensaClienteDet');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return compensaClienteDet.bulkCreate(req.body.compensaClienteDetalle, { transaction: t })
                .then(function () {
                    return compensaCliente.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}