const compensaProveedor = require('../controllers/compensaProveedor');
const compensaProveedorDet = require('../controllers/compensaProveedorDet');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return compensaProveedorDet.bulkCreate(req.body.compensaProveedorDetalle, { transaction: t })
                .then(function () {
                    return compensaProveedor.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}