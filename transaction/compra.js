const compra = require('../controllers/compra');
const compraDet = require('../controllers/compraDet');
const models = require('../models/index');



module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return compraDet.bulkCreate(req.body.compraDet, { transaction: t })
                .then(function () {
                    return compra.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
    updateTrans(req, res) {
        return models.sequelize.transaction(function (t) {
            return compraDet.deletePorCompra2(req.params.id, { transaction: t })
                .then(function () {
                    return compraDet.bulkCreate(req.body.compraDet, { transaction: t })
                        .then(() => {
                            return compra.update2(req, { transaction: t })
                        })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
}