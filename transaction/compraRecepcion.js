const compra = require('../controllers/compra');
const compraDetRecepcion = require('../controllers/compraDetRecepcion');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            console.log('1 :>> ');
            return compraDetRecepcion.bulkCreate(req.body.compraDetRecepcion, { transaction: t })
            
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}