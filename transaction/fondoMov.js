const fondoMov = require('../controllers/fondoMov');
const fondoMovTipoDet = require('../controllers/fondoMovTipoDet');
const fondoMovValorDet = require('../controllers/fondoMovValorDet');
const cuentaCobro = require('../controllers/cuentaCobro');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        let cuentaCobroObj = req.body.fondoMovTipoDet.find(item => item.cuentaCobroId === null)
        return models.sequelize.transaction(function (t) {
            if(cuentaCobroObj){
                // let index = req.body.fondoMovTipoDet.findIndex(item => item.cuentaCobroId === null)
                let newCuentaCobro = { 
                    cuentaCobroFecha: req.body.fondoMovFechaHora,
                    appuserId:cuentaCobroObj.appuserId,
                    clienteId:cuentaCobroObj.clienteId,
                    monedaId:cuentaCobroObj.monedaId,
                    cobradorId:cuentaCobroObj.cobradorId,
                    cuentaCobroMonto:cuentaCobroObj.cuentaCobroMonto,
                    cuentaCobroCotizacion:cuentaCobroObj.cuentaCobroCotizacion,
                    conceptoTexto:cuentaCobroObj.conceptoTexto
                }
                return cuentaCobro.create2(newCuentaCobro, { transaction: t })
                    .then(function (result) {
                        let index = req.body.fondoMovTipoDet.findIndex(item => item.cuentaCobroId === null)
                        req.body.fondoMovTipoDet[index].fondoMovTipoDetNumero = result.cuentaCobroId;
                        // req.body.fondoMovTipoDet.push({
                        //     fondoMovTipoDetNumero: result.cuentaCobroId,
                        //     fondoMovTipoId: 2,
                        //     monedaId: result.monedaId,
                        //     fondoMovTipoDetMonto: result.cuentaCobroMonto,
                        //     fondoMovTipoDetCotizacion: result.cuentaCobroCotizacion,
                        //     transaccionId: req.body.transaccionId
                        // })
                        console.log('req.body.fondoMovTipoDet :>> ', req.body.fondoMovTipoDet);
                        return fondoMovTipoDet.bulkCreate(req.body.fondoMovTipoDet, { transaction: t })
                            .then(() => {
                                return fondoMovValorDet.bulkCreate(req.body.fondoMovValorDet, { transaction: t })
                                    .then(() => {
                                        return fondoMov.create2(req.body, { transaction: t })
                                    })
                            })
                    })
            }else{
                return fondoMovTipoDet.bulkCreate(req.body.fondoMovTipoDet, { transaction: t })
                .then(() => {
                    return fondoMovValorDet.bulkCreate(req.body.fondoMovValorDet, { transaction: t })
                        .then(() => {
                            return fondoMov.create2(req.body, { transaction: t })
                        })
                })
            }
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
}