const notaCredito = require('../controllers/notaCredito');
const notaCreditoDet = require('../controllers/notaCreditoDet');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return notaCreditoDet.bulkCreate(req.body.notaCreditoDetalles, { transaction: t })
                .then(function () {
                    return notaCredito.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}