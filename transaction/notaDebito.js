const notaDebito = require('../controllers/notaDebito');
const notaDebitoDet = require('../controllers/notaDebitoDet');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return notaDebitoDet.bulkCreate(req.body.notaDebitoDetalles, { transaction: t })
                .then(function () {
                    return notaDebito.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            console.log('err', err)
            res.status(500).json(err);
        })
    }
}