const reciboCobro = require('../controllers/reciboCobro');
const cuentaClienteCobro = require('../controllers/cuentaClienteCobro');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            console.log('1 :>> ');
            return cuentaClienteCobro.bulkCreate(req.body.cuentaClientesCobros, { transaction: t })
                .then(function () {
                    console.log('2 :>> ');
                    return reciboCobro.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}