const reciboPago = require('../controllers/reciboPago');
const cuentaProveedorPago = require('../controllers/cuentaProveedorPago');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return cuentaProveedorPago.bulkCreate(req.body.cuentaProveedoresPagos, { transaction: t })
                .then(function () {
                    return reciboPago.create2(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    }
}