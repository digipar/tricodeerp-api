const ventaFactura = require('../controllers/ventaFactura');
const ventaFacturaDet = require('../controllers/ventaFacturaDet');
const models = require('../models/index');



module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return ventaFacturaDet.bulkCreate(req.body.ventaFacturaDet, { transaction: t })
                .then(function () {
                    return ventaFactura.create(req.body, { transaction: t })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
    updateTrans(req, res) {
        return models.sequelize.transaction(function (t) {
            return ventaFacturaDet.deletePorVentaFactura2(req.params.id, { transaction: t })
                .then(function () {
                    return ventaFacturaDet.bulkCreate(req.body.ventaFacturaDet, { transaction: t })
                        .then(() => {
                            return ventaFactura.update2(req, { transaction: t })
                        })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
}