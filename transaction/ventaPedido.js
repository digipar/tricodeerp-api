const ventaPedido = require('../controllers/ventaPedido');
const ventaPedidoDet = require('../controllers/ventaPedidoDet');
const cuentaClienteAcobrar = require('../controllers/cuentaClienteAcobrar');
const models = require('../models/index');

module.exports = {
    createTrans(req, res) {
        console.log('Datos del request: ', req.body);
        return models.sequelize.transaction(function (t) {
            return ventaPedidoDet.bulkCreate(req.body.ventaPedidoDet, { transaction: t })
                .then(function () {

                    if(req.body.ventaDetCuentasCliente && req.body.ventaDetCuentasCliente.length){
                        return cuentaClienteAcobrar.bulkCreate(req.body.ventaDetCuentasCliente, { transaction: t })
                        .then(() => {
                            return ventaPedido.create(req.body, { transaction: t })
                        })
                    }else{
                        return ventaPedido.create(req.body, { transaction: t })
                    }
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
    updateTrans(req, res) {
        return models.sequelize.transaction(function (t) {
            return ventaPedidoDet.deletePorVentaPedido2(req.params.id, { transaction: t })
                .then(function () {
                    return ventaPedidoDet.bulkCreate(req.body.ventaPedidoDet, { transaction: t })
                        .then(() => {
                            return ventaPedido.update2(req, { transaction: t })
                        })
                })
        }).then(function (result) {
            res.status(200).json(result);
        }).catch(function (err) {
            res.status(500).json(err);
        })
    },
}